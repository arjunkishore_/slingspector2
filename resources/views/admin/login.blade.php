<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>Login | SlingSpector</title>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="/assets/base/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="/assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="/assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="/assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="/assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="/assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="/assets/base/examples/css/pages/login-v3.css">
    <link rel="stylesheet" href="/assets/base/css/custom.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="/assets/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="/assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="/assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="/assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="/assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="/assets/global/vendor/modernizr/modernizr.js"></script>
    <script src="/assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="page-login-v3 layout-full" style="background-color: #404040;">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out" style="background-color: #404040;">
    <div class="page-content vertical-align-middle">
        <img src="/assets/global/images/SS-logo-login.png" style="margin-bottom: 30px; width: 100%;" />
        <div class="panel">
            <div class="panel-body">
				<!-- Notifications -->
                @include('notifications')

                <form method="post" action="{{ route('login') }}">
                    <div class="form-group form-material floating {{ $errors->first('email', 'has-error') }}" data-plugin="formMaterial">
                        <input type="email" class="form-control" name="email" autocomplete="Off" />
                        <label class="floating-label">Email</label>
                    </div>
					<span class="help-block">{{ $errors->first('email', ':message') }}</span>
                    <div class="form-group form-material floating {{ $errors->first('password', 'has-error') }}">
                        <input type="password" class="form-control" name="password" autocomplete="Off" value="" />
                        <label class="floating-label">Password</label>
                    </div>
					<span class="help-block">{{ $errors->first('password', ':message') }}</span>
					<div class="form-group clearfix">
		              <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg pull-left">
		                <input type="checkbox" id="inputCheckbox" name="remember-me">
		                <label for="inputCheckbox">Remember me</label>
		              </div>
		              <a class="pull-right" href="{{ url('forgot-password') }}">Forgot password?</a>
		            </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40" style="background: #808083; border: 0px;">Submit</button>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
				<p>Still no account? Please <a href="{{ url('signup') }}">Sign up</a></p>
            </div>
        </div>
		<footer class="page-copyright page-copyright-inverse">
          <p>© {{ date('Y') }} Arizon Healthcare. All rights reserved</p>
        </footer>
    </div>
</div>
<!-- End Page -->
<!-- Core  -->
<script src="/assets/global/vendor/jquery/jquery.js"></script>
<script src="/assets/global/vendor/bootstrap/bootstrap.js"></script>
<script src="/assets/global/vendor/animsition/animsition.js"></script>
<script src="/assets/global/vendor/asscroll/jquery-asScroll.js"></script>
<script src="/assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="/assets/global/vendor/asscrollable/jquery-asScrollable.min.js"></script>
<script src="/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<!-- Plugins -->
<script src="/assets/global/vendor/switchery/switchery.min.js"></script>
<script src="/assets/global/vendor/intro-js/intro.js"></script>
<script src="/assets/global/vendor/screenfull/screenfull.js"></script>
<script src="/assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="/assets/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<!-- Scripts -->
<script src="/assets/global/js/core.js"></script>
<script src="/assets/base/js/site.js"></script>
<script src="/assets/base/js/sections/menu.js"></script>
<script src="/assets/base/js/sections/menubar.js"></script>
<script src="/assets/base/js/sections/gridmenu.js"></script>
<script src="/assets/base/js/sections/sidebar.js"></script>
<script src="/assets/global/js/configs/config-colors.js"></script>
<script src="/assets/base/js/configs/config-tour.js"></script>
<script src="/assets/global/js/components/asscrollable.js"></script>
<script src="/assets/global/js/components/animsition.js"></script>
<script src="/assets/global/js/components/slidepanel.js"></script>
<script src="/assets/global/js/components/switchery.js"></script>
<script src="/assets/global/js/components/jquery-placeholder.js"></script>
<script src="/assets/global/js/components/material.js"></script>
<script>
    (function(document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
</body>
</html>
