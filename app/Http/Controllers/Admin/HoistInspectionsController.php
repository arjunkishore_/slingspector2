<?php

namespace App\Http\Controllers\Admin;

use App\Models\Hoist;
use App\Models\Location;
use App\Models\Organisation;
use App\Models\Inspection;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;
use Sentinel;
use DB;

class HoistInspectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = Sentinel::getUser();
		$view['user']= $user;

        $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

		if ($orgID){
			$inspections = Inspection::whereIn('type', ['stand-up-hoist', 'portable-ceiling-hoist', 'metal-hook-coat-hanger-hoist', 'fixed-ceiling-hoist', 'keyhole-plate-tilt-frame-hoist'])->with('sling', 'organisation')->where('organisation_id', $orgID)->get();
		} else {
			$inspections = Inspection::whereIn('type', ['stand-up-hoist', 'portable-ceiling-hoist', 'metal-hook-coat-hanger-hoist', 'fixed-ceiling-hoist', 'keyhole-plate-tilt-frame-hoist'])->with('sling', 'organisation')->get();
		}

		foreach ($inspections as $inspection){
			$l = Location::find($inspection->hoist['location_id']);

			if (isset($l)){
				$inspection->location_name = $l->name;
			}
		}

		$view['inspections'] = $inspections;

        return view('admin.hoists.inspections.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$view['organisations'] = Organisation::dropdownList();
        $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		$view['hoists'] = Hoist::dropdownList($orgID);
		$view['headings'] = Question::select('heading')->where('inspection_type', 'hoist')->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['questions'] = Question::where('inspection_type', 'hoist')->where('is_active', 1)->orderBy('ordering', 'ASC')->get();
        return view('admin.hoists.inspections.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Input::all();
		$answers = array();

		foreach ($post as $k => $v){
			$split = explode('_', $k);

			if ($split[0] == 'question' && !isset($split[2])){
				$answers[] = array(
					'question_id'	=> $split[1],
					'answer'		=> $v,
					'comments'		=> Input::get('question_'.$split[1].'_comment')
				);
			}
		}

		//print_r($answers);

		if (count($answers > 0)){
			$dt = Carbon::now();

			// Create new inspection
			$inspection = new Inspection;

			$inspection->organisation_id = Input::get('organisation_id');
			$inspection->hoist_id = Input::get('hoist_id');
			$inspection->user_id = Sentinel::getUser()->id;
			$inspection->type = Input::get('inspection_type');
			$inspection->inspection_date = $dt->toDateTimeString();
			$inspection->completed = 1;

			$inspection->save();

			// Save answers to Database
			foreach ($answers as $a){
				$answer = new Answer;
				$answer->inspection_id = $inspection->id;
				$answer->question_id = $a['question_id'];
				$answer->answer = $a['answer'];
				$answer->comments = $a['comments'];
				$answer->save();
			}

			// Update Hoist to set date for last inspection
			$hoist = Hoist::find(Input::get('hoist_id'));
			$hoist->last_6month_inspection = $dt->toDateString();
			$hoist->save();

			return Redirect::to('hoists/inspections')->with('success', 'Inspection Saved Successfully');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$view['inspection'] = Inspection::with('organisation', 'hoist')->find($id);
		$view['headings'] = Question::select('heading')->where('inspection_type', 'hoist')->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['answers'] = Inspection::getHoistAnswers($id);
		return view('admin.hoists.inspections.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
		$inspection = Inspection::with('organisation', 'hoist')->find($id);

		if($inspection->delete()){
			DB::table('inspections')->where('id', $inspection->id)
				->update(array('deleted_by' => filter_var ($request->deleted_by, FILTER_SANITIZE_STRING) ,'delete_reason' => filter_var( $request->delete_reason, FILTER_SANITIZE_STRING)));
		}

		return Redirect::to('hoists')->with('success', 'Inspection Deleted Successfully');
    }
}
