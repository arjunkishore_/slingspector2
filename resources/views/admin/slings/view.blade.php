@extends('admin.layouts.master')

@section('bodyclass', 'slings sling')

@section('title')
	{{ 'Sling - '.$sling->barcode }}
@stop

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>

    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Sling - {{ $sling->barcode }}</h1>
  		<div class="page-header-actions">
			<a href="/slings/{{ $sling->id }}/edit" class="btn btn-primary">Edit</a>
			<a data-href="/slings/{{ $sling->id }}" data-toggle="modal" data-target="#confirm-delete" data-row="{{ $sling->id }}" data-type="Sling" class="btn btn-danger delete">Delete</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings">Slings</a></li>
			<li class="breadcrumb-item active">Sling - {{ $sling->barcode }}</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
					<ul class="nav nav-tabs nav-tabs-solid" role="tablist">
					<li class="nav-item active" role="presentation">
  						<a class="nav-link active" data-toggle="tab" href="#information" aria-controls="information" role="tab">
  							Information
						</a>
					</li>
					<li class="nav-item" role="presentation">
  						<a class="nav-link" data-toggle="tab" href="#inspections" aria-controls="inspections" role="tab">
  							Inspections
						</a>
					</li>
					<li class="nav-item" role="presentation">
  						<a class="nav-link" data-toggle="tab" href="#washes" aria-controls="washes" role="tab">
  							Washes
						</a>
					</li>
					<li class="nav-item" role="presentation">
  						<a class="nav-link" data-toggle="tab" href="#repairs" aria-controls="repairs" role="tab">
  							Service/Repairs
						</a>
					</li>
					</ul>
					<div class="tab-content">
					<div class="tab-pane active" id="information" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-8 col-xs-12">
								<div class="form-group row">
									{{ Form::label('barcode', 'Sling ID:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
									<div class="col-md-8 col-xs-12">
										{{ $sling->barcode }}
									</div>
								</div>
								<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						    	<div class="form-group row">
									{{ Form::label('organisation_id', 'Organisation:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->organisation['name'] }}
						      		</div>
						    	</div>
								<?php } ?>
								<div class="form-group row">
									{{ Form::label('location_id', 'Facility:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->location['name'] }}
						      		</div>
						    	</div>
								<div class="form-group row">
									{{ Form::label('manufacturer', 'Manufacturer:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->manufacturer }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('name', 'Product Name:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->name }}
						      		</div>
						    	</div>
								<div class="form-group row">
									{{ Form::label('product_code', 'Product Code:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
									<div class="col-md-8 col-xs-12">
										{{ $sling->product_code }}
									</div>
								</div>
								<div class="form-group row">
									{{ Form::label('type', 'Product Type:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
									<div class="col-md-8 col-xs-12">
										{{ ucwords(str_replace("-", " ", $sling->type)) }}
									</div>
								</div>
								<div class="form-group row">
									{{ Form::label('size', 'Size:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->size }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('color', 'Color:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->colour }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('swl', 'Safe Working Load (SWL):', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $sling->swl .' '.$sling->unit }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('last_6month_inspection', 'Last 6 Month Inspection:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($sling->last_6month_inspection){
												$dt = Carbon::createFromFormat('d/m/Y', $sling->last_6month_inspection);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>

								<div class="form-group row">
									{{ Form::label('date_of_manufacture', 'Date of Manufacture:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($sling->date_of_manufacture){
												$dt = Carbon::createFromFormat('d/m/Y', $sling->date_of_manufacture);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('date_of_purchase', 'Date of Purchase:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($sling->date_of_purchase){
												$dt = Carbon::createFromFormat('d/m/Y', $sling->date_of_purchase);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('end_of_life', 'Date of Decommission:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
									  	<?php
											if ($sling->end_of_life){
												$dt = Carbon::createFromFormat('d/m/Y', $sling->end_of_life);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="inspections" role="tabpanel">

						<table class="table table-hover dataTable table-striped table-search w-full" id="inspectionsTable">
					  		<thead>
								<tr>
									<th>Date</th>
									<th>Status</th>
									<th class="no-sort">Actions</th>
								</tr>
					  		</thead>
					  		<tbody>
								<?php
									if (isset($inspections)){
										foreach ($inspections as $inspection){
											echo '<tr id="'.$inspection->id.'">';
												echo '<td>'.$inspection->inspection_date.'</td>';
												$status = $inspection->completed == 1 ? 'Completed' : 'In Progress';
												echo '<td>'.$status.'</td>';
												echo '<td>';
													if ($inspection->third_party_document){
														echo '<a href="http://files.roobix.com.au/slingspector-com/app/inspections/'.$inspection->third_party_document.'" class="btn btn-xs btn-slingspector action" target="_blank">Download</a>';
													} else {
														echo '<a href="/slings/inspections/'.$inspection->id.'" class="btn btn-xs btn-slingspector action">View</a>';
													}
													echo '<button data-href="/slings/inspections/'.$inspection->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$inspection->id.'" data-type="Inspection" class="btn btn-xs btn-danger action delete">Delete</button>';
												echo '</td>';
											echo '</tr>';
										}
									}
								?>
					  		</tbody>

						</table>
					</div>
					<div class="tab-pane" id="washes" role="tabpanel">

						<table class="table table-hover dataTable table-striped table-search w-full" id="washesTable">
					  		<thead>
								<tr>
									<th>Date</th>
									<th>Notes</th>
									<th class="no-sort">Actions</th>
								</tr>
					  		</thead>
					  		<!--
							<tfoot>
								<tr>
									<th>Date</th>
									<th>Notes</th>
									<th>Actions</th>
								</tr>
					  		</tfoot>
							-->
					  		<tbody>
								<?php
									if (isset($washes)){
										foreach ($washes as $wash){
											echo '<tr id="'.$wash->id.'">';
												echo '<td>'.$wash->wash_date.'</td>';
												echo '<td>'.$wash->notes.'</td>';
												echo '<td>';
													echo '<a href="/slings/washes/'.$wash->id.'" class="btn btn-xs btn-slingspector action">View</a>';
													echo '<button data-href="/slings/washes/'.$wash->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$wash->id.'" data-type="Wash" class="btn btn-xs btn-danger action delete">Delete</button>';
												echo '</td>';
											echo '</tr>';
										}
									}
								?>
					  		</tbody>
						</table>
					</div>
					<div class="tab-pane" id="repairs" role="tabpanel">

						<table class="table table-hover dataTable table-striped table-search w-full" id="repairsTable">
					  		<thead>
								<tr>
									<th>Date Lodged</th>
									<th>Date Completed</th>
									<th>Notes</th>
									<th class="no-sort">Actions</th>
								</tr>
					  		</thead>
					  		<!--
							<tfoot>
								<tr>
									<th>Date Lodged</th>
									<th>Date Completed</th>
									<th>Notes</th>
									<th>Actions</th>
								</tr>
					  		</tfoot>
							-->
					  		<tbody>
								<?php


									if (isset($repairs)){
										foreach ($repairs as $repair){
											echo '<tr id="'.$repair->id.'">';
												echo '<td>'.$repair->repair_date.'</td>';
												echo '<td>'.$repair->repair_complete.'</td>';
												echo '<td>'.$repair->notes.'</td>';
												echo '<td>';
													echo '<a href="/slings/repairs/'.$repair->id.'" class="btn btn-xs btn-slingspector action">View</a>';
													echo '<button data-href="/slings/repairs/'.$repair->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$repair->id.'" data-type="Repair" class="btn btn-xs btn-danger action delete">Delete</button>';
												echo '</td>';
											echo '</tr>';
										}
									}
								?>
					  		</tbody>
						</table>
					</div>
					</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="sling_id" id="sling_id_holder" value="{{ $sling->id }}">
@stop
