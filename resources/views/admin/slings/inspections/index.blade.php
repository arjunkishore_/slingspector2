@extends('admin.layouts.master')

@section('bodyclass', 'inspections')

@section('title', 'Inspections')

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Inspections</h1>
  		<div class="page-header-actions">
	  		<a href="/slings/inspections/create" class="btn btn-slingspector">Start Inspection</a>&nbsp;<a href="/slings/3rd-party-inspection" class="btn btn-slingspector">Upload 3rd-Party Inspection</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item active">Inspections</li>
		</ol>
	</div>
@stop

@section('content')
	<!-- Panel -->
	<div class="message-holder">
		@include('notifications')
	</div>

	<div class="panel">
	  	<!--
		<header class="panel-heading">
			<h3 class="panel-title">Individual Column Searching</h3>
	  	</header>
		-->
	  	<div class="panel-body">
			<table class="table table-hover dataTable table-striped table-search w-full" id="inspectionsTable">
		  		<thead>
					<tr>
						<th>Sling ID</th>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?><th>Organisation</th><?php } ?>
						<th>Facility</th>
						<th>Date</th>
						<th>Status</th>
						<th class="no-sort">Actions</th>
					</tr>
		  		</thead>
		  		<!--
				<tfoot>
					<tr>
						<th>Sling Code</th>
						<th>Organisation</th>
						<th>Location</th>
						<th>Date</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
		  		</tfoot>
				-->
		  		<tbody>
					<?php

						if (isset($inspections)){

							foreach ($inspections as $inspection){
								echo '<tr id="'.$inspection->id.'">';
									echo '<td>'.$inspection->sling['barcode'].'</td>';
									if (Sentinel::getUser()->inRole('admin')){ echo '<td>'.$inspection->organisation['name'].'</td>'; }
									echo '<td>'.$inspection->location_name.'</td>';
									echo '<td>'.$inspection->inspection_date.'</td>';
									$status = $inspection->completed == 1 ? 'Completed' : 'In Progress';
									echo '<td>'.$status.'</td>';
									echo '<td>';

										if ($inspection->third_party_document){
											echo '<a href="http://files.roobix.com.au/slingspector-com/app/inspections/'.$inspection->third_party_document.'" class="btn btn-xs btn-slingspector action" target="_blank">Download</a>';
										} else {
											echo '<a href="/slings/inspections/'.$inspection->id.'" class="btn btn-xs btn-slingspector action">View</a>';
										}
										
										//echo '<a href="/slings/inspections/'.$inspection->id.'" class="btn btn-xs btn-info action">Export</a>';
										if ($inspection->completed != 1){
											//echo '<a href="/slings/inspections/'.$inspection->id.'/edit" class="btn btn-xs btn-primary action">Continue</a>';
										}

										echo '<button data-href="/slings/inspections/'.$inspection->id.'" data-toggle="modal" data-target="#confirm-delete" data-username="'.$user->first_name." ".$user->last_name.'" data-row="'.$inspection->id.'" data-type="Inspection" class="btn btn-xs btn-danger action delete">Delete</button>';
									echo '</td>';
								echo '</tr>';
							}
						}
					?>
		  		</tbody>
			</table>
	  	</div>
	</div>
	<!-- End Panel -->
@stop
