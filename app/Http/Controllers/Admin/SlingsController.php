<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sling;
use App\Models\Location;
use App\Models\Organisation;
use App\Models\Question;
use App\Models\Inspection;
use App\Models\Repair;
use App\Models\Wash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Filesystem\Filesystem;
use Validator;
use Sentinel;
use Response;
use DB;
use Storage;
use Carbon\Carbon;

class SlingsController extends Controller
{
	/**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		  'organisation_id' => 'required',
		  'barcode' => 'required'
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		  'organisation_id.required' => 'The Organisation field is required',
		  'barcode.required' => 'The Code field is required'
    ];

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

      if ($orgID){
        $view['slings'] = Sling::with('organisation', 'location')->where('organisation_id', $orgID)->get();
      } else {
        $view['slings'] = Sling::with('organisation', 'location')->get();
      }

      $user = Sentinel::getUser();
      $view['user']= $user;
      
      return view('admin.slings.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
      $view['organisations'] = Organisation::dropdownList();
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
      $view['locations'] = Location::dropdownList($orgID);
      $view['types'] = ['webbing-loops-sling' => 'Webbing Loops Sling', 'keyhole-plate-sling' => 'Keyhole Plate Sling', 'other-sling' => 'Other Sling'];
      $view['units'] = ['kg' => 'kg', 'lbs' => 'lbs'];
      return view('admin.slings.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

      $sling = new Sling();
      $sling->fill($request->all());
      $sling->save();

      return Redirect::to('slings')->with('success', 'New Sling Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
      $view['sling'] = Sling::with('organisation', 'location')->find($id);
      $view['inspections'] = Inspection::where('sling_id', $id)->where('completed', 1)->orderBy('inspection_date', 'DESC')->get();
      $view['washes'] = Wash::where('sling_id', $id)->orderBy('wash_date', 'DESC')->get();
      $view['repairs'] = Repair::where('sling_id', $id)->orderBy('repair_date', 'DESC')->get();
      return view('admin.slings.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
      $view['sling'] = Sling::find($id);
      $view['organisations'] = Organisation::dropdownList();
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
      $view['locations'] = Location::dropdownList($orgID);
      $view['types'] = ['webbing-loops-sling' => 'Webbing Loops Sling', 'keyhole-plate-sling' => 'Keyhole Plate Sling', 'other-sling' => 'Other Sling'];
      $view['units'] = ['kg' => 'kg', 'lbs' => 'lbs'];
      return view('admin.slings.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
      $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

      $location = Sling::find($id);
      $location->fill($request->all());
      $location->save();

      return Redirect::to('slings')->with('success', 'Sling Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id){
      $sling = Sling::find($id);
      if($sling->delete()){
        DB::table('slings')->where('id', $sling->id)->update(array('deleted_by' => filter_var ($request->deleted_by, FILTER_SANITIZE_STRING) ,'delete_reason' => filter_var( $request->delete_reason, FILTER_SANITIZE_STRING)));
      }

      return Redirect::to('slings')->with('success', 'Sling Deleted Successfully');
    }

	public function newInspection($id){
		$view['sling'] = Sling::find($id);
		$view['sling_id'] = $view['sling']->id;
		$view['organisation_id'] = $view['sling']->organisation_id;
		$view['headings'] = Question::select('heading')->where('inspection_type', $view['sling']->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['questions'] = Question::where('inspection_type', $view['sling']->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->get();
    return view('admin.slings.inspections.form', $view);
	}

  public function new3rdPartyInspection($id=null){

    if ($id){
      $view['sling'] = Sling::find($id);
      $view['sling_id'] = $view['sling']->id;
      $view['organisation_id'] = $view['sling']->organisation_id;
    } else {
      $view['organisations'] = Organisation::dropdownList();
		  $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		  $view['slings'] = Sling::dropdownList($orgID);
      $view['organisation_id'] = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
    }
    
    return view('admin.slings.inspections.third-party', $view);
  }

  public function save3rdPartyInspection(Request $request){
    
    $file = $request->file('document');
    $filename = time() . '.' . $file->getClientOriginalExtension();
    $s3 = Storage::disk('s3');
    $filePath = '/slingspector-com/app/inspections/' . $filename;
    
    if ($s3->put($filePath, file_get_contents($file), 'public')){
      $sling = Sling::find($request->get('sling_id'));
      $inspection = new Inspection;
      $inspection->sling_id = $sling->id;
      $inspection->organisation_id = $request->get('organisation_id');
      $inspection->user_id = Sentinel::getUser()->id;
      $inspection->type = $sling->type;
      $inspection->third_party_document = $filename;
      $inspection->inspection_date = Carbon::createFromFormat('d/m/Y', $request->get('inspection_date'))->format('Y-m-d');
      $inspection->completed = 1;
      $inspection->save();

      return Redirect::to('slings/'.$request->get('sling_id'))->with('success', '3rd-Party Inspection Saved Successfully');
    } else {
      return Redirect::to('slings/'.$request->get('sling_id').'/3rd-party-inspection')->with('error', 'Unable to save inspection');
    }
  
  }

	public function newWash($id){
		$sling = Sling::find($id);
		$view['sling'] = $sling;
		$view['organisation'] = $sling->organisation;
    return view('admin.slings.washes.form', $view);
	}

	public function newRepair($id){
		$sling = Sling::find($id);
		$view['sling'] = $sling;
		$view['organisation'] = $sling->organisation;
    return view('admin.slings.repairs.form', $view);
	}
}
