<?php

namespace App\Http\Controllers\Admin;

use App\Models\Hoist;
use App\Models\Location;
use App\Models\Organisation;
use App\Models\Question;
use App\Models\Inspection;
use App\Models\Repair;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Sentinel;
use DB;
use Storage;
use Carbon\Carbon;

class HoistsController extends Controller
{
	/**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		'organisation_id' => 'required',
		'barcode' => 'required'
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		'organisation_id.required' => 'The Organisation field is required',
		'barcode.required' => 'The Code field is required'
    ];

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

      if ($orgID){
        $view['hoists'] = Hoist::with('organisation', 'location')->where('organisation_id', $orgID)->get();
      } else {
        $view['hoists'] = Hoist::with('organisation', 'location')->get();
      }

		  $user = Sentinel::getUser();
		  $view['user']= $user;

      return view('admin.hoists.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$view['organisations'] = Organisation::dropdownList();
		$orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
    $view['locations'] = Location::dropdownList($orgID);
    $view['types'] = ['metal-hook-coat-hanger-hoist' => 'Metal Hook Coat Hanger Hoist', 'keyhole-plate-tilt-frame-hoist' => 'Keyhole Plate Tilt Frame Hoist', 'stand-up-hoist' => 'Stand Up Hoist', 'portable-ceiling-hoist' => 'Portable Ceiling Hoist', 'fixed-ceiling-hoist' => 'Fixed Ceiling Hoist'];
		$view['units'] = ['kg' => 'kg', 'lbs' => 'lbs'];
        return view('admin.hoists.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$hoist = new Hoist();
		$hoist->fill($request->all());
		$hoist->save();

		return Redirect::to('hoists')->with('success', 'New Hoist Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$view['hoist'] = Hoist::with('organisation', 'location')->find($id);
		$view['inspections'] = Inspection::where('hoist_id', $id)->where('completed', 1)->orderBy('inspection_date', 'DESC')->get();
		$view['repairs'] = Repair::where('hoist_id', $id)->orderBy('repair_date', 'DESC')->get();
		return view('admin.hoists.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$view['hoist'] = Hoist::find($id);
		$view['organisations'] = Organisation::dropdownList();
		$orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
    $view['locations'] = Location::dropdownList($orgID);
    $view['types'] = ['metal-hook-coat-hanger-hoist' => 'Metal Hook Coat Hanger Hoist', 'keyhole-plate-tilt-frame-hoist' => 'Keyhole Plate Tilt Frame Hoist', 'stand-up-hoist' => 'Stand Up Hoist', 'portable-ceiling-hoist' => 'Portable Ceiling Hoist', 'fixed-ceiling-hoist' => 'Fixed Ceiling Hoist'];
		$view['units'] = ['kg' => 'kg', 'lbs' => 'lbs'];
        return view('admin.hoists.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$location = Hoist::find($id);
		$location->fill($request->all());
		$location->save();

		return Redirect::to('hoists')->with('success', 'Hoist Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$hoist = Hoist::find($id);
		$hoist->delete();

		return Redirect::to('hoists')->with('success', 'hoist Deleted Successfully');
    }


	public function newRepair($id){
		$hoist = Hoist::find($id);
		$view['hoist'] = $hoist;
		$view['organisation'] = $hoist->organisation;
		return view('admin.hoists.repairs.form', $view);
	}

	public function newInspection($id){
		$hoist = Hoist::find($id);
		$view['hoist_id'] = $hoist->id;
		$view['organisation_id'] = $hoist->organisation_id;
		$view['headings'] = Question::select('heading')->where('inspection_type', $hoist->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['questions'] = Question::where('inspection_type', $hoist->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->get();
        return view('admin.hoists.inspections.form', $view);
	}

  public function new3rdPartyInspection($id=null){

    if ($id){
      $view['hoist'] = Hoist::find($id);
      $view['hoist_id'] = $view['hoist']->id;
      $view['organisation_id'] = $view['hoist']->organisation_id;
    } else {
      $view['organisations'] = Organisation::dropdownList();
		  $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		  $view['hoists'] = Hoist::dropdownList($orgID);
      $view['organisation_id'] = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
    }
    
    return view('admin.hoists.inspections.third-party', $view);
  }

  public function save3rdPartyInspection(Request $request){
    
    $file = $request->file('document');
    $filename = time() . '.' . $file->getClientOriginalExtension();
    $s3 = Storage::disk('s3');
    $filePath = '/slingspector-com/app/inspections/' . $filename;
    
    if ($s3->put($filePath, file_get_contents($file), 'public')){
      $hoist = Hoist::find($request->get('hoist_id'));
      $inspection = new Inspection;
      $inspection->hoist_id = $hoist->id;
      $inspection->organisation_id = $request->get('organisation_id');
      $inspection->user_id = Sentinel::getUser()->id;
      $inspection->type = $hoist->type;
      $inspection->third_party_document = $filename;
      $inspection->inspection_date = Carbon::createFromFormat('d/m/Y', $request->get('inspection_date'))->format('Y-m-d');
      $inspection->completed = 1;
      $inspection->save();

      return Redirect::to('hoists/'.$request->get('hoist_id'))->with('success', '3rd-Party Inspection Saved Successfully');
    } else {
      return Redirect::to('hoists/'.$request->get('hoist_id').'/3rd-party-inspection')->with('error', 'Unable to save inspection');
    }
  
  }
}
