<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class SentinelAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Sentinel::check())
            return Redirect::to('login')->with('info', 'You must be logged in!');
        elseif(!Sentinel::inRole('admin'))
            return Redirect::to('dashboard')->with('info', 'You do not have the required permissions');

        return $next($request);
    }
}
