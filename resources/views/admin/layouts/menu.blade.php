<ul class="site-menu">
	<li class="site-menu-category">General</li>
	<li class="site-menu-item <?php if (Request::segment(1) == 'dashboard'){ echo 'active'; } ?>">
		<a href="/dashboard">
			<i class="site-menu-icon fa-dashboard" aria-hidden="true"></i>
			<span class="site-menu-title">Dashboard</span>
		</a>
	</li>
	<li class="site-menu-item has-sub <?php if (Request::segment(1) == 'slings'){ echo 'active open'; } ?>">
		<a href="javascript:void(0)">
			<i class="site-menu-icon fa-list-ul" aria-hidden="true"></i>
			<span class="site-menu-title">Sling Management</span>
			<span class="site-menu-arrow"></span>
		</a>
		<ul class="site-menu-sub">
			<li class="site-menu-item <?php if (Request::segment(1) == 'slings' && Request::segment(2) != 'inspections' && Request::segment(2) != 'washes' && Request::segment(2) != 'repairs'){ echo 'active'; } ?>">
				<a href="/slings">
					<i class="site-menu-icon fa-circle" aria-hidden="true"></i>
					<span class="site-menu-title">Slings</span>
				</a>
			</li>
			<li class="site-menu-item <?php if (Request::segment(1) == 'slings' && Request::segment(2) == 'inspections'){ echo 'active'; } ?>">
				<a href="/slings/inspections">
					<i class="site-menu-icon fa-search" aria-hidden="true"></i>
					<span class="site-menu-title">Inspections</span>
				</a>
			</li>
			<li class="site-menu-item <?php if (Request::segment(1) == 'slings' && Request::segment(2) == 'repairs'){ echo 'active'; } ?>">
				<a href="/slings/repairs">
					<i class="site-menu-icon fa-medkit" aria-hidden="true"></i>
					<span class="site-menu-title">Service/Repairs</span>
				</a>
			</li>
			<li class="site-menu-item <?php if (Request::segment(1) == 'slings' && Request::segment(2) == 'washes'){ echo 'active'; } ?>">
				<a href="/slings/washes">
					<i class="site-menu-icon fa-refresh" aria-hidden="true"></i>
					<span class="site-menu-title">Washes</span>
				</a>
			</li>
		</ul>
	</li>

	<li class="site-menu-item has-sub <?php if (Request::segment(1) == 'hoists'){ echo 'active open'; } ?>">
		<a href="javascript:void(0)">
			<i class="site-menu-icon fa-list-ul" aria-hidden="true"></i>
			<span class="site-menu-title <?php if (Request::segment(1) == 'hoists'){ echo 'active'; } ?>">Hoist Management</span>
				<span class="site-menu-arrow"></span>
			</a>
			<ul class="site-menu-sub">
				<li class="site-menu-item <?php if (Request::segment(1) == 'hoists'){ echo 'active '; } ?>">
					<a href="/hoists">
						<i class="site-menu-icon fa-circle" aria-hidden="true"></i>
						<span class="site-menu-title">Hoists</span>
					</a>
				</li>
				<li class="site-menu-item <?php if (Request::segment(1) == 'hoists' && Request::segment(2) == 'inspections'){ echo 'active'; } ?>">
					<a href="/hoists/inspections">
						<i class="site-menu-icon fa-search" aria-hidden="true"></i>
						<span class="site-menu-title">Inspections</span>
					</a>
				</li>
				<li class="site-menu-item <?php if (Request::segment(1) == 'hoists' && Request::segment(2) == 'repairs'){ echo 'active'; } ?>">
					<a href="/hoists/repairs">
						<i class="site-menu-icon fa-medkit" aria-hidden="true"></i>
						<span class="site-menu-title">Service/Repairs</span>
					</a>
				</li>
			</ul>
		</li>
		<?php if (!Sentinel::getUser()->inRole('employee')){ ?>
			<li class="site-menu-item has-sub <?php if( (Request::segment(1) == 'organisations')||(Request::segment(1) == 'locations')||(Request::segment(1) == 'users')||(Request::segment(1) == 'transactions')){ echo 'active open'; } ?>">
				<a href="javascript:void(0)">
					<i class="site-menu-icon wb-table" aria-hidden="true"></i>
					<span class="site-menu-title">Management</span>
					<span class="site-menu-arrow"></span>
				</a>
				<ul class="site-menu-sub">
						<?php if (Sentinel::getUser()->inRole('admin')){ ?>
							<li class="site-menu-item <?php if (Request::segment(1) == 'organisations'){ echo 'active'; } ?>">
								<a href="/organisations">
									<i class="site-menu-icon fa-university" aria-hidden="true"></i>
									<span class="site-menu-title">Organisations</span>
								</a>
							</li>
						<?php } ?>
						<?php if (Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->inRole('manager')){ ?>
						<li class="site-menu-item <?php if (Request::segment(1) == 'locations'){ echo 'active'; } ?>">
							<a href="/locations">
								<i class="site-menu-icon fa-map-marker" aria-hidden="true"></i>
								<span class="site-menu-title">Facilities</span>
							</a>
						</li>
						<?php } ?>
						<?php if (Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->inRole('manager')){ ?>
							<li class="site-menu-item <?php if (Request::segment(1) == 'users'){ echo 'active'; } ?>">
								<a href="/users">
									<i class="site-menu-icon fa-users" aria-hidden="true"></i>
									<span class="site-menu-title">Users</span>
								</a>
							</li>
						<?php } ?>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?>
							<li class="site-menu-item <?php if (Request::segment(1) == 'transactions'){ echo 'active'; } ?>">
								<a href="/transactions">
									<i class="site-menu-icon fa-credit-card-alt" aria-hidden="true"></i>
									<span class="site-menu-title">Transactions</span>
								</a>
							</li>
						<?php } ?>
				</ul>
			</li>
		<?php } ?>
		<li class="site-menu-item has-sub <?php if( (Request::segment(1) == 'help-centre')){ echo 'active open'; } ?>">
			<a href="javascript:void(0)">
				<i class="site-menu-icon fa-question-circle" aria-hidden="true"></i>
				<span class="site-menu-title">Help Centre</span>
				<span class="site-menu-arrow"></span>
			</a>
			<ul class="site-menu-sub">
				<li class="site-menu-item <?php if (Request::segment(1) == 'help-centre' && Request::segment(2) == 'training-videos'){ echo 'active'; } ?>">
					<a href="/help-centre/training-videos">
						<i class="site-menu-icon fa-video-camera" aria-hidden="true"></i>
						<span class="site-menu-title">Training Videos</span>
					</a>
				</li>
				<li class="site-menu-item <?php if (Request::segment(1) == 'help-centre' && Request::segment(2) == 'how-to-guide'){ echo 'active'; } ?>">
					<a href="/help-centre/how-to-guide">
						<i class="site-menu-icon fa-list-ol" aria-hidden="true"></i>
						<span class="site-menu-title">How To Guide</span>
					</a>
				</li>
				<li class="site-menu-item <?php if (Request::segment(1) == 'help-centre' && Request::segment(2) == 'how-to-videos'){ echo 'active'; } ?>">
					<a href="/help-centre/how-to-videos">
						<i class="site-menu-icon fa-play-circle" aria-hidden="true"></i>
						<span class="site-menu-title">How To Videos</span>
					</a>
				</li>
				<li class="site-menu-item <?php if (Request::segment(1) == 'help-centre' && Request::segment(2) == 'reference'){ echo 'active'; } ?>">
					<a href="/help-centre/reference">
						<i class="site-menu-icon fa-book" aria-hidden="true"></i>
						<span class="site-menu-title">Reference</span>
					</a>
				</li>
			</ul>
		</li>
	</ul>
