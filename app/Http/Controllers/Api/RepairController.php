<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use App\Models\Repair;
use App\Models\Sling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class RepairController extends ApiController
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        //
		$repairs = $id ? Sling::find($id)->repair : Repair::all();
		return $this->respond([
			'data'=> $repairs->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $repair_sp = explode("T", $request->get('data')['repair_date']);
        $repair_dt = Carbon::createFromFormat('Y-m-d', $repair_sp[0])->format('d/m/Y');

        $completed_sp = explode("T", $request->get('data')['repair_completed']);
        if (isset($completed_sp[0])){
            $completed_dt = Carbon::createFromFormat('Y-m-d', $completed_sp[0])->format('d/m/Y');
        } else {
            $completed_dt = null;
        }

		$repair = new Repair;

        if ($request->get('type') == 'sling'){
            $repair->sling_id = $request->get('id');
            $repair->hoist_id = NULL;
        } else if ($request->get('type') == 'hoist'){
            $repair->hoist_id = $request->get('id');
            $repair->sling_id = NULL;
        }
        
        $repair->user_id = $request->get('user');
        $repair->repair_date = $repair_dt;
        $repair->repair_complete = $completed_dt;
        $repair->repair_by = $request->get('data')['repair_by'];
        $repair->notes = $request->get('data')['notes'];
		$repair->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $repair->id,
			'message'=> 'Repair succesfully created',
			'success'=> true
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$repair = Repair::find($id);
		if (!$repair)
		{
			return $this->respondNotFound('Repair Not Found');
		}
		else {
			return $this->respond(['data'=>$repair,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		try{
            if ($repair = Repair::find($id)){
                $repair->update(Input::all());
                $repair->save();
                return $this->respond(['success' => true, 'message' => 'Repair information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update repair id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		try{
			if ($repair = Repair::find($id)){
				$repair->delete();
				return $this->respondDeleted('Repair Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
    }
}
