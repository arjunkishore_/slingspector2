<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="description" content="SlingSpector">
		<meta name="author" content="Ryan Bown - Roobix Pty Ltd">
		<meta name="format-detection" content="telephone=no">
		<link rel="icon" href="/favicon.png">
		<title>@yield('title') | SlingSpector</title>
		<!-- Stylesheets -->
		<link rel="stylesheet" href="/assets/global/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/global/css/bootstrap-extend.min.css">
		<link rel="stylesheet" href="/assets/base/css/site.min.css">
		<link rel="stylesheet" href="/assets/base/skins/grey.min.css">
		<!-- Plugins -->
		<link rel="stylesheet" href="/assets/global/vendor/animsition/animsition.css">
		<link rel="stylesheet" href="/assets/global/vendor/asscrollable/asScrollable.css">
		<link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
		<link rel="stylesheet" href="/assets/global/vendor/intro-js/introjs.css">
		<link rel="stylesheet" href="/assets/global/vendor/slidepanel/slidePanel.css">
		<link rel="stylesheet" href="/assets/global/vendor/flag-icon-css/flag-icon.css">
		<link rel="stylesheet" href="/assets/global/vendor/chartist-js/chartist.css">
		<link rel="stylesheet" href="/assets/global/vendor/jvectormap/jquery-jvectormap.css">
		<link rel="stylesheet" href="/assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
@yield('css')
		<link rel="stylesheet" href="/assets/base/css/custom.css">
		<!-- Fonts -->
		<link rel="stylesheet" href="/assets/global/fonts/weather-icons/weather-icons.css">
		<link rel="stylesheet" href="/assets/global/fonts/web-icons/web-icons.min.css">
		<link rel="stylesheet" href="/assets/global/fonts/brand-icons/brand-icons.min.css">
		<link rel="stylesheet" href="/assets/global/fonts/font-awesome/font-awesome.min.css">
		<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
		<!--[if lt IE 9]>
		<script src="/assets/global/vendor/html5shiv/html5shiv.min.js"></script>
		<![endif]-->
		<!--[if lt IE 10]>
		<script src="/assets/global/vendor/media-match/media.match.min.js"></script>
		<script src="/assets/global/vendor/respond/respond.min.js"></script>
		<![endif]-->
		<!-- Scripts -->
		<script src="/assets/global/vendor/modernizr/modernizr.js"></script>
		<script src="/assets/global/vendor/breakpoints/breakpoints.js"></script>
		<script>Breakpoints();</script>
	</head>
	<body class="@yield('bodyclass')">
	  <!--[if lt IE 8]>
	        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	    <![endif]-->
	  	<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
	    	<div class="navbar-header">
		      	<button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided" data-toggle="menubar">
		        	<span class="sr-only">Toggle navigation</span>
		        	<span class="hamburger-bar"></span>
		      	</button>
		      	<button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
		        	<i class="icon wb-more-horizontal" aria-hidden="true"></i>
		      	</button>
		      	<div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
					<img id="brand-logo" src="/assets/global/images/ss-icon.png" title="Slingspector">
		        	<img class="navbar-brand-logo" src="/assets/global/images/ss-logo.png" title="Slingspector">
		      	</div>
	    	</div>
	    	<div class="navbar-container container-fluid">
	      		<!-- Navbar Collapse -->
	      		<div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
	        		<!-- Navbar Toolbar -->
	        		<ul class="nav navbar-toolbar">
	          			<li class="hidden-float" id="toggleMenubar">
	            			<a data-toggle="menubar" href="#" role="button">
	              				<i class="icon hamburger hamburger-arrow-left">
	                  				<span class="sr-only">Toggle menubar</span>
	                  				<span class="hamburger-bar"></span>
	                			</i>
	            			</a>
	          			</li>
	        		</ul>
	      		</div>
	      		<!-- End Navbar Collapse -->
	      		<!-- Site Navbar Seach -->
	      		<div class="collapse navbar-search-overlap" id="site-navbar-search">
		        	<form role="search">
		          		<div class="form-group">
		            		<div class="input-search">
		              			<i class="input-search-icon wb-search" aria-hidden="true"></i>
		              			<input type="text" class="form-control" name="site-search" placeholder="Search...">
		              			<button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search" data-toggle="collapse" aria-label="Close"></button>
		            		</div>
		          		</div>
		        	</form>
		      	</div>
	      		<!-- End Site Navbar Seach -->
	    	</div>
	  	</nav>
	  	<div class="site-menubar">
	    	<div class="site-menubar-body">
	      		<div>
	        		<div>
	          			@include('admin.layouts.menu')
	        		</div>
	      		</div>
	    	</div>
	    	<div class="site-menubar-footer">
				<a href="/staff/edit/<?php //Auth::user()->id; ?>/<?php //str_slug(Auth::user()->first, '-').'-'.str_slug(Auth::user()->last, '-'); ?>" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Settings">
					<span class="icon wb-settings" aria-hidden="true"></span>
				</a>
				<a href="/logout" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
					<span class="icon wb-power" aria-hidden="true"></span>
				</a>
	    	</div>
	  	</div>
	  	<!-- Page -->
	  	<div class="page animsition">
	    	@yield('page-header')
	    	<div class="page-content container-fluid">
	      		@yield('content')
	    	</div>
	  	</div>

		<!-- Modal -->
		<div class="modal fade" id="modalContainer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <!-- Modal Header -->
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">
		                    <span aria-hidden="true">&times;</span>
		                    <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title" id="myModalLabel"></h4>
		            </div>
		            <!-- Modal Body -->
		            <div class="modal-body">
		                <form role="form" action="" method="post" id="modalForm">

		                </form>
		            </div>
		            <!-- Modal Footer -->
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">
		                    Close
		                </button>
		                <button type="button" class="btn btn-primary modal-submit">
		                    Save
		                </button>
		            </div>
		        </div>
		    </div>
		</div>
	  	<!-- End Page -->
	  	<!-- Footer -->
	  	<footer class="site-footer">
	    	<div class="site-footer-legal">© <?= date("Y"); ?> Arizon Healthcare</div>
	    	<div class="site-footer-right">
	      		&nbsp;
	    	</div>
	  	</footer>
		<!-- Core  -->
		<script src="/assets/global/vendor/jquery/jquery.js"></script>
		<script src="/assets/global/vendor/bootstrap/bootstrap.js"></script>
		<script src="/assets/global/vendor/animsition/animsition.js"></script>
		<script src="/assets/global/vendor/asscroll/jquery-asScroll.js"></script>
		<script src="/assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
		<script src="/assets/global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
		<script src="/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
		<!-- Plugins -->
		<script src="/assets/global/vendor/switchery/switchery.min.js"></script>
		<script src="/assets/global/vendor/intro-js/intro.js"></script>
		<script src="/assets/global/vendor/screenfull/screenfull.js"></script>
		<script src="/assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
		<script src="/assets/global/vendor/skycons/skycons.js"></script>
		<script src="/assets/global/vendor/chartist-js/chartist.min.js"></script>
		<script src="/assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
		<!-- Scripts -->
		<script src="/assets/global/js/core.js"></script>
		<script src="/assets/base/js/site.js"></script>
		<script src="/assets/base/js/sections/menu.js"></script>
		<script src="/assets/base/js/sections/menubar.js"></script>
		<script src="/assets/base/js/sections/gridmenu.js"></script>
		<script src="/assets/base/js/sections/sidebar.js"></script>
		<script src="/assets/global/js/components/asscrollable.js"></script>
		<script src="/assets/global/js/components/animsition.js"></script>
		<script src="/assets/global/js/components/slidepanel.js"></script>
		<script src="/assets/global/js/components/switchery.js"></script>
		<script src="/assets/global/js/components/matchheight.js"></script>
		<script src="/assets/global/js/plugins/responsive-tabs.js"></script>
		<script src="/assets/global/js/plugins/closeable-tabs.js"></script>
		<script src="/assets/global/js/components/tabs.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
@yield('javascript')
		<script src="/assets/base/js/custom.js"></script>
@yield('page-javascript')

		<meta name="csrf-token" content="{{ csrf_token() }}">
		<div class="modal fade modal-danger in" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4>Confirm Delete</h4>
					</div>
					<div class="modal-body">
						<div class="form-group form-material" data-plugin="formMaterial">
		                  <label class="form-control-label" for="delete_reason">Reason for Deleting</label>
		                  <textarea class="form-control" id="delete_reason" name="textarea" rows="3"></textarea>
		                </div>
						<div class="form-group form-material" data-plugin="formMaterial">
		                  <label class="form-control-label" for="deleted_by">Your Name</label>
		                  <input type="text" class="form-control" id="deleted_by" name="deleted_by" value="<?= Sentinel::getUser()->first_name.' '.Sentinel::getUser()->last_name; ?>"/>
		                </div>
						Are you sure you want to delete this <span class="delete-type"></span>?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger btn-ok" href="#" data-method="delete" data-row="">Delete</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
