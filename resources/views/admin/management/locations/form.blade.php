@extends('admin.layouts.master')

@section('bodyclass', 'locations')

@if (isset($location))
	@section('title', 'Edit Location')
@else
	@section('title', 'New Location')
@endif

@section('page-header')
	<div class="page-header">
		@if (isset($location))
  			<h1 class="page-title">Edit Location</h1>
		@else
			<h1 class="page-title">New Location</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/locations">Facilities</a></li>
			@if (isset($location))
				<li class="breadcrumb-item active">Edit Location</li>
			@else
				<li class="breadcrumb-item active">New Location</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($location))
						{{ Form::model($location, array('url' => 'locations/'.$location->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'locations', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif

						<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						<div class="form-group required row {{ $errors->has('organisation_id') ? 'has-error' :'' }}">
							{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
							</div>
						</div>
						<?php } else { ?>
							<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
						<?php } ?>

						<div class="form-group required {{ $errors->has('name') ? 'has-error' :'' }}">
							{{ Form::label('name', 'Name', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('name', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
