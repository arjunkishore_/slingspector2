<?php

namespace App\Http\Controllers\Admin;

use App\Models\Repair;
use App\Models\Hoist;
use App\Models\Organisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Sentinel;
use DB;
class HoistRepairsController extends Controller
{
	/**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		'hoist_id' => 'required',
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		'hoist_id.required' => 'The Hoist field is required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

      if ($orgID){
        $view['repairs'] = Repair::with([ 'hoist' => function($query) use ($orgID) {
          $query->where('organisation_id', $orgID);
        }])->get();
      } else {
        $view['repairs'] = Repair::with('hoist.organisation')->get();
      }

		  $user = Sentinel::getUser();
		  $view['user']= $user;
      return view('admin.hoists.repairs.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$view['organisations'] = Organisation::dropdownList();
    $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		$view['hoists'] = Hoist::dropdownList($orgID);
        return view('admin.hoists.repairs.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$repair = new Repair();
		$repair->fill($request->all());
		$repair->user_id = Sentinel::getUser()->id;
		$repair->save();

		return Redirect::to('hoists/repairs')->with('success', 'New Repair Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$view['repair'] = Repair::find($id);
		return view('admin.hoists.repairs.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$view['repair'] = Repair::find($id);
		$view['organisations'] = Organisation::dropdownList();
		$orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		$view['hoists'] = Hoist::dropdownList($orgID);
        return view('admin.hoists.repairs.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$repair = Repair::find($id);
		$repair->fill($request->all());
		$repair->user_id = Sentinel::getUser()->id;
		$repair->save();

		return Redirect::to('hoists/repairs')->with('success', 'Repair Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
		$repair = Repair::find($id);
		if( $repair->delete() ){
			DB::table('repairs')->where('id', $repair->id)
				->update(array('deleted_by' => filter_var ($request->deleted_by, FILTER_SANITIZE_STRING) ,'delete_reason' => filter_var( $request->delete_reason, FILTER_SANITIZE_STRING)));
		}
		return Redirect::to('hoists/repairs')->with('success', 'Repair Deleted Successfully');
    }
}
