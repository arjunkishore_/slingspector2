@extends('admin.layouts.master')

@section('bodyclass', 'hoists')

@section('title', 'Hoists')

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Hoists</h1>
  		<div class="page-header-actions">
	  		<a href="/hoists/create" class="btn btn-slingspector">Create New Hoist Profile</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item active">Hoists</li>
		</ol>
	</div>
@stop

@section('content')
	<!-- Panel -->
	<div class="message-holder">
		@include('notifications')
	</div>

	<div class="panel">
	  	<!--
		<header class="panel-heading">
			<h3 class="panel-title">Individual Column Searching</h3>
	  	</header>
		-->
	  	<div class="panel-body">
			<table class="table table-hover dataTable table-striped table-search w-full" id="hoistsTable">
		  		<thead>
					<tr>
						<th>Code</th>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?><th>Organisation</th><?php } ?>
						<th>Facility</th>
						<th>Last Inspection</th>
						<th class="no-sort">Actions</th>
					</tr>
		  		</thead>
		  		<!--
				<tfoot>
					<tr>
						<th>Code</th>
						<th>Organisation</th>
						<th>Location</th>
						<th>Last Inspection</th>
						<th>Actions</th>
					</tr>
		  		</tfoot>
				-->
		  		<tbody>
					<?php
						if (isset($hoists)){
							foreach ($hoists as $hoist){
								echo '<tr id="'.$hoist->id.'">';
									echo '<td>'.$hoist->barcode.'</td>';
									if (Sentinel::getUser()->inRole('admin')){ echo '<td>'.$hoist->organisation['name'].'</td>'; }
									echo '<td>'.$hoist->location['name'].'</td>';
									if($hoist->last_inspection !='Never'){
									$dt = Carbon::createFromFormat('d/m/Y', $hoist->last_inspection);
									echo '<td>'.$dt->format('M d, Y').'</td>';
									} else {  echo '<td>'.$hoist->last_inspection.'</td>';}
									echo '<td>';
										echo '<a href="/hoists/'.$hoist->id.'" class="btn btn-xs btn-slingspector action">View</a>';
										echo '<a href="/hoists/'.$hoist->id.'/edit" class="btn btn-xs btn-primary action">Edit</a>';
										echo '<button data-href="/hoists/'.$hoist->id.'" data-toggle="modal" data-target="#confirm-delete" data-username="'.$user->first_name." ".$user->last_name.'" data-row="'.$hoist->id.'" data-type="Hoist" class="btn btn-xs btn-danger action delete">Delete</button>';
									echo '</td>';
								echo '</tr>';
							}
						}
					?>
		  		</tbody>
			</table>
	  	</div>
	</div>
	<!-- End Panel -->
@stop
