@extends('admin.layouts.master')

@section('bodyclass', 'users')

@section('title', 'Users')

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Users</h1>
  		<div class="page-header-actions">
	  		<a href="/users/create" class="btn btn-slingspector">Add New</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
			<li class="breadcrumb-item">Management</li>
	        <li class="breadcrumb-item active">Users</li>
		</ol>
	</div>
@stop

@section('content')
	<!-- Panel -->
	<div class="message-holder">
		@include('notifications')
	</div>

	<div class="panel">
	  	<!--
		<header class="panel-heading">
			<h3 class="panel-title">Individual Column Searching</h3>
	  	</header>
		-->
	  	<div class="panel-body">
			<table class="table table-hover dataTable table-striped w-full table-search" id="usersTable">
		  		<thead>
					<tr>
						<th>Name</th>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?><th>Organisation</th><?php } ?>
						<th>Email</th>
						<th>Phone</th>
						<th>Role</th>
						<th class="no-sort">Actions</th>
					</tr>
		  		</thead>
		  		<!--
				<tfoot>
					<tr>
						<th>Organisation</th>
						<th>Name</th>
						<th>Actions</th>
					</tr>
		  		</tfoot>
				-->
		  		<tbody>
					<?php
						if (isset($users)){
							foreach ($users as $user){
								echo '<tr id="'.$user->id.'">';
									echo '<td>'.$user->first_name.' '.$user->last_name.'</td>';
									if (Sentinel::getUser()->inRole('admin')){ echo '<td>'.$user->organisation['name'].'</td>'; }
									echo '<td>'.$user->email.'</td>';
									echo '<td>'.$user->phone.'</td>';
									echo '<td>'.$user->role->name.'</td>';
									echo '<td>';
										echo '<a href="/users/'.$user->id.'/edit" class="btn btn-xs btn-primary action">Edit</a>';
										echo '<button data-href="/users/'.$user->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$user->id.'" data-type="User" class="btn btn-xs btn-danger action delete">Delete</button>';
									echo '</td>';
								echo '</tr>';
							}
						}
					?>
		  		</tbody>
			</table>
	  	</div>
	</div>
	<!-- End Panel -->
@stop
