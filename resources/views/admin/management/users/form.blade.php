@extends('admin.layouts.master')

@section('bodyclass', 'users')

@if (isset($user))
	@section('title', 'Edit User')
@else
	@section('title', 'New User')
@endif

@section('page-header')
	<div class="page-header">
		@if (isset($user))
  			<h1 class="page-title">Edit User</h1>
		@else
			<h1 class="page-title">New User</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/users">Users</a></li>
			@if (isset($user))
				<li class="breadcrumb-item active">Edit User</li>
			@else
				<li class="breadcrumb-item active">New User</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($user))
						{{ Form::model($user, array('url' => 'users/'.$user->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'users', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif

				    	<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						<div class="form-group row {{ $errors->has('organisation_id') ? 'has-error' :'' }}">
							{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::select('organisation_id', [null => ''] + $organisations, $orgID, array('class' => 'form-control', 'autocomplete' => 'off')) }}
							</div>
						</div>
						<?php } else { ?>
							<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
						<?php } ?>
						<div class="form-group required row {{ $errors->has('role') ? 'has-error' :'' }}">
							{{ Form::label('role', 'User Role', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::select('role', [null => ''] + $roles, $slug, array('class' => 'form-control', 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="form-group required row {{ $errors->has('first_name') ? 'has-error' :'' }}">
							{{ Form::label('first_name', 'First Name', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('first_name', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group required row {{ $errors->has('last_name') ? 'has-error' :'' }}">
							{{ Form::label('last_name', 'Surname', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('last_name', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group required row {{ $errors->has('email') ? 'has-error' :'' }}">
							{{ Form::label('email', 'Email', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('email', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('password') ? 'has-error' :'' }}">
							{{ Form::label('password', 'Password', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
							  	<input type="password" class="form-control" autocomplete="off" name="password" />
								<small class="text-help">{{ $errors->first('password', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('phone') ? 'has-error' :'' }}">
							{{ Form::label('phone', 'Phone', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('phone', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
