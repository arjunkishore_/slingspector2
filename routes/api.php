<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, X-Location-Type');

Route::group(['prefix'=>'v1'],
function(){
	//Route::get('/user', function (Request $request) { return $request->user(); })->middleware('auth:api');

	Route::post('auth','Api\AuthController@postLogin');

	/** Get Organisations related entities **/
	Route::get('organisations/{id}/locations','Api\LocationController@index');
	Route::get('organisations/{id}/slings','Api\SlingController@index');

	/** Get Slings related entities **/
	Route::get('slings/{id}/repairs','Api\RepairController@index');
	Route::get('slings/{id}/washes','Api\WashController@index');
	Route::get('slings/{id}/inspections','Api\InspectionController@index');
	Route::get('slings/{id}/inspection', 'Api\SlingInspectionsController@create');
	Route::post('slings/{id}/inspection', 'Api\SlingInspectionsController@store');

	Route::get('slings/find/{code}', 'Api\SlingController@find');

	/** Get Hoists related entities **/
	Route::get('hoists/{id}/repairs','Api\RepairController@index');
	Route::get('hoists/{id}/washes','Api\WashController@index');
	Route::get('hoists/{id}/inspections','Api\InspectionController@index');
	Route::get('hoists/{id}/inspection', 'Api\HoistInspectionsController@create');
	Route::post('hoists/{id}/inspection', 'Api\HoistInspectionsController@store');

	Route::get('hoists/find/{code}', 'Api\HoistController@find');

	Route::get('questions/types/{type}','Api\QuestionController@types');
	Route::get('questions/{id}/answers','Api\AnswerController@index');
	Route::get('questions/form','Api\QuestionController@form');

	Route::get('inspections/{id}/answers','Api\InspectionController@answers');

	Route::resource('organisations','Api\OrganisationController');
	Route::resource('locations','Api\LocationController');
	Route::resource('slings','Api\SlingController');
	Route::resource('hoists','Api\HoistController');
	Route::resource('washes','Api\WashController');
	Route::resource('repairs','Api\RepairController');
	Route::resource('inspections','Api\InspectionController');
	Route::resource('questions','Api\QuestionController');
	Route::resource('answers','Api\AnswerController');
});
