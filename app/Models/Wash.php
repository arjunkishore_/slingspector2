<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wash extends Model
{
	use UuidModelTrait;
	use SoftDeletes;

	protected $fillable = ['sling_id', 'wash_date', 'photos', 'notes','delete_reason','deleted_by'];
	public $timestamps = false;

	public function sling()
	{
		return $this->belongsTo('App\Models\Sling');
	}


	/**
     * Get the wash_date from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getWashDateAttribute($value)
    {
		$split = explode('-', $value);
		return $split[2].'/'.$split[1].'/'.$split[0];
    }

	public function setWashDateAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);
			$this->attributes['wash_date'] = $split[2].'-'.$split[1].'-'.$split[0];
		} else {
			$this->attributes['wash_date'] = $value;
		}
    }

}
