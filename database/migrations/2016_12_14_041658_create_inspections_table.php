<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('inspections', function (Blueprint $table) {
				$table->engine = 'InnoDB';
	            $table->uuid('id')->index();
				$table->uuid('organisation_id')->index();
				$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
	            $table->uuid('sling_id')->index()->nullable();
				$table->foreign('sling_id')->references('id')->on('slings')->onDelete('cascade');
				$table->uuid('hoist_id')->index()->nullable();
				$table->foreign('hoist_id')->references('id')->on('hoists')->onDelete('cascade');
	            $table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
				$table->string('type');
				$table->string('third_party_document')->nullable();
				$table->date('inspection_date');
				$table->boolean('completed');
				$table->timestamp('created_at');
				$table->timestamp('updated_at');
				$table->string('delete_reason');
				$table->string('deleted_by');
				$table->dateTime('deleted_at');
	            $table->primary('id');

	        });

	    }

	    /**
	     * Reverse the migrations.
	     *
	     * @return void
	     */
	    public function down()
	    {
	        //
			Schema::drop('inspections');
	    }
	}
