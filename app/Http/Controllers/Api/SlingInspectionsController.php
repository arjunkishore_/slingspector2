<?php

namespace App\Http\Controllers\Api;

use App\Models\Sling;
use App\Models\Location;
use App\Models\Organisation;
use App\Models\Inspection;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Carbon\Carbon;
use Sentinel;
use DB;

class SlingInspectionsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = Sentinel::getUser();
		$view['user']= $user;

		$inspections = Inspection::whereIn('type', ['webbing-loops-sling', 'keyhole-plate-sling', 'other-sling'])->with('sling', 'organisation')->get();

		foreach ($inspections as $inspection){
			$l = Location::find($inspection->sling['location_id']);

			if (isset($l)){
				$inspection->location_name = $l->name;
			}
		}

		return $this->respond([
			'data'=> $inspections,
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, Request $request)
    {
		$sling = Sling::find($id);
		$data['headings'] = Question::select('heading')->where('inspection_type', $sling->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$data['questions'] = Question::where('inspection_type',  $sling->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->get();
		$data['type'] = $sling->type;

		foreach ($data['questions'] as $key => $question){
			$skip = false;

			if ($question->restriction){
				$explode = explode('|', $question->restricted);
				
				if (!in_array($request->header('X-Location-Type'), $explode) && $question->restricted != $request->header('X-Location-Type')){
					unset($data['questions'][$key]);
					$skip = true;
				}
			}

			if (!$skip){
				$sp = explode("|", $question->input_options);
				$question->input_options = $sp;
				$question->safe_id = str_replace("-", "_", $question->id);

				if ($question->question == 'Next Inspection Due'){
					$question->default = Carbon::now()->addMonths(6)->format('Y-m-d');
				} else {
					$question->default = '';
				}
			}
		}

		return $this->respond([
			'data'=> $data,
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $posts = Input::all();
		$answers = array();

		// Get Sling
		$sling = Sling::find($id);

		foreach ($posts['answers'] as $post){
			$answers[] = array(
				'question_id'	=> $post['id'],
				'answer'		=> $post['answer'] ? $post['answer'] : '',
				'comments'		=> $post['comments'],
				'photo'			=> $post['photo']
			);
		}

		//print_r($answers);

		if (count($answers > 0)){
			$dt = Carbon::now();

			// Create new inspection
			$inspection = new Inspection;

			$inspection->organisation_id = $sling->organisation_id;
			$inspection->sling_id = $sling->id;
			$inspection->user_id = Input::get('user');
			$inspection->type = Input::get('inspection_type') ? Input::get('inspection_type') : '';
			$inspection->inspection_date = $dt->toDateTimeString();
			$inspection->completed = 1;

			$inspection->save();

			// Save answers to Database
			foreach ($answers as $a){
				$answer = new Answer;
				$answer->inspection_id = $inspection->id;
				$answer->question_id = $a['question_id'];
				$answer->answer = $a['answer'];
				$answer->comments = $a['comments'];
				$answer->photo = $a['photo'];
				$answer->save();
			}

			// Update Sling to set date for last inspection
			$sling = Sling::find($id);
			$sling->last_6month_inspection = $dt->format('Y-m-d');
			$sling->save();

			return $this->respond([
				'data'=> $sling,
				'payload'=> 'Slingspector API v1.0',
				'success' => true
			]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$view['inspection'] = Inspection::with('organisation', 'sling')->find($id);
		$view['headings'] = Question::select('heading')->where('inspection_type', $view['inspection']->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['answers'] = Inspection::getSlingAnswers($id);
		return view('admin.slings.inspections.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$view['inspection'] = Inspection::with('organisation', 'sling')->find($id);
		$view['headings'] = Question::select('heading')->where('inspection_type', $view['inspection'])->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$view['answers'] = Inspection::getSlingAnswers($id);
		return view('admin.slings.inspections.edit', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$post = Input::all();
		$answers = array();

		foreach ($post as $k => $v){
			$split = explode('_', $k);

			if ($split[0] == 'question' && !isset($split[2])){
				$answers[] = array(
					'question_id'	=> $split[1],
					'answer'		=> $v,
					'comments'		=> Input::get('question_'.$split[1].'_comment')
				);
			}
		}

		if (count($answers > 0)){
			$dt = Carbon::now();

			// Create new inspection
			$inspection = new Inspection;

			$inspection->organisation_id = Input::get('organisation_id');
			$inspection->sling_id = Input::get('sling_id');
			$inspection->user_id = Sentinel::getUser()->id;
			$inspection->type = Input::get('inspection_type');
			$inspection->inspection_date = $dt->toDateTimeString();
			$inspection->completed = 1;

			$inspection->save();

			// Save answers to Database
			foreach ($answers as $a){
				$answer = new Answer;
				$answer->inspection_id = $inspection->id;
				$answer->question_id = $a['question_id'];
				$answer->answer = $a['answer'];
				$answer->comments = $a['comments'];
				$answer->save();
			}

			// Update Sling to set date for last inspection
			$sling = Sling::find(Input::get('sling_id'));
			$sling->last_6month_inspection = $dt->toDateString();
			$sling->save();

			return Redirect::to('slings/inspections')->with('success', 'Inspection Saved Successfully');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		$inspection = Inspection::with('organisation', 'sling')->find($id);

		if($inspection->delete()){
			DB::table('inspections')->where('id', $inspection->id)
				->update(array('deleted_by' => filter_var ($request->deleted_by, FILTER_SANITIZE_STRING) ,'delete_reason' => filter_var( $request->delete_reason, FILTER_SANITIZE_STRING)));
		}

		return Redirect::to('slings')->with('success', 'Inspection Deleted Successfully');
    }
}
