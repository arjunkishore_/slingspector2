<?php

use Faker\Factory as Faker;

use App\Models\Hoist;
use App\Models\Location;
use App\Models\Organisation;
use Illuminate\Database\Seeder;

class HoistsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids from organisation and get the array of id using all()
		$organisationIds = Organisation::pluck('id')->all();
		$locationIds	 = Location::pluck('id')->all();

		foreach (range(1,25) as $index) {
			Hoist::create([
				'id' 						=> $faker->uuid,
				//'sling_id'  				=> $faker->regexify('[A-Z0-9]+[A-Z0-9]+[A-Z]{2,4}'),
				'organisation_id'			=> $faker->randomElement($organisationIds),
				//'custom_code'				=> $faker->streetAddress,
				'barcode' 					=> $faker->isbn10,
				'name' 						=> $faker->name,
				'size' 						=> $faker->randomElement(['S', 'M', 'L', 'XL', 'XXL']),
				//'attachments'				=> $faker->md5,
				'type'						=> $faker->randomElement(['stand-up-hoist', 'portable-ceiling-hoist', 'metal-hook-coat-hanger-hoist', 'fixed-ceiling-hoist', 'keyhole-plate-tilt-frame-hoist']),
				'swl'						=> $faker->numberBetween($min = 30, $max = 300),
				'unit' 						=> $faker->randomElement(['kg', 'lbs']),
				'manufacturer'				=> $faker->company,
				'date_of_manufacture' 		=> $faker->dateTimeThisDecade($max = 'now', $timezone = date_default_timezone_get()),
				'date_of_purchase'			=> $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = date_default_timezone_get()),
				'end_of_life'				=> $faker->dateTimeBetween($startDate = 'now', $endDate = '+5 years', $timezone = date_default_timezone_get()),
				'last_beforeuse_inspection'	=> $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'last_6month_inspection'	=> $faker->dateTimeBetween($startDate = '-12 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'last_12month_inspection'	=> $faker->dateTimeBetween($startDate = '-18 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'location_id'				=> $faker->randomElement($locationIds)

			]);
		}

    }
}
