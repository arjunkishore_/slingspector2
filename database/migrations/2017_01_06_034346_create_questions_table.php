<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('questions', function( Blueprint $table){
			$table->uuid('id')->index();
			$table->string('question');
			$table->string('input_type');
			$table->string('input_options')->nullable();
			$table->boolean('before_use_inspection')->default(0);
			$table->boolean('six_month_inspection')->default(0);
			$table->boolean('twelve_month_inspection')->default(0);
			$table->string('inspection_type')->comment = "sling / trolly-hoist / ceiling hoist";
			$table->string('heading');
			$table->string('display_extra_field_on')->nullable()->comment = "Set to the option value for which to show the extra field. eg. On Other so text field";
			$table->string('extra_field_input_type')->nullable()->comment = "text / textarea";
			$table->boolean('photo_option')->default(0);
			$table->boolean('required')->default(0);
			$table->unsignedInteger('ordering');
			$table->string('compliance')->nullable();
			$table->boolean('popup')->default(0);
			$table->text('popup_text')->nullable();
			$table->string('display_popup_on')->nullable()->comment = "Set to the option value for which to show the popup.";
			$table->boolean('is_active')->default(1);
			$table->text('restriction')->nullable()->comment = "Only show this question for specific countries separated by pipe | eg. England|Scotland|Ireland";
			// $table->string('delete_reason');
			// $table->string('deleted_by');
			$table->dateTime('deleted_at');
			$table->timestamps('updated_at');
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('questions');
    }
}
