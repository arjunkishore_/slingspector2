<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Wash;
use App\Models\Sling;

class WashTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_wash(){

		$this->getJson('api/v1/washes');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_wash()
	{
		// check first row
		$wash = $this->getJson('api/v1/washes')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($wash, 'sling_id','notes');
	}

	/** @test */
	public function it_deletes_a_single_wash()
	{
		// check first row id
		$washId = $this->getJson('api/v1/washes')->data[0]->id;

		$response = $this->getJson('api/v1/washes/'.$washId,'DELETE');
		// 202 expected on delete

		$this->assertResponseStatus(202);

	}

	/** @test */
    public function it_404s_if_a_wash_is_not_found()
    {
        $json = $this->getJson('api/v1/washes/asdfas');
        $this->assertResponseStatus(404);

		$this->assertObjectHasAttributes($json, 'error');

    }


	/** @test */
	public function it_updates_a_wash()
	{
		// check first row id
		$washId = $this->getJson('api/v1/washes')->data[0]->id;
		// dd($washId);
		$response = $this->getJson('api/v1/washes/'.$washId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makeWash( $washFields = [])
	{
		// Get collection of ids from organisation and get the array of id using all()
		$slingIds = Sling::pluck('id')->all();
		$wash = array_merge([
			'id' 		=> $faker->uuid,
			'sling_id' 	=> $faker->randomElement($slingIds),
			'wash_date' => $faker->dateTime,
			'photos' 	=> $faker->uuid,
			'notes' 	=> $faker->realText($maxNbChars = 200, $indexSize = 2)
		], $washFields);
		while ($this->times--) { Wash::create($wash);
		}
	}

	private function getStub()
	{
		return [
			'notes' => 'Arch Technologies'
		];
	}


}
