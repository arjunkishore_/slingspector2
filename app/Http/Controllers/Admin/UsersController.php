<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Organisation;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;

class UsersController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'role' => 'required',
		'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required'
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
        'role.required' => 'The Role field is required',
		'email.required' => 'The Name field is required',
        'first_name.required' => 'The First Name field is required',
        'last_name.required' => 'The Surname field is required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
        
        if ($orgID){
            $users = User::get();

            foreach ($users as $key => $user){
                if (isset($user->roles()->first()->pivot->organisation_id)){
                    if ($user->roles()->first()->pivot->organisation_id != $orgID){
                        unset($users[$key]);
                    }
                } else {
                    unset($users[$key]);
                }

                $user->role = Sentinel::findRoleById($user->roles()->first()->pivot->role_id);
            }

            $view['users'] = $users;
        } else {
            $users = User::with('organisations')->orderBy('first_name', 'ASC')->get();
            foreach ($users as $key => $user){
                $user->role = Sentinel::findRoleById($user->roles()->first()->pivot->role_id);
                $user->organisation = Organisation::find($user->roles()->first()->pivot->organisation_id);
            }

            $view['users'] = $users;
        }
        
        return view('admin.management.users.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view['organisations'] = Organisation::dropdownList();
        $roles = DB::table('roles')->orderBy('name', 'ASC')->get();
        $view['slug'] = '';
        $view['orgID'] = '';

        foreach ($roles as $role){
            $view['roles'][$role->slug] = $role->name;    

            if (!Sentinel::getUser()->inRole('admin')){
                unset($view['roles']['admin']);
            }
        }

        return view('admin.management.users.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validationRules['password'] = 'required';
        $this->validationMessages['password.required'] = 'Password is required for new users';
        $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

        $data = [
            'first_name'	=> $request->input('first_name'),
            'last_name'		=> $request->input('last_name'),
            'email'			=> $request->input('email'),
            'password'		=> $request->input('password'),
            'phone'         => $request->input('phone')
        ];

        if ($request->input('password') == "" || !$request->input('password')) {
            unset($data['password']);
        }

        $user = Sentinel::registerAndActivate($data);

        $role = Sentinel::findRoleBySlug($request->input('role'));
		$role->users()->attach($user, array('organisation_id' => $request->input('organisation_id') ));

		return Redirect::to('users')->with('success', 'New User Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view['user'] = Sentinel::findById($id);
        $view['slug'] = Sentinel::findRoleById($view['user']->roles()->first()->pivot->role_id)->slug;
        $view['orgID'] = $view['user']->roles()->first()->pivot->organisation_id;
		$view['organisations'] = Organisation::dropdownList();
        $roles = DB::table('roles')->orderBy('name', 'ASC')->get();

        foreach ($roles as $role){
            $view['roles'][$role->slug] = $role->name;    

            if (!Sentinel::getUser()->inRole('admin')){
                unset($view['roles']['admin']);
            }
        }
        return view('admin.management.users.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$user = Sentinel::findById($id);

        $data = [
            'first_name'	=> $request->input('first_name'),
            'last_name'		=> $request->input('last_name'),
            'email'			=> $request->input('email'),
            'password'		=> $request->input('password'),
            'phone'         => $request->input('phone')
        ];

        if ($request->input('password') == "" || !$request->input('password')) {
            unset($data['password']);
        }

        $user = Sentinel::update($user, $data);
        $role = Sentinel::findRoleBySlug($request->input('role'));

        DB::table('role_users')->where('user_id', $user->id)->delete();

        if ($request->input('role') == 'admin'){
		    $role->users()->attach($user, array('organisation_id' => 0 ));
        } else {
            $role->users()->attach($user, array('organisation_id' => $request->input('organisation_id') ));
        }

		return Redirect::to('users')->with('success', 'User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Sentinel::findById($id);
        $user->delete();
        return Redirect::to('users')->with('success', 'User Deleted Successfully');
    }
}
