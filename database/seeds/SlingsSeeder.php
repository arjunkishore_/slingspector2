<?php

use Faker\Factory as Faker;

use App\Models\Sling;
use App\Models\Location;
use App\Models\Organisation;
use Illuminate\Database\Seeder;

class SlingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids from organisation and get the array of id using all()
		$organisationIds = Organisation::pluck('id')->all();
		$locationIds	 = Location::pluck('id')->all();

		foreach (range(1,25) as $index) {
			Sling::create([
				'id' 						=> $faker->uuid,
				'organisation_id'			=> $faker->randomElement($organisationIds),
				'barcode' 					=> $faker->isbn10,
				'manufacturer'				=> $faker->company,
				'name' 						=> $faker->name,
				'product_code'				=> $faker->ean8,
				'type'						=> $faker->randomElement(['webbing-loops-sling', 'keyhole-plate-sling', 'other-sling']),
				'size' 						=> $faker->randomElement(['S', 'M', 'L', 'XL', 'XXL']),
				'colour'					=> $faker->colorName,
				'swl'						=> $faker->numberBetween($min = 30, $max = 300),
				'unit' 						=> $faker->randomElement(['kg', 'lbs']),
				'date_of_manufacture' 		=> $faker->dateTimeThisDecade($max = 'now', $timezone = date_default_timezone_get()),
				'date_of_purchase'			=> $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = date_default_timezone_get()),
				'end_of_life'				=> $faker->dateTimeBetween($startDate = 'now', $endDate = '+5 years', $timezone = date_default_timezone_get()),
				'last_beforeuse_inspection'	=> $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'last_6month_inspection'	=> $faker->dateTimeBetween($startDate = '-12 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'last_12month_inspection'	=> $faker->dateTimeBetween($startDate = '-18 months', $endDate = 'now', $timezone = date_default_timezone_get()),
				'photo'						=> $faker->md5,
				'close_photo'				=> $faker->md5,
				'notes'						=> $faker->realText($maxNbChars = 200, $indexSize = 2),
				//'close_photo'				=> $faker->file($sourceDir, $targetDir, false),
				'location_id'				=> $faker->randomElement($locationIds)

			]);
		}

    }
}
