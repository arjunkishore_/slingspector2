<?php

namespace App\Http\Controllers\Api;

use Input;
use Response;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Inspection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnswerController extends ApiController
{

	public function __construct(Request $request)
   	{
       $this->request = $request;
   	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id = null)
	{

		switch (true) {
			// case ($this->request->is('api/v1/questions/*')):
			// 	$returnResults = $id ? Question::find($id)->answer : Answer::all();
			// break;
			case ($this->request->is('api/v1/inspections/*')):
				$returnResults = $id ? Inspection::find($id)->answer : Answer::all();
			break;
			case ($this->request->is('api/v1/answers')):
				$returnResults = Answer::all();
			break;
		}

		return $this->respond([
			'data'=> $returnResults->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$answer= new Answer;
		$answer['attributes']= $request->all();
		$answer->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $answer->id,
			'message'=> 'answer succesfully created',
			'success'=> true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
		$answer = Answer::find($id);
		if (!$answer)
		{
			return $this->respondNotFound('answer Not Found');
		}
		else {
			return $this->respond(['data'=>$answer,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		try{
			if ($answer = Answer::find($id)){
				$answer->update(Input::all());
				$answer->save();
				return $this->respond(['success' => true, 'message' => 'answer information updated']);
			} else {
				return $this->respond(['success' => false, 'message' => 'Unable to update answer id->'.$id]);
			}
		}
		catch(Exception $e){
		   // do task when error
		   return $this->respond(['success' => false, 'message' => $e->getMessage()]);
		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		try{
			if ($answer = Answer::find($id)){
				$answer->delete();
				return $this->respondDeleted('answer Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
	}
	}
