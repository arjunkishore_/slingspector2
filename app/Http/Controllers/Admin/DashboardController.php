<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index(){
		$view['sling_inspections'] = Sling::where('last_6month_inspection', '<=', \DB::raw('(now() - interval 4 month)'))->orderBy('last_6month_inspection', 'ASC')->get();
		return view('admin.dashboard', $view);
	}
	
	public function test(){
		$nowPlus6Months = Carbon::now()->addMonths(6);
		$nowMinus6Months = Carbon::now()->subMonths(6);
		$nowMinus1Month = Carbon::now()->subMonth();

		// Last Inspection over 6 months ago
		$view['overdue'] = 	Sling::where('last_6month_inspection', '<', $nowMinus6Months->format('Y-m-d'))
					->get();

		// Upcoming Inspections 6 mmonths ago
		$view['upcoming'] = Sling::where(DB::raw('MONTH(last_6month_inspection)'), '=', $nowMinus6Months->format('m'))
					->where(DB::raw('YEAR(last_6month_inspection)'), '=', $nowMinus6Months->format('Y'))
					->get();

		// Inspections done last month
		$view['completed'] = Sling::where(DB::raw('MONTH(last_6month_inspection)'), '=', $nowMinus1Month->format('m'))
					->where(DB::raw('YEAR(last_6month_inspection)'), '=', $nowMinus1Month->format('Y'))
					->get();

		return view('emails.monthly-summary', $view);
	}
}
