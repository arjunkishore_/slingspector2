<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;
class Location extends Model
{
	use UuidModelTrait;
	use SoftDeletes;

	protected $fillable = ['organisation_id', 'name'];

	public $timestamps = false;

	public function organisation()
	{
		return $this->belongsTo('App\Models\Organisation');
	}

	public function location($value='')
	{
		return $this->belongsTo('App\Models\Location');
	}

	static function dropdownList($organisationID=null){
		if ($organisationID){
			$locations = Location::where('organisation_id', $organisationID)->orderBy('name', 'ASC')->get();
		} else {
			$locations = Location::with('organisation')->orderBy('name', 'ASC')->get();
		}

		$locationsArray = [];

		foreach ($locations as $location) {
			if (!$organisationID){
				$locationsArray[$location->id] = $location->name.' ('.$location->organisation['name'].')';
			} else {
				$locationsArray[$location->id] = $location->name;
			}
		}

		return $locationsArray;
	}

}
