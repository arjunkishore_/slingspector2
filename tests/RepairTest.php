<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Repair;
use App\Models\Sling;

class RepairTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_repair(){

		$this->getJson('api/v1/repairs');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_repair()
	{
		// check first row
		$repair = $this->getJson('api/v1/repairs')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($repair, 'sling_id','notes');
	}

	/** @test */
	public function it_deletes_a_single_repair()
	{
		// check first row id
		$repairId = $this->getJson('api/v1/repairs')->data[0]->id;

		$response = $this->getJson('api/v1/repairs/'.$repairId,'DELETE');
		// 202 expected on delete

		$this->assertResponseStatus(202);

	}

	/** @test */
    public function it_404s_if_a_repair_is_not_found()
    {
        $json = $this->getJson('api/v1/repairs/asdfas');
        $this->assertResponseStatus(404);

		$this->assertObjectHasAttributes($json, 'error');

    }


	/** @test */
	public function it_updates_a_repair()
	{
		// check first row id
		$repairId = $this->getJson('api/v1/repairs')->data[0]->id;
		// dd($repairId);
		$response = $this->getJson('api/v1/repairs/'.$repairId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makeRepair( $repairFields = [])
	{
		// Get collection of ids from organisation and get the array of id using all()
		$slingIds = Sling::pluck('id')->all();
		$repair = array_merge([
			'id' 		=> $faker->uuid,
			'sling_id' 	=> $faker->randomElement($slingIds),
			//'user_id' 	=> $faker->randomElement($userIds),
			'photos' 	=> $faker->uuid,
			'notes' 	=> $faker->realText($maxNbChars = 200, $indexSize = 2),
			'repair_complete'=>$faker->dateTime,
			'repair_date'=>$faker->dateTime,
			'repair_by'=>$faker->dateTime
		], $repairFields);
		while ($this->times--) { Repair::create($repair);
		}
	}

	private function getStub()
	{
		return [
			'notes' => 'Arch Technologies'
		];
	}


}
