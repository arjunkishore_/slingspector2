<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Sling extends Model
{
	use UuidModelTrait;
	use SoftDeletes;

	protected $fillable = ['organisation_id', 'barcode', 'name', 'size', 'type', 'swl', 'manufacturer', 'date_of_manufacture', 'date_of_purchase', 'next_inspection', 'end_of_life', 'last_beforeuse_inspection','last_6month_inspection','last_12month_inspection','last_repair','location_id','delete_reason','deleted_by'];
	//protected $dates = ['date_of_manufacture', 'date_of_purchase', 'next_inspection', 'end_of_life','last_beforeuse_inspection','last_6month_inspection','last_12month_inspection','last_repair'];
	protected $appends = array('last_inspection', 'next_inspection');

	public $timestamps = false;

	public function location()
	{
		return $this->belongsTo('App\Models\Location');
	}

	public function organisation()
	{
		return $this->belongsTo('App\Models\Organisation');
	}

	public function wash()
	{
		return $this->hasMany('App\Models\Wash');
	}

	public function repair()
	{
		return $this->hasMany('App\Models\Repair');
	}

	public function inspection()
	{
		return $this->hasMany('App\Models\Inspection');
	}

	static function dropdownList($organisationID=null){
		if ($organisationID){
			$slings = Sling::where('organisation_id', $organisationID)->orderBy('barcode', 'ASC')->get();
		} else {
			$slings = Sling::orderBy('barcode', 'ASC')->get();
		}

		$slingsArray = [];

		foreach ($slings as $sling) {
			$slingsArray[$sling->id] = $sling->barcode;
		}

		return $slingsArray;
	}


	/**
	 * Get a custom attribute 'next_inspection'
	 *
	 * @return string
	*/
	public function getNextInspectionAttribute()
    {
		if ($this->attributes['last_6month_inspection'] > $this->attributes['last_12month_inspection']){
			$date = $this->attributes['last_6month_inspection'];
		} else if ($this->attributes['last_12month_inspection'] > $this->attributes['last_6month_inspection']){
			$date = $this->attributes['last_12month_inspection'];
		} else {
			$date = 'Never';
		}

		if ($date != 'Never'){
			$dt = Carbon::createFromFormat('Y-m-d', $date);
			return $dt->addMonths(6)->format('d/m/Y');
		} else {
			return $date;
		}
    }

	/**
	 * Get a custom attribute 'last_inspection'
	 *
	 * @return string
	*/
	public function getLastInspectionAttribute()
    {
		if ($this->attributes['last_6month_inspection'] > $this->attributes['last_12month_inspection']){
			$date = $this->attributes['last_6month_inspection'];
		} else if ($this->attributes['last_12month_inspection'] > $this->attributes['last_6month_inspection']){
			$date = $this->attributes['last_12month_inspection'];
		} else {
			$date = 'Never';
		}

		if ($date != 'Never'){
			$split = explode('-', $date);
			return $split[2].'/'.$split[1].'/'.$split[0];
		} else {
			return $date;
		}
    }

	/**
     * Set the date_of_manufacture from dd/mm/yyyy to yyyy-mm-dd.
     *
     * @param  string  $value
     * @return void
     */
    public function setDateOfManufactureAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);

			if (isset($split[2])){
				$this->attributes['date_of_manufacture'] = $split[2].'-'.$split[1].'-'.$split[0];
			}
		} else {
			$this->attributes['date_of_manufacture'] = $value;
		}
    }

	/**
     * Set the date_of_purchase from dd/mm/yyyy to yyyy-mm-dd.
     *
     * @param  string  $value
     * @return void
     */
    public function setDateOfPurchaseAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);

			if (isset($split[2])){
				$this->attributes['date_of_purchase'] = $split[2].'-'.$split[1].'-'.$split[0];
			}
		} else {
			$this->attributes['date_of_purchase'] = $value;
		}
    }

	/**
     * Set the end_of_life from dd/mm/yyyy to yyyy-mm-dd.
     *
     * @param  string  $value
     * @return void
     */
    public function setEndOfLifeAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);
			if (isset($split[2])){
				$this->attributes['end_of_life'] = $split[2].'-'.$split[1].'-'.$split[0];
			}
		} else {
			$this->attributes['end_of_life'] = $value;
		}
    }

	/**
     * Get the date_of_manufacture from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getDateOfManufactureAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

	/**
     * Get the date_of_purchase from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getDateOfPurchaseAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

	/**
     * Get the end_of_life from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getEndOfLifeAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

	/**
     * Get the last_6month_inspection from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getLast6monthInspectionAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

	/**
     * Get the last_12month_inspection from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getLast12monthInspectionAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

	/**
     * Get the last_beforeuse_inspection from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getLastBeforeuseInspectionAttribute($date)
    {
		if ($date){
			$split = explode('-', $date);
			if (isset($split[2])){
				return $split[2].'/'.$split[1].'/'.$split[0];
			}
		}
    }

}
