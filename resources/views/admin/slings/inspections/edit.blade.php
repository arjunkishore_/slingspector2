@extends('admin.layouts.master')

@section('bodyclass', 'inspection')

@section('title', 'Sling Inspection')

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
	<link rel="stylesheet" href="/assets/global/vendor/jquery-wizard/jquery-wizard.css">
	<link rel="stylesheet" href="/assets/global/vendor/formvalidation/formValidation.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
	<script src="/assets/global/vendor/formvalidation/formValidation.js"></script>
  	<script src="/assets/global/vendor/formvalidation/framework/bootstrap.js"></script>
	<script src="/assets/global/vendor/jquery-wizard/jquery-wizard.js"></script>
	<script type="text/javascript">
	jQuery(function($) {
	  	$('#inspectionFormWizard').wizard({
			templates: {
	          buttons: function() {
	            var options = this.options;
	            var html = '<div class="btn-group btn-group-sm">' +
	              '<a class="btn btn-default" href="#' + this.id + '" data-wizard="back" role="button">' + options.buttonLabels.back + '</a>' +
	              '<a class="btn btn-success pull-right" href="#' + this.id + '" data-wizard="finish" role="button">' + options.buttonLabels.finish + '</a>' +
	              '<a class="btn btn-slingspector pull-right" href="#' + this.id + '" data-wizard="next" role="button">' + options.buttonLabels.next + '</a>' +
	              '</div>';
	            return html;
	          }
	        },
			onAfterShow: function() {
				$('html, body').animate({
			        scrollTop: $(".step.current").offset().top-70
			    }, 200);
			},
			onFinish: function() {
				$('#inspectionForm').submit();
			},
			onInit: function() {
				$('#inspectionFormWizard').formValidation({
			      	framework: 'bootstrap',
			      	fields: {
					 	@foreach ($answers as $question)
							@if ($question->required)
								'question_{{ $question->id }}': {
									validators: {
										notEmpty: {
											message: 'This field is required'
										}
									}
								},
							@endif
						@endforeach
			      	},
					row: {
    					selector: '.question-block'
					}
			    });
			},
			validator: function() {
	          	var fv = $('#inspectionFormWizard').data('formValidation');

	          	var $this = $(this);

	          	// Validate the container
	          	fv.validateContainer($this);

	          	var isValidStep = fv.isValidContainer($this);
	          	if (isValidStep === false || isValidStep === null) {
					$('html, body').animate({
				        scrollTop: $(".question-block.has-error:first").offset().top-80
				    }, 200);

	            	return false;
	          	}

	          	return true;
	        },
		});


	});
	</script>
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Sling Inspection</h1>
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings">Inspections</a></li>
			<li class="breadcrumb-item active">Sling Inspection</li>
		</ol>
	</div>
@stop

@section('content')
	@if (isset($sling))
		{{ Form::model($inspection, array('url' => 'slings/inspections/'.$inspection->id, 'method' => 'PUT', 'id' => 'inspectionForm')) }}
	@else
		{{ Form::open(array('url' => 'slings/inspections', 'method' => 'POST', 'id' => 'inspectionForm')) }}
	@endif
		@if (!isset($sling_id))
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="row row-lg">
						<div class="col-md-12 col-lg-6 col-xs-12">
							<div class="form-group row">
								<label class="col-xs-12 col-md-3 form-control-label font-weight-bold">Organisation:</label>
								<div class="col-md-8 col-xs-12">
									{{ $inspection->organisation['name'] }}
									<input type="hidden" name="organisation_id" value="6-month-sling" />
								</div>
							</div>
							<div class="form-group required row">
								<label class="col-xs-12 col-md-3 form-control-label font-weight-bold">Sling Barcode:</label>
								<div class="col-md-9 col-xs-12">
									{{ $inspection->sling['barcode'] }}
								</div>
							</div>
							<input type="hidden" name="inspection_type" value="6-month-sling" />
						</div>
					</div>
				</div>
			</div>
		@else
			<input type="hidden" name="sling_id" value="{{ $sling_id }}" />
			<input type="hidden" name="organisation_id" value="{{ $organisation_id }}" />
			<input type="hidden" name="inspection_type" value="6-month-sling" />
		@endif
		<div class="panel">
	  		<div class="panel-body container-fluid">
				<div class="row row-lg">
					<div class="col-md-12 col-lg-12 col-xs-12">
						<!-- Panel Wizard Form -->
			            <div class="panel" id="inspectionFormWizard">
			                	<!-- Steps -->
			                	<ul class="wizard-steps" role="tablist">
									<?php $i = 1; ?>
									@foreach ($headings as $heading)
									<?php if ($i == 1){ $class = 'current'; } else { $class = ''; } ?>
			                  		<li class="step col-xs-12 col-lg-3 {{ $class }}" role="tab">
			                    		<span class="step-number">{{ $i }}</span>
			                    		<div class="step-desc">
			                      			<p>{{ $heading->heading }}</p>
			                    		</div>
			                  		</li>
									<?php $i++; ?>
									@endforeach
			                	</ul>
			                	<!-- End Steps -->
			                	<!-- Wizard Content -->
			                	<div class="wizard-content">
									<?php $i = 1; ?>
									@foreach ($headings as $heading)
										<?php if ($i == 1){ $class = 'active'; } else { $class = ''; } ?>
										<div class="wizard-pane {{ $class }}" role="tabpanel">
											@foreach ($answers as $question)
												<?php $isItAnswered = $question->answer ? true : false; ?>
												@if ($question->heading == $heading->heading)
													<div class="col-lg-12 col-md-12 col-xs-12 question-block">
														<?php $class = $question->required ? 'required' : ''; ?>
														{{ Form::label('question_'.$question->id, $question->question, array('class' => 'form-control-label font-weight-bold '.$class)) }}
														<div class="options-row">
															@if ($question->input_type == 'radio')
																<?php
																	// Get options
																	if (!empty($question->input_options)){
																		$options = explode('|', $question->input_options);
																	} else {
																		$options = '';
																	}

																?>
																@if (!empty($options))
																	@foreach ($options as $k => $option)
																		<label class="radio-inline">
																		{{ Form::radio('question_'.$question->id, $option, false, ['class' => 'field']) }}{{ $option }}
																		</label>
																	@endforeach
																@endif

																<div class="comments-row">
																	{{ Form::textarea('question_'.$question->id.'_comment', Input::old('question_'.$question->id.'_comment') , array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Enter your comments...' )) }}
																</div>

																<div class="photos-row">
																	<button class="btn btn-primary"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</button>
																</div>
															@elseif ($question->input_type == 'text')
																{{ Form::text('question_'.$question->id, Input::old('question_'.$question->id), array('class' => 'form-control', 'autocomplete' => 'off')) }}
															@elseif  ($question->input_type == 'textarea')
																{{ Form::textarea('question_'.$question->id, Input::old('question_'.$question->id), array('class' => 'form-control', 'autocomplete' => 'off')) }}
															@endif
														</div>
													</div>
												@endif
											@endforeach
										</div>
										<?php $i++; ?>
									@endforeach
			                	</div>
			                <!-- End Wizard Content -->
			            </div>
			            <!-- End Panel Wizard One Form -->
					</div>
				</div>
			</div>
		</div>
		{{ Form::token() }}
	{{ Form::close() }}
@stop
