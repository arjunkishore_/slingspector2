<?php

namespace App\Http\Controllers\Api;

use Response;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
	// Default set to 200
	protected $statusCode = 200;

	public function getStatusCode()
	{
		return $this->statusCode;
	}

	public function setStatusCode($statusCode)
	{
	    $this->statusCode = $statusCode;
		return $this;
	}

	public function respond( $data, $headers=[])
	{
		return response()->json($data, $this->getStatusCode(),$headers);
	}

	public function respondWithPagination( LengthAwarePaginator $items, $data)
	{
		$data = array_merge($data, [
		'paginator' => [
		'total_count' => $items->total(),
		'total_pages' => $items->lastPage(),
		'current_page' => $items->currentPage(),
		'next_page'	=> $items->nextPageUrl(),
		'previous_page' => $items->previousPageUrl(),
		'limit' => $items->perPage()
		]
		]);
		return $this->respond($data);
	}

	public function respondWithError($message)
	{
		return response()->json([
			'error' 		=> $message,
			'status_code'   => $this->getStatusCode()
		],$this->getStatusCode());
	}

	public function respondNotFound($message ='Not Found')
	{
		return $this->setStatusCode(404)->respondWithError($message);
	}

	public function respondInternalError($message='Internal Server Error')
	{
		return $this->setStatusCode(500)->respondWithError($message);
	}

	public function respondDeleted($message='Deleted succesfully')
	{
		return $this->setStatusCode(202)->respondWithError($message);
	}

	public function repondUnableToDelete($message='Problem Deleting')
	{
		return $this->setStatusCode(409)->respondWithError($message);
	}




} // End of ApiController class
