<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;
class Inspection extends Model
{
    use UuidModelTrait;
	use SoftDeletes;
	
    //public $timestamps = false;
	protected $fillable = ['id', 'organisation_id','sling_id','hoist_id','user_id','type', 'inspection_date', 'completed','updated_at','deleted_at','delete_reason','deleted_by'];

	public function organisation()
	{
		return $this->belongsTo('App\Models\Organisation');
	}
	public function sling()
	{
		return $this->belongsTo('App\Models\Sling');
	}

	public function hoist()
	{
		return $this->belongsTo('App\Models\Hoist');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}


	public function answers()
	{
		return $this->hasMany('App\Models\Answer');
	}

	/**
     * Get the inspection_date from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getInspectionDateAttribute($value)
    {
		$datetime = explode(' ', $value);
		$split = explode('-', $datetime[0]);
		return $split[2].'/'.$split[1].'/'.$split[0];
    }

	static function getSlingAnswers($id){
		$inspection = Inspection::find($id);
		$questions = Question::where('inspection_type', $inspection->type)->where('is_active', 1)->orderBy('ordering', 'ASC')->get();
		foreach ($questions as $question){
			$answer = Answer::where('inspection_id', $id)->where('question_id', $question->id)->first();
			$question->answer = $answer;
		}

		return $questions;
	}

	static function getHoistAnswers($id){
		$questions = Question::where('inspection_type', 'hoist')->where('is_active', 1)->orderBy('ordering', 'ASC')->get();

		foreach ($questions as $question){
			$answer = Answer::where('inspection_id', $id)->where('question_id', $question->id)->first();
			$question->answer = $answer;
		}

		return $questions;
	}
}
