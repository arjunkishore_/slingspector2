<?php

namespace App\Http\Controllers\Admin;

use App\Models\Location;
use App\Models\Organisation;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Sentinel;

class LocationsController extends Controller
{
	/**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		'organisation_id' => 'required',
		'name' => 'required'
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		'organisation_id.required' => 'The Organisation field is required',
		'name.required' => 'The Name field is required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

		  if ($orgID){
		    $view['locations'] = Location::with('organisation')->where('organisation_id', $orgID)->get();
      } else {
        $view['locations'] = Location::with('organisation')->get();
      }
      return view('admin.management.locations.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$view['organisations'] = Organisation::dropdownList();
        return view('admin.management.locations.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$location = new Location();
		$location->fill($request->all());
		$location->save();

		return Redirect::to('locations')->with('success', 'New Location Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$view['location'] = Location::find($id);
		$view['organisations'] = Organisation::dropdownList();
        return view('admin.management.locations.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$location = Location::find($id);
		$location->fill($request->all());
		$location->save();

		return Redirect::to('locations')->with('success', 'Location Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
		$location->delete();

		return Redirect::to('locations')->with('success', 'Location Deleted Successfully');
    }
}
