<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;
use Input;
use Response;
use App\Models\Question;
use App\Models\Answer;
use Request;
use App\Http\Controllers\Controller;


class QuestionController extends ApiController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id = null)
	{
		$questions = $id ? Question::find($id) : Question::all();
		return $this->respond([
			'data'=> $questions->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function types($type)
	{
		$questions = DB::table('questions')->where('inspection_type', $type)->get();
		return $this->respond([
			'data'=> $questions->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$question= new Question;
		$question['attributes']= $request->all();
		$question->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $question->id,
			'message'=> 'question succesfully created',
			'success'=> true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
		$question = Question::find($id);
		if (!$question)
		{
			return $this->respondNotFound('question Not Found');
		}
		else {
			return $this->respond(['data'=>$question,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		try{
			if ($question = Question::find($id)){
				$question->update(Input::all());
				$question->save();
				return $this->respond(['success' => true, 'message' => 'question information updated']);
			} else {
				return $this->respond(['success' => false, 'message' => 'Unable to update question id->'.$id]);
			}
		}
		catch(Exception $e){
		   // do task when error
		   return $this->respond(['success' => false, 'message' => $e->getMessage()]);
		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		try{
			if ($question = Question::find($id)){
				$question->delete();
				return $this->respondDeleted('question Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
	}

	public function form(){
		$headings = Question::select('heading')->where('inspection_type', 'sling')->where('is_active', 1)->orderBy('ordering', 'ASC')->groupBy('heading')->get();
		$questions = Question::where('inspection_type', 'sling')->where('is_active', 1)->orderBy('ordering', 'ASC')->get();

		foreach ($headings as $heading){
			foreach ($questions as $question){
				if ($question->heading == $heading->heading){
					// Get options
					if ($question->input_options != ''){
						$options = explode('|', $question->input_options);
					} else {
						$options = '';
					}

					$questionArray[] = array(
						'question_id' 	=> $question->id,
						'question'		=> $question->question,
						'input_type'	=> $question->input_type,
						'input_options'	=> $options,
						'required'		=> $question->required ? true : false
					);
				}
			}

			$response[][$heading->heading] = $questionArray;
			unset($questionArray);
		}

		return $this->respond(['data'=>$response,
		"payload" => "Slingspector API v1.0",
		"success" => true]);
	}
}
