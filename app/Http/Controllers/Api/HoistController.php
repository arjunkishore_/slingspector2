<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Hoist;
use App\Models\Organisation;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HoistController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {

		$hoists = $id ? Organisation::find($id)->Hoists : Hoist::with('location')->orderBy('last_6month_inspection', 'ASC')->get();

    foreach ($hoists as $hoist){
        $hoist->type_display = ucwords(str_replace("-", " ", $hoist->type));
    }

		return $this->respond([
			'data'=> $hoists,
			'payload'=> 'Hoistspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      // Date of Manufacture
      $dom_sp = explode("T", $request->get('data')['date_of_manufacture']);

      if (isset($dom_sp[0])){
          $dom_dt = Carbon::createFromFormat('Y-m-d', $dom_sp[0])->format('d/m/Y');
      } else {
          $dom_dt = null;
      }

      $dop_sp = explode("T", $request->get('data')['date_of_purchase']);

      if (isset($dop_sp[0])){
          $dop_dt = Carbon::createFromFormat('Y-m-d', $dop_sp[0])->format('d/m/Y');
      } else {
          $dop_dt = null;
      }

      $doc_sp = explode("T", $request->get('data')['date_of_decommission']);

      if (isset($doc_sp[0])){
          $dod_dt = Carbon::createFromFormat('Y-m-d', $doc_sp[0])->format('d/m/Y');
      } else {
          $dod_dt = null;
      }

      $hoist = new Hoist;
      $hoist->organisation_id = $request->get('organisation');
      $hoist->barcode = $request->get('data')['barcode'];
      $hoist->manufacturer = $request->get('data')['manufacturer'];
      //$hoist->size = $request->get('data')['size'];
      $hoist->swl = $request->get('data')['swl'];
      $hoist->unit = $request->get('data')['unit'];
      $hoist->type = $request->get('data')['type'];
      $hoist->date_of_manufacture = $dom_dt;
      $hoist->date_ofpurchase = $dop_dt;
      $hoist->end_of_life = $dod_dt;
      //$hoist->notes = $request->get('data')['notes'];
      $hoist->save();

      return $this->setStatusCode(201)->respond([
        'id'	 => $hoist->id,
        'message'=> 'Hoist succesfully created',
        'success'=> true
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$hoist = Hoist::with('location')->where('id', $id)->first();
    $hoist->type_display = ucwords(str_replace("-", " ", $hoist->type));
    
		if (!$hoist)
		{
			return $this->respondNotFound('Hoist Not Found');
		}
		else {
			return $this->respond(['data'=>$hoist,
			"payload" => "Hoistspector API v1.0",
			"success" => true]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try{
            if ($Hoist = Hoist::find($id)){
                $Hoist->update(Input::all());
                $Hoist->save();
                return $this->respond(['success' => true, 'message' => 'Hoist information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update Hoist id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try{
			if ($Hoist = Hoist::find($id)){
				$Hoist->delete();
				return $this->respondDeleted('Hoist Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
    }
}
