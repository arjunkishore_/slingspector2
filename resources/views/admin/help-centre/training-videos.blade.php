@extends('admin.layouts.master')

@section('bodyclass', 'help-centre')

@section('title', 'Training Videos | Help Centre')

@section('css')
  	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
@endsection

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Training Videos</h1>
  		<div class="page-header-actions">
  		</div>
	</div>
@endsection

@section('content')
	<div class="row" data-plugin="matchHeight" data-by-row="true">
		<div class="col-xxl-12 col-lg-12 col-xs-12">
			<div class="panel">
            	<div class="panel-body">
					<?php 
					switch($organisation->location){
						case "Australia":
						case "default":
						case "Rest of World":
							$slingInspection = "61zVxZaZ_fg";
							$beforeSLingUse = "99HeEb5eCXk";
							$trolleyhoist = "ClAeNqg41Sw";
							$ceilinghoist = "dxBnNIu73Bw";
						break;
						case "USA":
							$slingInspection = "vIKSlrXuMtg";
							$beforeSLingUse = "qNbQdObTBfE";
							$trolleyhoist = "cQjAHNQceB4";
							$ceilinghoist = "MPuz1FSrysI";
						break;
						case "England":
						case "Scotland":
						case "Ireland":
						case "Wales":
						case "Rest of Europe":
							$slingInspection = "UmnTzS3cs6s";
							$beforeSLingUse = "nVEbqwLIIdc";
							$trolleyhoist = "EaLtMu1dQQo";
							$ceilinghoist = "4LsXn0Mw-Zc";
						break;
					}
					?>
					<div class="row">
						<div class="col-xxl-6 col-lg-6 col-xs-12">
							<h4>6 Monthly Thorough Sling Inspection</h4>
							<iframe width="560" height="310" src="https://www.youtube.com/embed/{{ $slingInspection }}?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="col-xxl-6 col-lg-6 col-xs-12">
							<h4>Sling Inspection Before Use</h4>
							<iframe width="560" height="310" src="https://www.youtube.com/embed/{{ $beforeSLingUse }}?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="row">
						<br /><br />
					</div>
					<div class="row">
						<div class="col-xxl-6 col-lg-6 col-xs-12">
							<h4>Trolley Hoist Inspection</h4>
							<iframe width="560" height="310" src="https://www.youtube.com/embed/{{ $trolleyhoist }}?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="col-xxl-6 col-lg-6 col-xs-12">
							<h4>Ceiling Hoist Inspection</h4>
							<iframe width="560" height="310" src="https://www.youtube.com/embed/{{ $ceilinghoist }}?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
            	</div>
          	</div>
		</div>
	</div>
@endsection
