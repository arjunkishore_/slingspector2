<?php

namespace App\Http\Controllers\Admin;

use App\Models\Wash;
use App\Models\Sling;
use App\Models\Organisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Sentinel;
use  DB;

class SlingWashesController extends Controller
{
	/**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		'sling_id' => 'required',
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		'sling_id.required' => 'The Sling field is required',
    ];

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;

      if ($orgID){
        $view['washes'] = Wash::with([ 'sling' => function($query) use ($orgID) {
          $query->where('organisation_id', $orgID);
        }])->get();
      } else {
        $view['washes'] = Wash::with('sling.organisation')->get();
      }

		  $user = Sentinel::getUser();
		  $view['user']= $user;
		
      return view('admin.slings.washes.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$view['organisations'] = Organisation::dropdownList();
		$orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		$view['slings'] = Sling::dropdownList($orgID);
        return view('admin.slings.washes.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$wash = new Wash();
		$wash->fill($request->all());
		$wash->user_id = Sentinel::getUser()->id;
		$wash->save();

		return Redirect::to('slings/washes')->with('success', 'New Wash Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$view['wash'] = Wash::find($id);
		return view('admin.slings.washes.view', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$view['wash'] = Wash::find($id);
		$view['organisations'] = Organisation::dropdownList();
		$orgID = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
		$view['slings'] = Sling::dropdownList($orgID);
        return view('admin.slings.washes.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$wash = Wash::find($id);
		$wash->fill($request->all());
		$wash->user_id = Sentinel::getUser()->id;
		$wash->save();

		return Redirect::to('slings/washes')->with('success', 'Wash Saved Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		$wash = Wash::find($id);
		if( $wash->delete() ){
			DB::table('washes')->where('id', $wash->id)
				->update(array('deleted_by' => filter_var ($request->deleted_by, FILTER_SANITIZE_STRING) ,'delete_reason' => filter_var( $request->delete_reason, FILTER_SANITIZE_STRING)));
		}
		return Redirect::to('slings/washes')->with('success', 'Wash Deleted Successfully');
    }
}
