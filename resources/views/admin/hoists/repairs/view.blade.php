@extends('admin.layouts.master')

@section('bodyclass', 'repairs repair')

@section('title')
	{{ 'Repair - Hoist '.$repair->hoist['barcode'] }}
@stop

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Repair - Hoist {{ $repair->hoist['barcode'] }}</h1>
  		<div class="page-header-actions">
			<a href="/hoists/repairs/{{ $repair->id }}/edit" class="btn btn-primary">Edit</a>
			<a data-href="/hoists/repairs/{{ $repair->id }}" data-toggle="modal" data-target="#confirm-delete" data-row="{{ $repair->id }}" data-type="Repair" class="btn btn-danger delete">Delete</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/hoists">hoists</a></li>
			<li class="breadcrumb-item"><a href="/hoists/repairs">Repairs</a></li>
			<li class="breadcrumb-item active">Repair - Hoist {{ $repair->hoist['barcode'] }}</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="panel">
		  		<div class="panel-body container-fluid">
					<div class="row row-lg">
						<div class="col-md-12 col-lg-12 col-xs-12">
							<div class="form-group row {{ $errors->has('hoist_id') ? 'has-error' :'' }}">
								{{ Form::label('hoist_id', 'Hoist', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $repair->hoist['barcode'] }}
					      		</div>
					    	</div>
							<div class="form-group row {{ $errors->has('repair_date') ? 'has-error' :'' }}">
								{{ Form::label('repair_date', 'Date Lodged', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $repair->repair_date }}
					      		</div>
						    </div>
							<div class="form-group row {{ $errors->has('repair_complete') ? 'has-error' :'' }}">
								{{ Form::label('repair_complete', 'Date Completed', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $repair->repair_complete }}
					      		</div>
						    </div>
							<div class="form-group row {{ $errors->has('repair_by') ? 'has-error' :'' }}">
								{{ Form::label('repair_by', 'Repair By', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $repair->repair_by }}
					      		</div>
						    </div>
							<div class="form-group row {{ $errors->has('notes') ? 'has-error' :'' }}">
								{{ Form::label('notes', 'Notes', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $repair->notes }}
					      		</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
