<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
//use Alsofronie\Uuid\UuidModelTrait;

class User extends EloquentUser
{
    use Notifiable;
	//use UuidModelTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes to be fillable from the model.
	 *
	 * A dirty hack to allow fields to be fillable by calling empty fillable array
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'password'];
	protected $guarded = ['id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	* To allow soft deletes
	*/
	use SoftDeletes;

    protected $dates = ['deleted_at'];

	public function washes()
	{
		return $this->hasMany('App\Models\Wash');
	}

	public function repairs()
	{
		return $this->hasMany('App\Models\Repair');
	}

	public function inspections()
	{
		return $this->hasMany('App\Models\Inspection');
	}

	public function organisations()
	{
		return $this->belongsTo('App\Models\Organisation');
	}

	/**
     * Returns the roles relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(){
        return $this->belongsToMany(static::$rolesModel, 'role_users', 'user_id', 'role_id')->withPivot('organisation_id')->withTimestamps();
    }
}
