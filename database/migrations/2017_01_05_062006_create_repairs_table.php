<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('repairs', function (Blueprint $table) {
	      	$table->engine = 'InnoDB';
	      	$table->uuid('id')->index();
		  	$table->uuid('sling_id')->nullable();
		  	$table->foreign('sling_id')->references('id')->on('slings')->onDelete('cascade');
			$table->uuid('hoist_id')->nullable();
		  	$table->foreign('hoist_id')->references('id')->on('hoists')->onDelete('cascade');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		  	$table->text('photos');
		  	$table->text('notes');
			$table->string('repair_by');
		  	$table->date('repair_date');
		  	$table->date('repair_complete');
			$table->string('delete_reason');
			$table->string('deleted_by');
			$table->dateTime('deleted_at');
		  	$table->primary('id');
	  });
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repairs');
    }
}
