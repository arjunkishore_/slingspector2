<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
// UuidModelTrait - the key must be CHAR(36) and contains the dashes
// Uuid32ModelTrait - the key must be CHAR(32), the dashes are stripped
// UuidBinaryModelTrait - the key is BINARY(16).
class Organisation extends Model
{
	use UuidModelTrait;
	use SoftDeletes;
	
  	protected $fillable = ['id', 'name', 'address_1', 'address_2', 'suburb', 'state', 'postcode', 'country', 'phone', 'contact_phone', 'contact_first', 'contact_last', 'contact_email', 'type', 'plan', 'location', 'monthly_rate', 'annual_inspection_limit', 'join_date', 'is_active'];
	protected $dates = ['join_date'];

	/**
	 * [locations gets the locations within the organisations]
	 */
	public function locations()
	{
		return $this->hasMany('App\Models\Location');
	}

    /**
     * [slings within the organisation]
     */
	public function slings()
	{
		return $this->hasMany('App\Models\Sling');
	}

	/**
	 * [hoists within the organisation]
	 */
	public function hoists()
	{
		return $this->hasMany('App\Models\Hoist');
	}

	/**
	 * [users description]
	 */
	public function users()
	{
		return $this->hasMany('App\Models\User');
	}

	static function dropdownList(){
		$organisations = Organisation::orderBy('name', 'ASC')->where('id', '<>', 0)->get();
		$organisationsArray = [];

		foreach ($organisations as $organisation) {
			$organisationsArray[$organisation->id] = $organisation->name;
		}

		return $organisationsArray;
	}

	/**
     * Set the join_date from dd/mm/yyyy to yyyy-mm-dd.
     *
     * @param  string  $value
     * @return void
     */
    public function setJoinDateAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);

			if (isset($split[2])){
				$this->attributes['join_date'] = $split[2].'-'.$split[1].'-'.$split[0];
			}
		} else {
			$this->attributes['join_date'] = $value;
		}
    }

	/**
     * Get the join_date from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getJoinDateAttribute($date)
    {
		$split = explode('-', $date);
		if (isset($split[2])){
			return $split[2].'/'.$split[1].'/'.$split[0];
		}
		 //return Carbon::parse($date);
    }

}
