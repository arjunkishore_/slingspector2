@extends('admin.layouts.master')

@section('bodyclass', 'organisations')

@if (isset($location))
	@section('title', 'Edit Organisation')
@else
	@section('title', 'New Organisation')
@endif

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
@stop

@section('page-header')
	<div class="page-header">
		@if (isset($organisation))
  			<h1 class="page-title">Edit Organisation</h1>
		@else
			<h1 class="page-title">New Organisation</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/locations">Organisations</a></li>
			@if (isset($organisation))
				<li class="breadcrumb-item active">Edit Organisation</li>
			@else
				<li class="breadcrumb-item active">New Organisation</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($organisation))
						{{ Form::model($organisation, array('url' => 'organisations/'.$organisation->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'organisations', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif
						<div class="form-group required row {{ $errors->has('name') ? 'has-error' :'' }}">
							{{ Form::label('name', 'Organisation Name', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('name', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group required row {{ $errors->has('location') ? 'has-error' :'' }}">
							{{ Form::label('location', 'Location', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
							<div class="col-md-8 col-xs-12">
								{{ Form::select('location', [null => ''] + $locations, Input::old('location'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="form-group row {{ $errors->has('address_1') ? 'has-error' :'' }}">
							{{ Form::label('address_1', 'Address Line 1', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('address_1', Input::old('address_1'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('address_1', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('address_2') ? 'has-error' :'' }}">
							{{ Form::label('address_2', 'Address Line 2', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('address_2', Input::old('address_2'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('address_2', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('suburb') ? 'has-error' :'' }}">
							{{ Form::label('suburb', 'Suburb', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('suburb', Input::old('suburb'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('suburb', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('postcode') ? 'has-error' :'' }}">
							{{ Form::label('postcode', 'Postcode', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('postcode', Input::old('postcode'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('postcode', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('state') ? 'has-error' :'' }}">
							{{ Form::label('state', 'State', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('state', Input::old('state'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('state', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('country') ? 'has-error' :'' }}">
							{{ Form::label('country', 'Country', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('country', Input::old('country'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('country', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('phone') ? 'has-error' :'' }}">
							{{ Form::label('phone', 'Organisation Phone', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('phone', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('contact_first') ? 'has-error' :'' }}">
							{{ Form::label('contact_first', 'Contact First Name', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('contact_first', Input::old('contact_first'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('contact_first', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('contact_last') ? 'has-error' :'' }}">
							{{ Form::label('contact_last', 'Contact Surname', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('contact_last', Input::old('contact_last'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('contact_last', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('contact_phone') ? 'has-error' :'' }}">
							{{ Form::label('contact_phone', 'Contact Phone', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('contact_phone', Input::old('contact_phone'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('contact_phone', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('contact_email') ? 'has-error' :'' }}">
							{{ Form::label('contact_email', 'Contact Email', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
								{{ Form::text('contact_email', Input::old('contact_email'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('contact_email', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group required row {{ $errors->has('plan') ? 'has-error' :'' }}">
							{{ Form::label('plan', 'Plan', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
							<div class="col-md-8 col-xs-12">
								{{ Form::select('plan', [null => ''] + $plans, Input::old('plan'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="form-group required row {{ $errors->has('monthly_rate') ? 'has-error' :'' }}">
							{{ Form::label('monthly_rate', 'Monthly Fee', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-4 col-xs-12">
								{{ Form::text('monthly_rate', Input::old('monthly_rate'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('monthly_rate', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group required row {{ $errors->has('annual_inspection_limit') ? 'has-error' :'' }}">
							{{ Form::label('annual_inspection_limit', 'Annual Inspection Limit', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-4 col-xs-12">
								{{ Form::text('annual_inspection_limit', Input::old('annual_inspection_limit'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('annual_inspection_limit', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('join_date') ? 'has-error' :'' }}">
							{{ Form::label('join_date', 'Join Date', array('class' => 'col-xs-12 col-md-4 form-control-label')) }}
				      		<div class="col-md-8 col-xs-12">
							  	<?php if (!isset($organisation)){ ?>
									{{ Form::text('join_date', date('d/m/Y'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy' )) }}
								<?php } else { ?>
									{{ Form::text('join_date', Input::old('join_date'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy' )) }}
								<?php } ?>
								<small class="text-help">{{ $errors->first('join_date', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
