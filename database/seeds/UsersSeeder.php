<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Organisation;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		$organisationIds = Organisation::pluck('id')->all();

		// Create Roles
		$roles = array(
			'admin' => 'Admin',
			'manager' => 'Manager',
			'employee' => 'Employee'
		);

		foreach ($roles as $k => $v){
			$role = Sentinel::getRoleRepository()->createModel()->create([
			    'name' => $v,
			    'slug' => $k,
			]);
		}

		foreach (range(1,25) as $index) {
			$user = Sentinel::registerAndActivate([
				'first_name'	=> $faker->firstName,
				'last_name'		=> $faker->lastName,
				'email'			=> $faker->unique()->email,
				'password'		=> $faker->password
			]);

			$role = Sentinel::findRoleByName($faker->randomElement($roles));
			$role->users()->attach($user, array('organisation_id' => $faker->randomElement($organisationIds) ));
		}

		// Admin User
		$admin = Sentinel::registerAndActivate([
			'first_name'	=> 'Bill',
			'last_name'		=> 'Smith',
			'email'			=> 'admin@admin.com',
			'password'		=> 'admin'
		]);

		$role = Sentinel::findRoleByName('Admin');
		$role->users()->attach($admin, array('organisation_id' => 0 ));

		$manager = Sentinel::registerAndActivate([
			'first_name'	=> 'Jane',
			'last_name'		=> 'Doe',
			'email'			=> 'manager@manager.com',
			'password'		=> 'manager'
		]);

		$role = Sentinel::findRoleByName('Manager');
		$role->users()->attach($manager, array('organisation_id' => $faker->randomElement($organisationIds) ));

		$employee = Sentinel::registerAndActivate([
			'first_name'	=> 'Ned',
			'last_name'		=> 'Brown',
			'email'			=> 'employee@employee.com',
			'password'		=> 'employee'
		]);

		$role = Sentinel::findRoleByName('Employee');
		$role->users()->attach($employee, array('organisation_id' => $faker->randomElement($organisationIds) ));

		/*
		$organisation = Organisation::find();
		$organisation->users()->attach($user);
		*/

    }
}
