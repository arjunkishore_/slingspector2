<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Organisation;

class OrganisationTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_organisation(){

		$this->getJson('api/v1/organisations');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_organisation()
	{
		// check first row
		$organisation = $this->getJson('api/v1/organisations')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($organisation, 'id','name','address_1','address_2');
	}

	/** @test */
	public function it_deletes_a_single_organisation()
	{
		// check first row id
		$organisationId = $this->getJson('api/v1/organisations')->data[0]->id;

		$response = $this->getJson('api/v1/organisations/'.$organisationId,'DELETE');
		// 202 expected on delete

		$this->assertEquals(202, $response->status_code);
		$this->assertResponseStatus(202);

	}

	/** @test */
    public function it_404s_if_a_organisation_is_not_found()
    {
        $json = $this->getJson('api/v1/organisations/asdfas');
        $this->assertResponseStatus(404);
		$this->assertObjectHasAttributes($json, 'error');

    }


	/** @test */
	public function it_updates_an_organisation()
	{
		// check first row id
		$organisationId = $this->getJson('api/v1/organisations')->data[0]->id;

		$response = $this->getJson('api/v1/organisations/'.$organisationId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makeOrganisation( $organisationFields = [])
	{
		$organisation = array_merge([
			'name'			=> $this->fake->company,
			'address_1'		=> $this->fake->streetName,
			'address_2'		=> $this->fake->streetAddress,
			'suburb'		=> $this->fake->city,
			'state'			=> $this->fake->state,
			'postcode'		=> $this->fake->postcode,
			'country'		=> 'Australia',
			'type'			=> 'Business',
			'is_active'		=>  $this->fake->boolean(200)
		], $organisationFields);
		while ($this->times--) { Organisation::create($organisation);
		}
	}

	private function getStub()
	{
		return [
			'name' => 'Arch Technologies'
		];
	}


}
