(function(document, window, $) {
	'use strict';
	var Site = window.Site;
	$(document).ready(function() {
  		Site.run();
		$('#modalContainer').on('hidden.bs.modal', function () {
			$('#myModalLabel').html('');
			$('#modalForm').attr('action', '');
			$('#modalForm').html('');
		});

		$(document).on('click', '.modal-submit', function(){
			$.ajax({
				url: $('#modalForm').attr('action'),
				type: 'POST',
				dataType: 'json',
				data: $('#modalForm').serialize()
			})
			.done(function(data) {
				location.reload();
				$('#modalContainer').modal('close');
			})
			.fail(function() {
				console.log("error");
			});
		});

		// Handle Delete Methods
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		});

		$('#confirm-delete').on('show.bs.modal', function(e) {
			$(this).find('span.delete-type').html($(e.relatedTarget).data('type'));
			$(this).find('#deleted_by').attr('value',$(e.relatedTarget).data('username'));
		    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			$(this).find('.btn-ok').attr('data-row', $(e.relatedTarget).data('row'));
		});

		$(document).on('click', '.btn-ok', function(e) {
			e.preventDefault(); // does not go through with the link.

		    var $this = $(this);
			var $deleted_by = $('#deleted_by').val();
			var $delete_reason = $('#delete_reason').val();
		    $.post({
		        type: 'POST',
		        url: $this.attr('href'),
				data: {
					'_method': 'DELETE',
					'id': $this.attr('data-row'),
					'deleted_by': $deleted_by,
					'delete_reason': $delete_reason
				}
		    }).done(function (data) {
		        $('.table #'+$this.attr('data-row')).remove();
				$('#confirm-delete').modal('hide');
				$('.message-holder').html('<div class="alert dark alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> SUCCESS : Item deleted successfully.</div>');

				setTimeout(function(){
				  $('.message-holder .alert').fadeOut(400, function(){
					  $('.message-holder').html('');
				  });
				}, 5000);
		    });
		});
	});
	$(window).load(function(){
		var _sid = $('#sling_id_holder').val();
		if(typeof _sid != 'undefined'){
			$( "#inspectionsTable_wrapper .buttonarea" ).html('<a href="/slings/'+_sid+'/inspection" class="btn btn-slingspector">Start Inspection</a>&nbsp;<a href="/slings/'+_sid+'/3rd-party-inspection" class="btn btn-slingspector">Upload 3rd-Party Inspection</a>');
			$( ".sling #washesTable_wrapper .buttonarea" ).html('<a href="/slings/'+_sid+'/wash" class="btn btn-slingspector">Mark as Washed</a>');
			$( ".sling #repairsTable_wrapper .buttonarea" ).html('<a href="/slings/'+_sid+'/repair" class="btn btn-slingspector">Log Service/Repair</a>');
		}
		var _hid =$('#hoist_id_holder').val();
		if(typeof _hid != 'undefined'){
			$( "#inspectionsTable_wrapper .buttonarea" ).html('<a href="/hoists/'+_hid+'/inspection" class="btn btn-slingspector">Start Inspection</a>&nbsp;<a href="/hoists/'+_hid+'/3rd-party-inspection" class="btn btn-slingspector">Upload 3rd-Party Inspection</a>');
			$( "#washesTable_wrapper .buttonarea" ).html('<a href="/hoists/'+_hid+'/wash" class="btn btn-slingspector">Mark as Washed</a>');
			$( "#repairsTable_wrapper .buttonarea" ).html('<a href="/hoists/'+_hid+'/repair" class="btn btn-slingspector">Log Service/Repair</a>');
		}
	});
})(document, window, jQuery);
