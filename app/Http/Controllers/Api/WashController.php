<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use App\Models\Wash;
use App\Models\Sling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class WashController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index($id = null)
	{
	//
		$washes = $id ? Sling::find($id)->wash : Wash::all();
		return $this->respond([
		'data'=> $washes->all(),
		'payload'=> 'Slingspector API v1.0',
		'success' => true
		]);
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sp = explode("T", $request->get('data')['wash_date']);
        $dt = Carbon::createFromFormat('Y-m-d', $sp[0])->format('d/m/Y');

		$wash = new Wash;
        $wash->sling_id = $request->get('id');
        $wash->user_id = $request->get('user');
        $wash->wash_date = $dt;
        $wash->notes = $request->get('data')['notes'];
		$wash->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $wash->id,
			'message'=> 'Wash succesfully created',
			'success'=> true
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$wash = Wash::find($id);
		if (!$wash)
		{
			return $this->respondNotFound('Wash Not Found');
		}
		else {
			return $this->respond(['data'=>$wash,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		try{
            if ($wash = Wash::find($id)){
                $wash->update(Input::all());
                $wash->save();
                return $this->respond(['success' => true, 'message' => 'Wash information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update Wash id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		try{
			if ($wash = Wash::find($id)){
				$wash->delete();
				return $this->respondDeleted('Wash Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
    }
}
