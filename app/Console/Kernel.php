<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Sling;
use Carbon\Carbon;
use DB;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $nowPlus6Months = Carbon::now()->addMonths(6);
            $nowMinus6Months = Carbon::now()->subMonths(6);
            $nowMinus1Month = Carbon::now()->subMonth();

            // Last Inspection over 6 months ago
            $overdue = 	Sling::where('last_6month_inspection', '<', $nowMinus6Months->format('Y-m-d'))
                        ->get();

            // Upcoming Inspections 6 mmonths ago
            $upcoming = Sling::where(DB::raw('MONTH(last_6month_inspection)'), '=', $nowMinus6Months->format('m'))
                        ->where(DB::raw('YEAR(last_6month_inspection)'), '=', $nowMinus6Months->format('Y'))
                        ->get();

            // Inspections done last month
            $completed = Sling::where(DB::raw('MONTH(last_6month_inspection)'), '=', $nowMinus1Month->format('m'))
                        ->where(DB::raw('YEAR(last_6month_inspection)'), '=', $nowMinus1Month->format('Y'))
                        ->get();

            Mail::send('emails.monthly-summary',  ['completed' => $completed, 'upcoming' => $upcoming, 'overdue' => $overdue], function ($m) {
                $m->from('no-reply@slingspector.com', 'SlingSpector');
                $m->to('ryan@roobix.com.au', 'Ryan Bown')->subject('Monthly Inspection Summary');
            });

        })->monthly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
