<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use App\Models\Organisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class OrganisationController extends ApiController
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $limit = request()->limit ? : 4;
        $organisations = Organisation::paginate($limit);

		return($this->respondWithPagination($organisations, [
			'data'=> $organisations->all(),
			'payload'=> 'Slingspector API v1.0',
			'success'=>true
		]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$organisation = new Organisation;
    	$organisation->name='Roobix';
    	$organisation->address_1='105 Royal Street';
		$organisation->address_2='Near SIP Coffee Shop';
		$organisation->suburb='East Perth';
		$organisation->state='Western Australia';
		$organisation->postcode='6004';
		$organisation->country='Australia';
		$organisation->type='Business';
		$organisation->is_active='1';
		$organisation->save();
		return $this->setStatusCode(201)->respond([
			'id'=>$organisation->id,
			'success'=>true,
			'payload'=> "Roobix Slingspector API V 1.0"]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$organisation = new Organisation;
		$organisation['attributes']= $request->all();
		$organisation->save();

		return $this->setStatusCode(201)->respond([
			'id'	=> $organisation->id,
			'message'=>'Organisation succesfully created',
			'success'=>true
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  uuid  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organisation = Organisation::find($id);
		if (!$organisation)
		{
			return $this->respondNotFound('Organisation Not Found');
		}
		else {
			return $this->respond(['data'=>$organisation]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try{
            if ($organisation = Organisation::find($id)){
                $organisation->update(Input::all());
                $organisation->save();
                return $this->respond(['success' => true, 'message' => 'Organisation information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update organisation id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try{
            if ($organisation = Organisation::find($id)){
				$organisation->delete();

                return $this->respondDeleted('Organisation Deleted');
            } else {
            	return $this->repondUnableToDelete();
            }
        }
         catch(Exception $e){
           // do task when error
           return $this->respondUnableToDelete($e->getMessage());
        //    return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
