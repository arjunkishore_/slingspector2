<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use UuidModelTrait;
	use SoftDeletes;
	
	protected $fillable = ['inspection_id','question_id','answer','photo','ordering','comments'];

	public function question()
	{
		return $this->belongTo('App\Models\Question');
	}

	public function inspection()
	{
		return $this->belongTo('App\Models\Inspection');
	}
}
