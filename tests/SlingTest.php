<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Sling;
use App\Models\Organisation;

class SlingTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_sling(){

		$this->getJson('api/v1/slings');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_sling()
	{
		// check first row
		$sling = $this->getJson('api/v1/slings')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($sling, 'organisation_id','name','barcode');
	}

	/** @test */
	public function it_deletes_a_single_sling()
	{
		// check first row id
		$slingId = $this->getJson('api/v1/slings')->data[0]->id;

		$response = $this->getJson('api/v1/slings/'.$slingId,'DELETE');
		// 202 expected on delete

		$this->assertResponseStatus(202);

	}

	/** @test */
	public function it_404s_if_a_sling_is_not_found()
	{
		$json = $this->getJson('api/v1/slings/asdfas');
		$this->assertResponseStatus(404);
		$this->assertObjectHasAttributes($json, 'error');

	}


	/** @test */
	public function it_updates_a_sling()
	{
		// check first row id
		$slingId = $this->getJson('api/v1/slings')->data[0]->id;
		// dd($slingId);
		$response = $this->getJson('api/v1/slings/'.$slingId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makeSling( $slingFields = [])
	{
		// Get collection of ids from organisation and get the array of id using all()
		$organisationIds = Organisation::pluck('id')->all();
		$locationIds	 = Location::pluck('id')->all();

		$sling = array_merge([
			'id' 						=> $faker->uuid,
			'sling_id'  				=> $faker->regexify('[A-Z0-9]+[A-Z0-9]+[A-Z]{2,4}'),
			'organisation_id'			=> $faker->randomElement($organisationIds),
			'custom_code'				=> $faker->streetAddress,
			'barcode' 					=> $faker->isbn10,
			'name' 						=> $faker->name,
			'size' 						=> $faker->state,
			'attachments'				=> $faker->md5,
			'swl'						=> $faker->numberBetween($min = 30, $max = 300),
			'manufacturer'				=> $faker->company,
			'date_of_manufacture' 		=> $faker->dateTimeThisDecade($max = 'now', $timezone = date_default_timezone_get()),
			'date_of_purchase'			=> $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = date_default_timezone_get()),
			'end_of_life'				=> $faker->dateTimeBetween($startDate = '-20 years', $endDate = 'now', $timezone = date_default_timezone_get()),
			'location_id'				=> $faker->randomElement($locationIds)

		], $slingFields);
		while ($this->times--) { Sling::create($sling);
		}
	}

	private function getStub()
	{
		return [
			'name' => 'Arch Technolgies'
		];
	}


}
