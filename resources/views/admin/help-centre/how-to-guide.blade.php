@extends('admin.layouts.master')

@section('bodyclass', 'help-centre')

@section('title', 'How to Guide | Help Centre')

@section('css')
  	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
@endsection

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">How to Guide</h1>
  		<div class="page-header-actions">
  		</div>
	</div>
@endsection

@section('content')
	<div class="row" data-plugin="matchHeight" data-by-row="true">
		<div class="col-xxl-12 col-lg-12 col-xs-12">
			<div class="panel">
            	<div class="panel-body">
					
            	</div>
          	</div>
		</div>
	</div>
@endsection
