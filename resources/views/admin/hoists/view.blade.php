@extends('admin.layouts.master')

@section('bodyclass', 'hoists hoist')

@section('title')
	{{ 'Hoist - '.$hoist->barcode }}
@stop

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Hoist - {{ $hoist->barcode }}</h1>
  		<div class="page-header-actions">
			<a href="/hoists/{{ $hoist->id }}/edit" class="btn btn-primary">Edit</a>
			<a data-href="/hoists/{{ $hoist->id }}" data-toggle="modal" data-target="#confirm-delete" data-row="{{ $hoist->id }}" data-type="Hoist" class="btn btn-danger delete">Delete</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/hoists">Hoists</a></li>
			<li class="breadcrumb-item active">Hoist - {{ $hoist->barcode }}</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
					<ul class="nav nav-tabs nav-tabs-solid" role="tablist">
					<li class="nav-item active" role="presentation">
  						<a class="nav-link active" data-toggle="tab" href="#information" aria-controls="information" role="tab">
  							Information
						</a>
					</li>
					<li class="nav-item" role="presentation">
  						<a class="nav-link" data-toggle="tab" href="#inspections" aria-controls="inspections" role="tab">
  							Inspections
						</a>
					</li>
					<li class="nav-item" role="presentation">
  						<a class="nav-link" data-toggle="tab" href="#repairs" aria-controls="repairs" role="tab">
  							Repairs
						</a>
					</li>
					</ul>
					<div class="tab-content">
					<div class="tab-pane active" id="information" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-8 col-xs-12">
								<div class="form-group row">
									{{ Form::label('barcode', 'Code:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
									<div class="col-md-8 col-xs-12">
										{{ $hoist->barcode }}
									</div>
								</div>
								<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						    	<div class="form-group row">
									{{ Form::label('organisation_id', 'Organisation:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $hoist->organisation['name'] }}
						      		</div>
						    	</div>
								<?php } ?>
								<div class="form-group row">
									{{ Form::label('location_id', 'Facility:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $hoist->location['name'] }}
						      		</div>
						    	</div>
								<div class="form-group row">
									{{ Form::label('type', 'Type:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
									<div class="col-md-8 col-xs-12">
										{{ ucwords(str_replace("-", " ", $hoist->type)) }}
									</div>
								</div>
								<div class="form-group row">
									{{ Form::label('swl', 'SWL:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $hoist->swl .' '.$hoist->unit }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('manufacturer', 'Manufacturer:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										{{ $hoist->manufacturer }}
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('last_6month_inspection', 'Last 6 Month Inspection:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($hoist->last_6month_inspection){
												$dt = Carbon::createFromFormat('d/m/Y', $hoist->last_6month_inspection);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('date_of_manufacture', 'Date of Manufacture:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($hoist->date_of_manufacture){
												$dt = Carbon::createFromFormat('d/m/Y', $hoist->date_of_manufacture);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('date_of_purchase', 'Date of Purchase:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($hoist->date_of_purchase){
												$dt = Carbon::createFromFormat('d/m/Y', $hoist->date_of_purchase);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
								<div class="form-group row">
									{{ Form::label('end_of_life', 'Date of Decommission:', array('class' => 'col-xs-12 col-md-4 form-control-label font-weight-bold')) }}
						      		<div class="col-md-8 col-xs-12">
										<?php
											if ($hoist->end_of_life){
												$dt = Carbon::createFromFormat('d/m/Y', $hoist->end_of_life);
												echo $dt->format('M d, Y');
											}
										?>
						      		</div>
							    </div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="inspections" role="tabpanel">
						<table class="table table-hover dataTable table-striped table-search w-full" id="inspectionsTable">
					  		<thead>
								<tr>
									<th>Date</th>
									<th>Status</th>
									<th class="no-sort">Actions</th>
								</tr>
					  		</thead>
					  		<!--
							<tfoot>
								<tr>
									<th>Date</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
					  		</tfoot>
							-->
					  		<tbody>
								<?php
									if (isset($inspections)){
										foreach ($inspections as $inspection){
											echo '<tr id="'.$inspection->id.'">';
												echo '<td>'.$inspection->inspection_date.'</td>';
												$status = $inspection->completed == 1 ? 'Completed' : 'In Progress';
												echo '<td>'.$status.'</td>';
												echo '<td>';
													if ($inspection->third_party_document){
														echo '<a href="http://files.roobix.com.au/slingspector-com/app/inspections/'.$inspection->third_party_document.'" class="btn btn-xs btn-slingspector action" target="_blank">Download</a>';
													} else {
														echo '<a href="/hoists/inspections/'.$inspection->id.'" class="btn btn-xs btn-slingspector action">View</a>';
													}
													echo '<button data-href="/hoists/inspections/'.$inspection->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$inspection->id.'" data-type="Inspection" class="btn btn-xs btn-danger action delete">Delete</button>';
												echo '</td>';
											echo '</tr>';
										}
									}
								?>
					  		</tbody>
						</table>
					</div>
					<div class="tab-pane" id="repairs" role="tabpanel">
						<table class="table table-hover dataTable table-striped table-search w-full" id="repairsTable">
					  		<thead>
								<tr>
									<th>Date Lodged</th>
									<th>Date Completed</th>
									<th>Notes</th>
									<th class="no-sort">Actions</th>
								</tr>
					  		</thead>
					  		<tbody>
								<?php
									if (isset($repairs)){
										foreach ($repairs as $repair){
											echo '<tr id="'.$repair->id.'">';
												echo '<td>'.$repair->repair_date.'</td>';
												echo '<td>'.$repair->repair_complete.'</td>';
												echo '<td>'.$repair->notes.'</td>';
												echo '<td>';
													echo '<a href="/hoists/repairs/'.$repair->id.'" class="btn btn-xs btn-slingspector action">View</a>';
													echo '<button data-href="/hoists/repairs/'.$repair->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$repair->id.'" data-type="Repair" class="btn btn-xs btn-danger action delete">Delete</button>';
												echo '</td>';
											echo '</tr>';
										}
									}
								?>
					  		</tbody>
						</table>
					</div>
					</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="hoist_id" id="hoist_id_holder" value="{{ $hoist->id }}">
@stop
