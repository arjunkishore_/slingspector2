<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use UuidModelTrait;
	use SoftDeletes;

	protected $fillable =['id','question','input_type','input_options','inspection_type','ordering','compliance','is_active','updated_at'];

	public function answer()
	{
		return $this->hasMany('App\Models\Answer');
	}

}
