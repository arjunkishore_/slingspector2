<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitial extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{

	    Schema::create('organisations', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->uuid('id')->index();
			$table->string('name');
			$table->string('address_1');
			$table->string('address_2');
			$table->string('suburb');
			$table->string('postcode');
			$table->string('state');
			$table->string('country');
			$table->string('phone');
			$table->string('type');
			$table->string('contact_first');
			$table->string('contact_last');
			$table->string('contact_email');
			$table->string('contact_phone');
			$table->string('plan');
			$table->string('location');
			$table->integer('monthly_rate');
			$table->integer('annual_inspection_limit');
			$table->date('join_date');
			$table->boolean('is_active')->default(false);
			$table->timestamps();
			$table->dateTime('deleted_at');
			$table->primary('id');
	    });

	    Schema::create('locations', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->uuid('id')->index();
			$table->uuid('organisation_id')->index();
			$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
			$table->string('name');
			$table->dateTime('deleted_at');
			$table->primary('id');
	    });

		Schema::create('slings', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->uuid('id')->index();
			//$table->string('sling_id')->index();
			$table->uuid('organisation_id')->index();
			$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
			$table->string('barcode')->nullable();
			$table->string('manufacturer')->nullable();
			$table->string('name')->nullable();
			$table->string('product_code')->nullable();
			$table->string('type')->nullable();
			$table->string('size')->nullable();
			$table->string('colour')->nullable();
			$table->float('swl')->nullable();
			$table->string('unit')->nullable();
			$table->date('date_of_manufacture')->nullable();
			$table->date('date_of_purchase')->nullable();
			$table->date('end_of_life')->nullable();
			$table->date('last_beforeuse_inspection')->nullable();
			$table->date('last_6month_inspection')->nullable();
			$table->date('last_12month_inspection')->nullable();
			$table->date('last_repair')->nullable();
			$table->string('photo')->nullable();
			$table->string('close_photo')->nullable();
			$table->string('notes')->nullable();
			$table->uuid('location_id')->nullable()->index();
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
			$table->string('delete_reason');
			$table->string('deleted_by');
			$table->dateTime('deleted_at');
			$table->primary('id');
	    });

	    Schema::create('hoists', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->uuid('id')->index();
			//$table->string('hoist_id')->index();
			$table->uuid('organisation_id')->index();
			$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
			//$table->string('custom_code');
			$table->string('barcode')->nullable();
			$table->string('name')->nullable();
			$table->string('size')->nullable();
			$table->string('type')->nullable();
			//$table->text('attachments');
			$table->float('swl')->nullable();
			$table->string('unit')->nullable();
			$table->string('manufacturer')->nullable();
			$table->date('date_of_manufacture')->nullable();
			$table->date('date_of_purchase')->nullable();
			$table->date('end_of_life')->nullable();
			$table->date('last_beforeuse_inspection')->nullable();
			$table->date('last_6month_inspection')->nullable();
			$table->date('last_12month_inspection')->nullable();
			$table->date('last_repair')->nullable();
			$table->uuid('location_id')->nullable()->index();
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
			$table->string('delete_reason');
			$table->string('deleted_by');
			$table->dateTime('deleted_at');
			$table->primary('id');
	    });

  	}

  	/**
  	* Reverse the migrations.
  	*
  	* @return void
  	*/
  	public function down()
  	{
    	// Note: it should be in reverse order functions in up()
    	Schema::drop('slings');
    	Schema::drop('hoists');
    	Schema::drop('locations');
    	Schema::drop('organisations');
  	}
}
