@extends('admin.layouts.master')

@section('bodyclass', 'inspection')

@section('title', '3rd-Party Hoist Inspection')

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
  	<script src="/assets/global/vendor/formvalidation/framework/bootstrap.js"></script>
	<script type="text/javascript">
	jQuery(function($) {
	  	
	});


	</script>
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">3rd-Party Hoist Inspection</h1>
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/hoists/inspections">Inspections</a></li>
			<li class="breadcrumb-item active">3rd-Party Hoist Inspection</li>
		</ol>
	</div>
@stop

@section('content')
	@if (isset($inspection))
		{{ Form::model($inspection, array('url' => 'hoists/3rd-party-inspection/'.$inspection->id, 'method' => 'PUT', 'id' => 'inspectionForm', 'files' => true)) }}
	@else
		{{ Form::open(array('url' => 'hoists/3rd-party-inspection', 'method' => 'POST', 'id' => 'inspectionForm', 'files' => true)) }}
	@endif
		@if (!isset($hoist_id))
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="row row-lg">
						<div class="col-md-12 col-lg-6 col-xs-12">
							<?php if (Sentinel::getUser()->inRole('admin')){ ?>
							<div class="form-group required row">
								{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
					      		<div class="col-md-9 col-xs-12">
									{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
					      		</div>
					    	</div>
							<?php } else { ?>
								<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
							<?php } ?>
							<div class="form-group required">
								{{ Form::label('hoist_id', 'Hoist', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
					      		<div class="col-md-9 col-xs-12">
									{{ Form::select('hoist_id', [null => ''] + $hoists, Input::old('hoist_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
					      		</div>
					    	</div>
						</div>
					</div>
				</div>
			</div>
		@else
			<input type="hidden" name="hoist_id" value="{{ $hoist_id }}" />
			<input type="hidden" name="organisation_id" value="{{ $organisation_id }}" />
			<input type="hidden" name="inspection_type" value="{{ $hoist->type }}" />
		@endif
		<div class="panel">
	  		<div class="panel-body container-fluid">
				<div class="row row-lg">
					<div class="col-md-12 col-lg-12 col-xs-12">
						<div class="form-group row {{ $errors->has('inspection_date') ? 'has-error' :'' }}">
							{{ Form::label('inspection_date', 'Inspection Date', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('inspection_date', Input::old('inspection_date'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('inspection_date', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('document') ? 'has-error' :'' }}">
							{{ Form::label('document', 'Upload File', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::file('document', Input::old('document'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('document', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::token() }}
	{{ Form::close() }}
@stop
