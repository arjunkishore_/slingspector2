<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organisation;
use Validator;
use Sentinel;
use Response;
use DB;
use Illuminate\Support\Facades\Redirect;

class OrganisationsController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
		'location' => 'required',
		'name' => 'required',
        'plan' => 'required',
        'monthly_rate' => 'required',
        'annual_inspection_limit' => 'required'
    ];

	/**
     * Define your validation messages in a property in
     * the controller to reuse the rules.
     */
    protected $validationMessages = [
		'location.required' => 'The Location field is required',
		'name.required' => 'The Name field is required',
        'plan.required' => 'The Plan field is required',
        'monthly_rate.required' => 'The Monthly Fee field is required',
        'annual_inspection_limit.required' => 'The Annual Inspection Limit field is required'
    ];

    protected $locations = array(
        'USA' => 'USA',
        'England' => 'England',
        'Scotland' => 'Scotland',
        'Ireland' => 'Ireland',
        'Wales' => 'Wales',
        'Rest of Europe' => 'Rest of Europe',
        'Australia' => 'Australia',
        'Rest of World' => 'Rest of World'
    );

    protected $plans = array(
        'Training' => 'Training',
        'Home' => 'Home',
        'Bronze' => 'Bronze',
        'Silver' => 'Silver',
        'Gold' => 'Gold',
        'Platinum' => 'Platinum',
        'Diamond' => 'Diamond',
        'Custom' => 'Custom'
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		$view['organisations'] = Organisation::where('id', '<>', '0')->get();
        return view('admin.management.organisations.index', $view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $view['locations'] = $this->locations;
        $view['plans'] = $this->plans;
        return view('admin.management.organisations.form', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$organisation = new Organisation();
		$organisation->fill($request->all());
		$organisation->save();

		return Redirect::to('organisations')->with('success', 'New Organisation Saved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $view['organisation'] = Organisation::find($id);
		$view['locations'] = $this->locations;
        $view['plans'] = $this->plans;
        return view('admin.management.organisations.form', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), $this->validationRules, $this->validationMessages)->validate();

		$organisation = Organisation::find($id);
		$organisation->fill($request->all());
		$organisation->save();

		return Redirect::to('organisations')->with('success', 'Organisation Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $organisation = Organisation::find($id);
		$organisation->delete();

		return Redirect::to('organisations')->with('success', 'Organisation Deleted Successfully');
    }
}
