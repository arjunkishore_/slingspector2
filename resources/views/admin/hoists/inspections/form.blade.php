@extends('admin.layouts.master')

@section('bodyclass', 'inspection')

@section('title', 'Hoist Inspection')

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
	<link rel="stylesheet" href="/assets/global/vendor/jquery-wizard/jquery-wizard.css">
	<link rel="stylesheet" href="/assets/global/vendor/formvalidation/formValidation.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
	<script src="/assets/global/vendor/formvalidation/formValidation.js"></script>
  	<script src="/assets/global/vendor/formvalidation/framework/bootstrap.js"></script>
	<script src="/assets/global/vendor/jquery-wizard/jquery-wizard.js"></script>
	<script type="text/javascript">
	jQuery(function($) {
	  	$('#inspectionFormWizard').wizard({
			templates: {
	          buttons: function() {
	            var options = this.options;
	            var html = '<div class="btn-group btn-group-sm">' +
	              '<a class="btn btn-default" href="#' + this.id + '" data-wizard="back" role="button">' + options.buttonLabels.back + '</a>' +
	              '<a class="btn btn-success pull-right" href="#' + this.id + '" data-wizard="finish" role="button">' + options.buttonLabels.finish + '</a>' +
	              '<a class="btn btn-slingspector pull-right" href="#' + this.id + '" data-wizard="next" role="button">' + options.buttonLabels.next + '</a>' +
	              '</div>';
	            return html;
	          }
	        },
			onAfterShow: function() {
				$('html, body').animate({
			        scrollTop: $(".step.current").offset().top-70
			    }, 200);
			},
			onFinish: function() {
				$('#inspectionForm').submit();
			},
			onInit: function() {
				$('#inspectionFormWizard').formValidation({
			      	framework: 'bootstrap',
			      	fields: {
					 	@foreach ($questions as $question)
							@if ($question->required)
								'question_{{ $question->id }}': {
									validators: {
										notEmpty: {
											message: 'This field is required'
										}
									}
								},
							@endif
						@endforeach
			      	},
					row: {
    					selector: '.question-block'
					}
			    });
			},
			validator: function() {
	          	var fv = $('#inspectionFormWizard').data('formValidation');

	          	var $this = $(this);

	          	// Validate the container
	          	fv.validateContainer($this);

	          	var isValidStep = fv.isValidContainer($this);
	          	if (isValidStep === false || isValidStep === null) {
					$('html, body').animate({
				        scrollTop: $(".question-block.has-error:first").offset().top-80
				    }, 200);

	            	return false;
	          	}

	          	return true;
	        },
		});

		$('.answer-option').change(function(){
			var correct = $(this).parent().parent().data('extra');
			if (correct == $(this).val()){
				$(this).parent().parent().find('.comments-row').show();
				$(this).parent().parent().find('.photos-row').show();
			}else{
				$(this).parent().parent().find('.comments-row').hide();
				$(this).parent().parent().find('.photos-row').hide();
			}
		});


	});
	</script>
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Hoist Inspection</h1>
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/hoists">Inspections</a></li>
			<li class="breadcrumb-item active">Hoist Inspection</li>
		</ol>
	</div>
@stop

@section('content')
	@if (isset($hoist))
		{{ Form::model($inspection, array('url' => 'hoists/inspections/'.$inspection->id, 'method' => 'PUT', 'id' => 'inspectionForm')) }}
	@else
		{{ Form::open(array('url' => 'hoists/inspections', 'method' => 'POST', 'id' => 'inspectionForm')) }}
	@endif
		@if (!isset($hoist_id))
			<div class="panel">
				<div class="panel-body container-fluid">
					<div class="row row-lg">
						<div class="col-md-12 col-lg-6 col-xs-12">
							<?php if (Sentinel::getUser()->inRole('admin')){ ?>
							<div class="form-group required row">
								{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
					      		<div class="col-md-9 col-xs-12">
									{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
					      		</div>
					    	</div>
							<?php } else { ?>
								<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
							<?php } ?>
							<div class="form-group required">
								{{ Form::label('hoist_id', 'Hoist', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
					      		<div class="col-md-9 col-xs-12">
									{{ Form::select('hoist_id', [null => ''] + $hoists, Input::old('hoist_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
					      		</div>
					    	</div>
							<input type="hidden" name="inspection_type" value="6-month-hoist" />
						</div>
					</div>
				</div>
			</div>
		@else
			<input type="hidden" name="hoist_id" value="{{ $hoist_id }}" />
			<input type="hidden" name="organisation_id" value="{{ $organisation_id }}" />
			<input type="hidden" name="inspection_type" value="6-month-hoist" />
		@endif
		<div class="panel">
	  		<div class="panel-body container-fluid">
				<div class="row row-lg">
					<div class="col-md-12 col-lg-12 col-xs-12">
						<!-- Panel Wizard Form -->
			            <div class="panel" id="inspectionFormWizard">
			                	<!-- Steps -->
			                	<ul class="wizard-steps" role="tablist">
									<?php $i = 1; ?>
									@foreach ($headings as $heading)
									<?php if ($i == 1){ $class = 'current'; } else { $class = ''; } ?>
			                  		<li class="step col-xs-12 col-lg-3 {{ $class }}" role="tab">
			                    		<span class="step-number">{{ $i }}</span>
			                    		<div class="step-desc">
			                      			<p>{{ $heading->heading }}</p>
			                    		</div>
			                  		</li>
									<?php $i++; ?>
									@endforeach
			                	</ul>
			                	<!-- End Steps -->
			                	<!-- Wizard Content -->
			                	<div class="wizard-content">
									<?php $i = 1; ?>
									@foreach ($headings as $heading)
										<?php if ($i == 1){ $class = 'active'; } else { $class = ''; } ?>
										<div class="wizard-pane {{ $class }}" role="tabpanel">
											@foreach ($questions as $question)
												@if ($question->heading == $heading->heading)
													<?php $blockclass = $question->input_type == "heading" ? 'heading' : ''; ?>
													<div class="col-lg-12 col-md-12 col-xs-12 question-block {{ $blockclass }}">
														<?php $class = $question->required ? 'required' : ''; ?>
														{{ Form::label('question_'.$question->id, $question->question, array('class' => 'form-control-label font-weight-bold '.$class)) }}
														<div class="options-row" data-extra="{{ $question->display_extra_field_on }}">
															@if ($question->input_type == 'radio')
																<?php
																	// Get options
																	if ($question->input_options != ''){
																		$options = explode('|', $question->input_options);
																	} else {
																		$options = '';
																	}
																?>
																@if ($options != '')
																	@foreach ($options as $k => $option)
																		<label class="radio-inline"> {{ Form::radio('question_'.$question->id, $option, false, ['class' => 'field answer-option']) }} {{ $option }}</label>
																	@endforeach
																@endif

																<div class="comments-row">
																	{{ Form::textarea('question_'.$question->id.'_comment', Input::old('question_'.$question->id.'_comment'), array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Enter your comments...' )) }}
																</div>

																<div class="photos-row">
																	<button class="btn btn-primary"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</button>
																</div>
															@elseif ($question->input_type == 'text')
																{{ Form::text('question_'.$question->id, Input::old('question_'.$question->id), array('class' => 'form-control', 'autocomplete' => 'off')) }}
															@elseif  ($question->input_type == 'textarea')
																{{ Form::textarea('question_'.$question->id, Input::old('question_'.$question->id), array('class' => 'form-control', 'autocomplete' => 'off')) }}
															@endif
														</div>
													</div>
												@endif
											@endforeach
										</div>
										<?php $i++; ?>
									@endforeach
			                	</div>
			                <!-- End Wizard Content -->
			            </div>
			            <!-- End Panel Wizard One Form -->
					</div>
				</div>
			</div>
		</div>
		{{ Form::token() }}
	{{ Form::close() }}
@stop
