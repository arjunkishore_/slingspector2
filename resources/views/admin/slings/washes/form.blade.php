@extends('admin.layouts.master')

@section('bodyclass', 'washes')

@if (isset($wash))
	@section('title', 'Edit Wash')
@else
	@section('title', 'New Wash')
@endif

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
@stop

@section('page-header')
	<div class="page-header">
		@if (isset($wash))
  			<h1 class="page-title">Edit Wash</h1>
		@else
			<h1 class="page-title">New Wash</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings">Slings</a></li>
			<li class="breadcrumb-item"><a href="/slings/washes">Washes</a></li>
			@if (isset($wash))
				<li class="breadcrumb-item active">Edit Wash</li>
			@else
				<li class="breadcrumb-item active">New Wash</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($wash))
						{{ Form::model($wash, array('url' => 'slings/washes/'.$wash->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'slings/washes', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif
						@if(!empty($sling))
							<?php if (Sentinel::getUser()->inRole('admin')){ ?>
								<div class="form-group required row">
									{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
									<div class="col-md-9 col-xs-12">
										{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
									</div>
								</div>
								<?php } else { ?>
									<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
								<?php } ?>
							<div class="form-group row {{ $errors->has('sling_id') ? 'has-error' :'' }}">
								{{ Form::label('sling_id', 'Sling Barcode', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
								<div class="col-md-9 col-xs-12">
									{{ $sling->barcode }}
									<small class="text-help">{{ $errors->first('sling_id', ':message') }}</small>
									<input type="hidden" name="sling_id" value="{{ $sling->id }}" />
								</div>
							</div>
						@else
								@if (!isset($wash))
							    	<?php if (Sentinel::getUser()->inRole('admin')){ ?>
									<div class="form-group required row">
										{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label font-weight-bold')) }}
										<div class="col-md-9 col-xs-12">
											{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
										</div>
									</div>
									<?php } else { ?>
										<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
									<?php } ?>
									<div class="form-group required row {{ $errors->has('sling_id') ? 'has-error' :'' }}">
										{{ Form::label('sling_id', 'Sling', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							      		<div class="col-md-9 col-xs-12">
											{{ Form::select('sling_id', [null => ''] + $slings, Input::old('sling_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
											<small class="text-help">{{ $errors->first('sling_id', ':message') }}</small>
							      		</div>
							    	</div>
								@else
									<div class="form-group row {{ $errors->has('sling_id') ? 'has-error' :'' }}">
										{{ Form::label('sling_id', 'Sling', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							      		<div class="col-md-9 col-xs-12">
											{{ $wash->sling['barcode'] }}
							      		</div>
							    	</div>
									<input type="hidden" name="sling_id" value="{{ $wash->sling_id }}" />
								@endif
						@endif
						<div class="form-group row {{ $errors->has('wash_date') ? 'has-error' :'' }}">
							{{ Form::label('wash_date', 'Wash Date', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('wash_date', Input::old('wash_date'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('wash_date', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('notes') ? 'has-error' :'' }}">
							{{ Form::label('notes', 'Notes', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::textarea('notes', Input::old('notes'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('notes', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
