<?php
use App\Models\Organisation;
use App\Models\Location;
use App\Models\Sling;
use App\Models\Hoist;
use App\Models\User;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
	/**
	 * [$tables description]
	 */
	private $tables =[
		'Organisations', // Organisation should be seeded first
		'Users',
		'Locations',
		'Slings',
		'Hoists',
		'Washes',
		'Repairs',
		'Inspections',
		'SlingQuestions',
		'HoistQuestions',
		'Answers'
	];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		$this->cleanDatabase();
		$this->seedDatabase();
    }
	private function seedDatabase()
	{
		//create seedfile with {{Name}}Seeder
		foreach ($this->tables as $tableName) {
			$this->call($tableName.'Seeder');
		}
	}

	private function cleanDatabase()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		foreach ($this->tables as $tableName) {
			if ($tableName == 'SlingQuestions' || $tableName = 'HoistQuestions'){
				$tableName = 'questions';
			} 
			DB::table(strtolower($tableName))->truncate();
		}
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}
}
