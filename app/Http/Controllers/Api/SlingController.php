<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Sling;
use App\Models\Hoist;
use App\Models\Organisation;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class SlingController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {

		$slings = $id ? Organisation::find($id)->Slings : Sling::with('location')->orderBy('last_6month_inspection', 'ASC')->get();

        foreach ($slings as $sling){
            $sling->type_display = ucwords(str_replace("-", " ", $sling->type));
        }

		return $this->respond([
			'data'=> $slings->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Date of Manufacture
		$dom_sp = explode("T", $request->get('data')['date_of_manufacture']);

        if (isset($dom_sp[0])){
            $dom_dt = Carbon::createFromFormat('Y-m-d', $dom_sp[0])->format('d/m/Y');
        } else {
            $dom_dt = null;
        }

        $dop_sp = explode("T", $request->get('data')['date_of_purchase']);

        if (isset($dop_sp[0])){
            $dop_dt = Carbon::createFromFormat('Y-m-d', $dop_sp[0])->format('d/m/Y');
        } else {
            $dop_dt = null;
        }

        $doc_sp = explode("T", $request->get('data')['date_of_decommission']);

        if (isset($doc_sp[0])){
            $dod_dt = Carbon::createFromFormat('Y-m-d', $doc_sp[0])->format('d/m/Y');
        } else {
            $dod_dt = null;
        }

		$sling = new Sling;
        $sling->organisation_id = $request->get('organisation');
        $sling->barcode = $request->get('data')['barcode'];
        $sling->manufacturer = $request->get('data')['manufacturer'];
        $sling->name = $request->get('data')['name'];
        $sling->product_code = $request->get('data')['product_code'];
        $sling->size = $request->get('data')['size'];
        $sling->swl = $request->get('data')['swl'];
        $sling->unit = $request->get('data')['unit'];
        $sling->type = $request->get('data')['type'];
        $sling->date_of_manufacture = $dom_dt;
        $sling->date_ofpurchase = $dop_dt;
        $sling->end_of_life = $dod_dt;
        $sling->notes = $request->get('data')['notes'];
		$sling->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $sling->id,
			'message'=> 'Sling succesfully created',
			'success'=> true
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$sling = Sling::with('location')->where('id', $id)->first();
        $sling->type_display = ucwords(str_replace("-", " ", $sling->type));
		if (!$sling)
		{
			return $this->respondNotFound('Sling Not Found');
		}
		else {
			return $this->respond(['data'=>$sling,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
    }

	public function find($code){
		$sling = Sling::with('location')->where('barcode', $code)->first();
        $sling->type_display = ucwords(str_replace("-", " ", $sling->type));
        
		if (!$sling){
			$hoist = Hoist::with('location')->where('barcode', $code)->first();
			
			if (!$hoist){
				return $this->respondNotFound('Not Found');
			} else {
				return $this->respond(['data'=>$hoist,
				"payload" => "Slingspector API v1.0",
				"type" => "hoist",
				"success" => true]);
			}

			return $this->respondNotFound('Not Found');
		} else {
			return $this->respond(['data'=>$sling,
			"payload" => "Slingspector API v1.0",
			"type" => "sling",
			"success" => true]);
		}
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		try{
            if ($sling = Sling::find($id)){
                $sling->update(Input::all());
                $sling->save();
                return $this->respond(['success' => true, 'message' => 'Sling information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update Sling id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try{
			if ($sling = Sling::find($id)){
				$sling->delete();
				return $this->respondDeleted('Sling Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
    }
}
