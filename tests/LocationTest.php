<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Location;
use App\Models\Organisation;

class LocationTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_location(){

		$this->getJson('api/v1/locations');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_location()
	{
		// check first row
		$location = $this->getJson('api/v1/locations')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($location, 'organisation_id','name');
	}

	/** @test */
	public function it_deletes_a_single_location()
	{
		// check first row id
		$locationId = $this->getJson('api/v1/locations')->data[0]->id;

		$response = $this->getJson('api/v1/locations/'.$locationId,'DELETE');
		// 202 expected on delete

		$this->assertResponseStatus(202);

	}

	/** @test */
    public function it_404s_if_a_location_is_not_found()
    {
        $json = $this->getJson('api/v1/locations/asdfas');
        $this->assertResponseStatus(404);
		$this->assertObjectHasAttributes($json, 'error');

    }


	/** @test */
	public function it_updates_a_location()
	{
		// check first row id
		$locationId = $this->getJson('api/v1/locations')->data[0]->id;
		// dd($locationId);
		$response = $this->getJson('api/v1/locations/'.$locationId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makelocation( $locationFields = [])
	{
		// Get collection of ids from organisation and get the array of id using all()
		$organisationIds = Organisation::pluck('id')->all();
		$location = array_merge([
			'id' 		        => $faker->uuid,
			'organisation_id'  	=> $faker->randomElement($organisationIds),
			'name'				=> $faker->company
		], $locationFields);
		while ($this->times--) { location::create($location);
		}
	}

	private function getStub()
	{
		return [
			'name' => 'Arch Technologies'
		];
	}


}
