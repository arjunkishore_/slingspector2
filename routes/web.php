<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
// Route::get('/', function () {
//     return view('welcome');
// });
//
// Auth::routes();
//
// Route::get('/home', 'HomeController@index');

Route::get('/', function(){ return redirect('dashboard'); });
Route::get('login', array('as' => 'login', 'uses' => 'Admin\AuthController@getLogin'));
Route::post('login', 'Admin\AuthController@postLogin');
Route::get('logout', 'Admin\AuthController@logout');
Route::get('test','Admin\DashboardController@test');

Route::group(array('middleware' => 'SentinelUser'), function () {
	Route::resource('dashboard','Admin\DashboardController');
	Route::resource('organisations','Admin\OrganisationsController');
	Route::resource('locations','Admin\LocationsController');
	Route::resource('users','Admin\UsersController');
	Route::get('/helpcenter', 'Admin\DashboardController@helpcenter');
	Route::group(['prefix'=>'slings'], function(){
		Route::resource('inspections', 'Admin\SlingInspectionsController');
		Route::resource('washes', 'Admin\SlingWashesController');
		Route::resource('repairs', 'Admin\SlingRepairsController');
	});
	Route::get('slings/3rd-party-inspection', 'Admin\SlingsController@new3rdPartyInspection');
	Route::post('slings/3rd-party-inspection', 'Admin\SlingsController@save3rdPartyInspection');
	Route::resource('slings','Admin\SlingsController');
	Route::get('slings/{id}/inspection', 'Admin\SlingsController@newInspection');
	Route::get('slings/{id}/3rd-party-inspection', 'Admin\SlingsController@new3rdPartyInspection');
	Route::get('slings/{id}/wash', 'Admin\SlingsController@newWash');
	Route::get('slings/{id}/repair', 'Admin\SlingsController@newRepair');

	Route::group(['prefix'=>'hoists'], function(){
		Route::resource('inspections', 'Admin\HoistInspectionsController');
		Route::resource('repairs', 'Admin\HoistRepairsController');
	});
	Route::get('hoists/3rd-party-inspection', 'Admin\HoistsController@new3rdPartyInspection');
	Route::post('hoists/3rd-party-inspection', 'Admin\HoistsController@save3rdPartyInspection');
	Route::resource('hoists','Admin\HoistsController');
	Route::get('hoists/{id}/inspection', 'Admin\HoistsController@newInspection');
	Route::get('hoists/{id}/3rd-party-inspection', 'Admin\HoistsController@new3rdPartyInspection');
	Route::get('hoists/{id}/repair', 'Admin\HoistsController@newRepair');
	Route::group(['prefix'=>'help-centre'], function(){
		Route::resource('training-videos', 'Admin\HelpCentreController@trainingVideos');
		Route::resource('how-to-videos', 'Admin\HelpCentreController@howToVideos');
		Route::resource('how-to-guide', 'Admin\HelpCentreController@howToGuide');
		Route::resource('reference', 'Admin\HelpCentreController@referenceMaterial');
	});
});
