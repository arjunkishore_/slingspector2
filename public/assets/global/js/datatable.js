// Datatable w/ column searching
// ---------------------------
(function() {
  	$(document).ready(function() {
		var defaults = $.components.getDefaults("dataTable");

		var options = $.extend(true, {}, defaults, {
	  		initComplete: function() {
				this.api().columns().every(function() {
		  		var column = this;
				/*
		  		if (column.header().innerText != "Actions"){
			  		var select = $('<select class="form-control width-full"><option value=""></option></select>')
					.appendTo($(column.footer()).empty())
					.on('change', function() {
				  		var val = $.fn.dataTable.util.escapeRegex(
							$(this).val()
				  		);

				  		column
						.search(val ? '^' + val + '$' : '', true, false)
						.draw();
					});

			  		column.data().unique().sort().each(function(d, j) {
						select.append('<option value="' + d + '">' + d + '</option>')
			  		});
		  		} else {
					$(column.footer()).empty()
				}
				*/
			});
		},
		iDisplayLength: 25,
		aLengthMenu: [
	  		[25, 50, 100, -1],
	  		[25, 50, 100, "All"]
		],
		order: [],
    	columnDefs: [ {
	      	targets  : 'no-sort',
	      	orderable: false,
	    }],
      "sDom": '<"buttonarea">frtp<"datatable-footer-container"il>'
	});

	$('.table-search').DataTable(options);
  });
})();
