<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Activation;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Sentinel;
use Redirect;

class AuthController extends Controller
{
	/**
     * Account sign in.
     *
     * @return View
     */
    public function getLogin()
    {
        // Is the user logged in?
        if (Sentinel::check()) {
            return redirect('dashboard');
        }

        // Show the login page
        return view('admin.login');
    }

	public function logout()
    {
        Sentinel::logout(null, true);
        // Show the login page
        return view('admin.login');
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postLogin(Request $request)
    {
        try {
            // Try to log the user in
            if (strcmp($request->get('remember-me'), 'on') ? Sentinel::authenticateAndRemember($request->only('email', 'password')) : Sentinel::authenticate($request->only('email', 'password'))) {

				return redirect("dashboard");
            } else {
                return Redirect::to('login')->with('error', 'Email or password is incorrect.');
            }

        } catch (UserNotFoundException $e) {
            $this->messageBag->add('email', 'Email or password is incorrect.');
        } catch (NotActivatedException $e) {
            $this->messageBag->add('email', 'This user account is not activated.');
        } catch (UserSuspendedException $e) {
            $this->messageBag->add('email', 'User account suspended.');
        } catch (UserBannedException $e) {
            $this->messageBag->add('email', 'This user account is banned.');
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $this->messageBag->add('email', 'User account suspended because of too many login attempts. Try again after '.$delay.' seconds');
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }
}
