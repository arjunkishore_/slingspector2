@extends('admin.layouts.master')

@section('bodyclass', 'slings')

@if (isset($sling))
	@section('title', 'Edit Sling')
@else
	@section('title', 'New Sling')
@endif

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
@stop

@section('page-header')
	<div class="page-header">
		@if (isset($sling))
  			<h1 class="page-title">Edit Sling</h1>
		@else
			<h1 class="page-title">New Sling</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings">Slings</a></li>
			@if (isset($sling))
				<li class="breadcrumb-item active">Edit Sling</li>
			@else
				<li class="breadcrumb-item active">New Sling</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($sling))
						{{ Form::model($sling, array('url' => 'slings/'.$sling->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'slings', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif
								@if (isset($sling))
								<div class="form-group row">
									{{ Form::label('barcode', 'Sling ID', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
									<div class="col-md-9 col-xs-12">
										{{ Form::text('barcode', Input::old('barcode'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
										<small class="text-help">{{ $errors->first('barcode', ':message') }}</small>
								@else
								<div class="form-group required row {{ $errors->has('barcode') ? 'has-error' :'' }}">
									{{ Form::label('barcode', 'Sling ID', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
									<div class="col-md-9 col-xs-12">
									{{ Form::text('barcode', Input::old('barcode'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
									<small class="text-help">{{ $errors->first('barcode', ':message') }}</small>
								@endif
							</div>
						</div>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						<div class="form-group required row {{ $errors->has('organisation_id') ? 'has-error' :'' }}">
							{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
						    <div class="col-md-9 col-xs-12">
								{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('organisation_id', ':message') }}</small>
				      		</div>
				    	</div>
						<?php } else { ?>
							<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
						<?php } ?>
						<div class="form-group row {{ $errors->has('location_id') ? 'has-error' :'' }}">
							{{ Form::label('location_id', 'Facility', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::select('location_id', [null => ''] + $locations, Input::old('location_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('location_id', ':message') }}</small>
				      		</div>
				    	</div>
						<div class="form-group row {{ $errors->has('manufacturer') ? 'has-error' :'' }}">
							{{ Form::label('manufacturer', 'Manufacturer Name', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('manufacturer', Input::old('manufacturer'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('manufacturer', ':message') }}</small>
							</div>
						</div>
						<div class="form-group row {{ $errors->has('name') ? 'has-error' :'' }}">
							{{ Form::label('name', 'Sling Product Name', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('name', ':message') }}</small>
							</div>
						</div>
						<div class="form-group row {{ $errors->has('product_code') ? 'has-error' :'' }}">
							{{ Form::label('product_code', 'Sling Product Code', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('product_code', Input::old('product_code'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('product_code', ':message') }}</small>
							</div>
						</div>
						<div class="form-group row {{ $errors->has('size') ? 'has-error' :'' }}">
							{{ Form::label('size', 'Size', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('size', Input::old('size'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('size', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('location_id') ? 'has-error' :'' }}">
							{{ Form::label('type', 'Sling Type', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::select('type', $types, Input::old('type'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('type', ':message') }}</small>
				      		</div>
				    	</div>
						<div class="form-group row {{ $errors->has('swl') ? 'has-error' :'' }}">
							{{ Form::label('swl', 'Safe Working Load', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-6 col-xs-12">
								{{ Form::text('swl', Input::old('swl'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('swl', ':message') }}</small>
				      		</div>
							<div class="form-group required row {{ $errors->has('unit') ? 'has-error' :'' }}">
								<div class="col-md-2">
									{{ Form::select('unit', $units, Input::old('unit'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
									<small class="text-help">{{ $errors->first('unit', ':message') }}</small>
								</div>
							</div>
					    </div>

						<div class="form-group row {{ $errors->has('date_of_manufacture') ? 'has-error' :'' }}">
							{{ Form::label('date_of_manufacture', 'Date of Manufacture', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('date_of_manufacture', Input::old('date_of_manufacture'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy' )) }}
								<small class="text-help">{{ $errors->first('date_of_manufacture', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('date_of_purchase') ? 'has-error' :'' }}">
							{{ Form::label('date_of_purchase', 'Date of Purchase', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('date_of_purchase', Input::old('date_of_purchase'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('date_of_purchase', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('end_of_life') ? 'has-error' :'' }}">
							{{ Form::label('end_of_life', 'Date of Decommission', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('end_of_life', Input::old('end_of_life'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('end_of_life', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('photo') ? 'has-error' :'' }}">
							{{ Form::label('photo', 'Photo', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('photo', Input::old('photo'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('photo', ':message') }}</small>
							</div>
						</div>
						<div class="form-group row {{ $errors->has('close_photo') ? 'has-error' :'' }}">
							{{ Form::label('close_photo', 'Close Photo', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('close_photo', Input::old('close_photo'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('close_photo', ':message') }}</small>
							</div>
						</div>
						<div class="form-group row {{ $errors->has('notes') ? 'has-error' :'' }}">
							{{ Form::label('notes', 'Notes', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::textarea('notes', Input::old('notes'), array('class' => 'form-control')) }}
								<small class="text-help">{{ $errors->first('notes', ':message') }}</small>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
