<?php

use Faker\Factory as Faker;
use App\Models\Organisation;
use Illuminate\Database\Seeder;

class OrganisationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		$locations = array(
			'USA',
			'England',
			'Scotland',
			'Ireland',
			'Wales',
			'Rest of Europe',
			'Australia',
			'Rest of World'
		);

		DB::table('organisations')->insert(
			array(
                'id' => 0,
                'name' => 'Arizon Pty Ltd',
                'address_1' => '',
                'suburb' => 'Osborne Park',
                'postcode' => '',
                'state' => 'WA',
                'type' => 'business',
                'is_active' => true,
				'location' => 'Australia'
			)
		);

		Organisation::create(
            array(
                'id' => $faker->uuid,
                'name' => 'Roobix Pty Ltd',
                'address_1' => ' 1/105 Royal St',
                'suburb' => 'East Perth ',
                'postcode' => '6004',
                'state' => 'WA',
                'type' => 'business',
                'is_active' => true,
				'location' => 'Australia'
			)
		);

		foreach (range(1,15) as $index ) {
			Organisation::create([
				'id' 		=> $faker->uuid,
				'name'  	=> $faker->company,
				'address_1'	=> $faker->streetAddress,
				'address_2'	=> $faker->streetAddress,
				'suburb' 	=> $faker->city,
				'postcode' 	=> $faker->postcode,
				'state' 	=> $faker->state,
				'country'	=> 'Australia',
				'phone'		=> $faker->phoneNumber,
				'type'		=> 'business',
				'is_active' => $faker->boolean(200),
				'location'	=> $faker->randomElement($locations),
			]);
		}
    }
}
