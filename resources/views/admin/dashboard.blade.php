@extends('admin.layouts.master')

@section('bodyclass', 'dashboard')

@section('title', 'Dashboard')

@section('css')
  	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
@endsection

@section('page-header')
	<!--
	<div class="page-header">
  		<h1 class="page-title">Dashboard</h1>
  		<div class="page-header-actions">
  		</div>
	</div>
	-->
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<a class="dashboard-box" href="/slings/create">
				<div class="quickstart">Quickstart</div>
				<div class="details">New Sling Profile</div>
			</a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<a class="dashboard-box" href="/slings/create">
				<div class="quickstart">Quickstart</div>
				<div class="details">Sling Inspection</div>
			</a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<a class="dashboard-box" href="/slings">
				<div class="quickstart">Quickstart</div>
				<div class="details">Decommission Sling</div>
			</a>
		</div>
	</div>
@endsection
