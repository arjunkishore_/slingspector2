<?php
use Faker\Factory as Faker;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Inspection;
use Illuminate\Database\Seeder;

class AnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker =Faker::create('en_AU');
		$inspectionIds= Inspection::pluck('id')->all();
		$questionIds  = Question::pluck('id')->all();
		
		foreach (range(1,15) as $index) {
			Answer::create([
				'id'			=> $faker->uuid,
				'inspection_id' => $faker->randomElement($inspectionIds),
				'question_id'	=> $faker->randomElement($questionIds),
				'answer'		=> $faker->text,
				'photo'			=> $faker->md5,
				'comments'		=> $faker->text
			]);
		}
    }
}
