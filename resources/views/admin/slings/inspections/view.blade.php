@extends('admin.layouts.master')

@section('bodyclass', 'inspection-view')

@section('title')
	{{ 'Inspection' }}
@stop

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Sling Inspection</h1>
  		<div class="page-header-actions">

  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings/inspections">Inspections</a></li>
			<li class="breadcrumb-item"><a href="/slings/{{$inspection->sling['id']}}">Sling {{$inspection->sling['barcode']}}</a></li>
			<li class="breadcrumb-item active">Inspection</li>
		</ol>
	</div>
@stop

@section('content')
<div class="message-holder">
	@include('notifications')
</div>

<div class="panel">
	<div class="panel-body container-fluid">
		<div class="row row-lg">
			<div class="col-md-12 col-lg-12 col-xs-12">
				<?php if (Sentinel::getUser()->inRole('admin')){ ?>
				<div class="form-group row">
					<label class="col-xs-12 col-md-3 form-control-label font-weight-bold">Organisation:</label>
					<div class="col-md-8 col-xs-12">
						{{ $inspection->organisation['name'] }}
					</div>
				</div>
				<?php } ?>
				<div class="form-group row">
					<label class="col-xs-12 col-md-3 form-control-label font-weight-bold">Sling ID:</label>
					<div class="col-md-9 col-xs-12">
						{{ $inspection->sling['barcode'] }}
					</div>
				</div>
				@foreach ($headings as $heading)
					<h3>{{ $heading->heading }}</h3>
					@foreach ($answers as $answer)
						@if ($answer->heading == $heading->heading)
							<div class="form-group">
								<label class="form-control-label font-weight-bold">{{ $answer->question }}</label>
								<div class="">
									{{ $answer->answer['answer'] }}
								</div>
							</div>
						@endif
					@endforeach
				@endforeach
			</div>
		</div>
	</div>
</div>
@stop
