@extends('admin.layouts.master')

@section('bodyclass', 'slings')

@section('title', 'Slings')

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Slings</h1>
  		<div class="page-header-actions">
	  		<a href="/slings/create" class="btn btn-slingspector">Create New Sling Profile</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item active">Slings</li>
		</ol>
	</div>
@stop

@section('content')
	<!-- Panel -->
	<div class="message-holder">
		@include('notifications')
	</div>

	<div class="panel">
	  	<!--
		<header class="panel-heading">
			<h3 class="panel-title">Individual Column Searching</h3>
	  	</header>
		-->
	  	<div class="panel-body">
			<table class="table table-hover dataTable table-striped table-search w-full" id="slingsTable">
		  		<thead>
					<tr>
						<th>Sling ID</th>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?><th>Organisation</th><?php } ?>
						<th>Location</th>
						<th>Last Inspection</th>
						<th class="no-sort">Actions</th>
					</tr>
		  		</thead>
		  		<!--
				<tfoot>
					<tr>
						<th>Code</th>
						<th>Organisation</th>
						<th>Location</th>
						<th>Last Inspection</th>
						<th>Actions</th>
					</tr>
		  		</tfoot>
				-->
		  		<tbody>
					<?php
						if (isset($slings)){
							foreach ($slings as $sling){
								echo '<tr id="'.$sling->id.'">';
									echo '<td>'.$sling->barcode.'</td>';
									if (Sentinel::getUser()->inRole('admin')){ echo '<td>'.$sling->organisation['name'].'</td>'; }
									echo '<td>'.$sling->location['name'].'</td>';
									if($sling->last_inspection !='Never'){
									$dt = Carbon::createFromFormat('d/m/Y', $sling->last_inspection);
									echo '<td>'.$dt->format('M d, Y').'</td>';
									} else {  echo '<td>'.$sling->last_inspection.'</td>';}
									echo '<td>';
										echo '<a href="/slings/'.$sling->id.'" class="btn btn-xs btn-slingspector action">View</a>';
										echo '<a href="/slings/'.$sling->id.'/edit" class="btn btn-xs btn-primary action">Edit</a>';
										echo '<button data-href="/slings/'.$sling->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$sling->id.'" data-type="Sling" class="btn btn-xs btn-danger action delete">Delete</button>';
									echo '</td>';
								echo '</tr>';
							}
						}
					?>
		  		</tbody>
			</table>
	  	</div>
	</div>
	<!-- End Panel -->
@stop
