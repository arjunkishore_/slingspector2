<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organisation;
use Sentinel;

class HelpCentreController extends Controller
{
    public function trainingVideos(){
		$view['organisation'] = Organisation::find(Sentinel::getUser()->inRole('admin') ? 0 : Sentinel::getUser()->roles()->first()->pivot->organisation_id);
		return view('admin.help-centre.training-videos', $view);
	}

	public function howToGuide(){
		return view('admin.help-centre.how-to-guide');
	}

	public function howToVideos(){
		return view('admin.help-centre.how-to-videos');
	}

	public function referenceMaterial(){
		return view('admin.help-centre.reference');
	}
}
