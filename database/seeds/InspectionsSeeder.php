<?php
use Faker\Factory as Faker;

use App\Models\Inspection;
use App\Models\Organisation;
use App\Models\Sling;
use App\Models\Hoist;
use App\Models\User;
use Illuminate\Database\Seeder;

class InspectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids
		$organisationIds= Organisation::pluck('id')->all();
		$slingIds 		= Sling::pluck('id')->all();
		$hoistIds 		= Hoist::pluck('id')->all();
		$userIds 		= User::pluck('id')->all();

		$types = array(
			'sling',
			'hoist'
		);

		foreach (range(1,25) as $index) {
			$randomSling = $faker->randomElement($slingIds);
			$randomHoist = $faker->randomElement($hoistIds);
			$randomType = $faker->randomElement($types);
			$inspectionDate = $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = date_default_timezone_get());

			if ($randomType == 'sling'){
				$randomType = $faker->randomElement(array('webbing-loops-sling', 'keyhole-plate-sling', 'other-sling'));
				$randomHoist = NULL;
			} else if ($randomType == 'hoist'){
				$randomType = $faker->randomElement(array('stand-up-hoist', 'portable-ceiling-hoist', 'metal-hook-coat-hanger-hoist', 'fixed-ceiling-hoist', 'keyhole-plate-tilt-frame-hoist'));
				$randomSling = NULL;
			}

			Inspection::create([
				'id' 		        => $faker->uuid,
				'organisation_id'  	=> $faker->randomElement($organisationIds),
				'sling_id'			=> $randomSling,
		        'hoist_id'			=> $randomHoist,
		        'user_id'			=> $faker->randomElement($userIds),
		        'type'				=> $randomType,
		        'inspection_date'	=> $inspectionDate,
		        'completed'			=> 1,
			]);

			if ($randomType == 'sling'){
				$sling = Sling::find($randomSling);
				if ($inspectionDate > $sling->last_6month_inspection){
					$sling->last_6month_inspection = $inspectionDate;
					$sling->save();
				}
			} else if ($randomType == 'hoist'){
				$hoist = Hoist::find($randomHoist);
				if ($inspectionDate > $hoist->last_6month_inspection){
					$hoist->last_6month_inspection = $inspectionDate;
					$hoist->save();
				}
			}
		}
    }
}
