<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use App\Models\Inspection;
use App\Models\Sling;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InspectionController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id = null)
	{
		//
		$inspections = $id ? Sling::find($id)->inspection : Inspection::all();
		return $this->respond([
			'data'=> $inspections->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$inspection= new Inspection;
		$inspection['attributes']= $request->all();
		$inspection->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $inspection->id,
			'message'=> 'Inspection succesfully created',
			'success'=> true
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
		$inspection = Inspection::find($id);
		if (!$inspection)
		{
			return $this->respondNotFound('Inspection Not Found');
		}
		else {
			return $this->respond(['data'=>$inspection,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		try{
			if ($inspection = Inspection::find($id)){
				$inspection->update(Input::all());
				$inspection->save();
				return $this->respond(['success' => true, 'message' => 'Inspection information updated']);
			} else {
				return $this->respond(['success' => false, 'message' => 'Unable to update inspection id->'.$id]);
			}
		}
		catch(Exception $e){
		   // do task when error
		   return $this->respond(['success' => false, 'message' => $e->getMessage()]);
		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		try{
			if ($inspection = Inspection::find($id)){
				$inspection->delete();
				return $this->respondDeleted('Inspection Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
	}


	/**
	 * Display answers for the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function answers($id)
	{
		$answers = Inspection::with(array('answers' => function($query){
            $query->join('questions AS q', 'q.id', '=', 'answers.question_id');
        }))->find($id);

		if (!$answers)
		{
			return $this->respondNotFound('Answers Not Found');
		}
		else {
			return $this->respond(['data'=> $answers,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
	}

}
