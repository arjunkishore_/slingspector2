<?php

use Faker\Factory as Faker;
use App\Models\Location;
use App\Models\Organisation;
use Illuminate\Database\Seeder;


class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids from organisation and get the array of id using all()
		$organisationIds = Organisation::pluck('id')->all();
		
		foreach (range(1,15) as $index ) {
			Location::create([
				'id' 		        => $faker->uuid,
				'organisation_id'  	=> $faker->randomElement($organisationIds),
				'name'				=> $faker->company
			]);
		}
    }
}
