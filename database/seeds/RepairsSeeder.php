<?php
use Faker\Factory as Faker;
use App\Models\Repair;
use App\Models\User;
use App\Models\Sling;
use App\Models\Hoist;
use Illuminate\Database\Seeder;

class RepairsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids from organisation and get the array of id using all()
		$slingIds 	= DB::table('slings')->pluck('id')->all();
		$hoistIds 	= DB::table('hoists')->pluck('id')->all();
		$userIds 	= DB::table('users')->pluck('id')->all();

		foreach (range(1,15) as $index) {
			Repair::create( [
				'id' 		=> $faker->uuid,
				'sling_id' 	=> $faker->randomElement($slingIds),
				'user_id' 	=> $faker->randomElement($userIds),
				'hoist_id' 	=> $faker->randomElement($hoistIds),
				'photos' 	=> $faker->uuid,
				'notes' 	=> $faker->realText($maxNbChars = 200, $indexSize = 2),
				'repair_complete'=>$faker->dateTime,
				'repair_date'=>$faker->dateTime,
				'repair_by'=>$faker->dateTime
			]);
		}

    }
}
