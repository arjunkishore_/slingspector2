<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('answers', function( Blueprint $table){
			$table->uuid('id')->index();
			$table->string('inspection_id')->index();
			$table->foreign('inspection_id')->references('id')->on('inspections')->onDelete('cascade');
			$table->string('question_id')->index();
			$table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
			$table->string('answer');
			$table->string('photo')->nullable();
			$table->text('comments')->nullable();
			// $table->string('delete_reason');
			// $table->string('deleted_by');
			$table->dateTime('deleted_at');
			$table->timestamps('updated_at');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('answers');
	}
}
