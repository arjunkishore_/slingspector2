<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::table('users', function(Blueprint $table){
			$table->string('phone');
		});

		Schema::table('role_users', function(Blueprint $table){
			$table->uuid('organisation_id')->nullable();
			$table->primary(['user_id', 'role_id', 'organisation_id']);
			$table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::disableForeignKeyConstraints();

		Schema::table('users', function(Blueprint $table){
			// delete above columns
			$table->dropColumn(array('phone'));
		});

		Schema::table('role_users', function(Blueprint $table){
			// delete above columns
			$table->dropForeign('role_users_organisation_id_foreign');
			$table->dropColumn(array('organisation_id'));
		});

		Schema::enableForeignKeyConstraints();
	}

}
