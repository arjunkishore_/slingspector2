@extends('admin.layouts.master')

@section('bodyclass', 'washes wash')

@section('title')
	{{ 'Wash - Sling '.$wash->sling['barcode'] }}
@stop

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Wash - Sling {{ $wash->sling['barcode'] }}</h1>
  		<div class="page-header-actions">
			<a href="/slings/washes/{{ $wash->id }}/edit" class="btn btn-primary">Edit</a>
			<a data-href="/slings/washes/{{ $wash->id }}" data-toggle="modal" data-target="#confirm-delete" data-row="{{ $wash->id }}" data-type="Wash" class="btn btn-danger delete">Delete</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/slings">Slings</a></li>
			<li class="breadcrumb-item"><a href="/slings/washes">Washes</a></li>
			<li class="breadcrumb-item active">Wash - Sling {{ $wash->sling['barcode'] }}</li>
		</ol>
	</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="panel">
		  		<div class="panel-body container-fluid">
					<div class="row row-lg">
						<div class="col-md-12 col-lg-12 col-xs-12">
							<div class="form-group row {{ $errors->has('sling_id') ? 'has-error' :'' }}">
								{{ Form::label('sling_id', 'Sling', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $wash->sling['barcode'] }}
					      		</div>
					    	</div>
							<div class="form-group row {{ $errors->has('wash_date') ? 'has-error' :'' }}">
								{{ Form::label('wash_date', 'Wash Date', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $wash->wash_date }}
					      		</div>
						    </div>
							<div class="form-group row {{ $errors->has('notes') ? 'has-error' :'' }}">
								{{ Form::label('notes', 'Notes', array('class' => 'col-xs-12 col-md-3 form-control-label  font-weight-bold')) }}
					      		<div class="col-md-12 col-xs-12 col-lg-12">
									{{ $wash->notes }}
					      		</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
