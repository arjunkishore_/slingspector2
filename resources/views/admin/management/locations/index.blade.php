@extends('admin.layouts.master')

@section('bodyclass', 'locations')

@section('title', 'Facilities')

@section('css')
	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.css">
	<link rel="stylesheet" href="/assets/global/vendor/datatables-responsive/dataTables.responsive.css">
	<link rel="stylesheet" href="/assets/base/examples/css/tables/datatable.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/datatables/jquery.dataTables.js"></script>
	<script src="/assets/global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
	<script src="/assets/global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
	<script src="/assets/global/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<script src="/assets/global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

	<script src="/assets/global/js/components/datatables.js"></script>
	<script src="/assets/global/js/datatable.js"></script>
    <!--<script src="/assets/base/examples/js/tables/datatable.js"></script>-->
@stop

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Facilities</h1>
  		<div class="page-header-actions">
	  		<a href="/locations/create" class="btn btn-slingspector">Add New</a>
  		</div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item active">Facilities</li>
		</ol>
	</div>
@stop

@section('content')
	<!-- Panel -->
	<div class="message-holder">
		@include('notifications')
	</div>

	<div class="panel">
	  	<!--
		<header class="panel-heading">
			<h3 class="panel-title">Individual Column Searching</h3>
	  	</header>
		-->
	  	<div class="panel-body">
			<table class="table table-hover dataTable table-striped w-full table-search" id="locationsTable">
		  		<thead>
					<tr>
						<?php if (Sentinel::getUser()->inRole('admin')){ ?><th>Organisation</th><?php } ?>
						<th>Name</th>
						<th class="no-sort">Actions</th>
					</tr>
		  		</thead>
		  		<!--
				<tfoot>
					<tr>
						<th>Organisation</th>
						<th>Name</th>
						<th>Actions</th>
					</tr>
		  		</tfoot>
				-->
		  		<tbody>
					<?php
						if (isset($locations)){
							foreach ($locations as $location){
								echo '<tr id="'.$location->id.'">';
									if (Sentinel::getUser()->inRole('admin')){ echo '<td>'.$location->organisation['name'].'</td>'; }
									echo '<td>'.$location->name.'</td>';
									echo '<td>';
										echo '<a href="/locations/'.$location->id.'/edit" class="btn btn-xs btn-primary action">Edit</a>';
										echo '<button data-href="/locations/'.$location->id.'" data-toggle="modal" data-target="#confirm-delete" data-row="'.$location->id.'" data-type="Location" class="btn btn-xs btn-danger action delete">Delete</button>';
									echo '</td>';
								echo '</tr>';
							}
						}
					?>
		  		</tbody>
			</table>
	  	</div>
	</div>
	<!-- End Panel -->
@stop
