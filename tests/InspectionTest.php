<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Inspection;
use App\Models\Sling;

class InspectionTest extends ApiTester
{
	function __construct()
	{
		parent::__construct();
	}
	use DatabaseTransactions;

	/** @test */
	public function it_fetches_inspection(){

		$this->getJson('api/v1/inspections');

		$this->assertResponseOk();
	}

	/** @test */
	public function it_fetches_a_single_inspection()
	{
		// check first row
		$inspection = $this->getJson('api/v1/inspections')->data[0];
		$this->assertResponseOk();
		// verify if these fields exist
		$this->assertObjectHasAttributes($inspection, 'sling_id','type');
	}

	/** @test */
	public function it_deletes_a_single_inspection()
	{
		// check first row id
		$inspectionId = $this->getJson('api/v1/inspections')->data[0]->id;

		$response = $this->getJson('api/v1/inspections/'.$inspectionId,'DELETE');
		// 202 expected on delete

		$this->assertResponseStatus(202);

	}

	/** @test */
    public function it_404s_if_a_inspection_is_not_found()
    {
        $json = $this->getJson('api/v1/inspections/asdfas');
        $this->assertResponseStatus(404);

		$this->assertObjectHasAttributes($json, 'error');

    }


	/** @test */
	public function it_updates_a_inspection()
	{
		// check first row id
		$inspectionId = $this->getJson('api/v1/inspections')->data[0]->id;
		// dd($inspectionId);
		$response = $this->getJson('api/v1/inspections/'.$inspectionId,'PUT',$this->getStub());

		$this->assertResponseStatus(200);
	}


	private function makeInspection( $inspectionFields = [])
	{
		// Get collection of ids from organisation and get the array of id using all()
		$slingIds = Sling::pluck('id')->all();
		$inspection = array_merge([
			'id' 		        => $faker->uuid,
			'organisation_id'  	=> $faker->randomElement($organisationIds),
			'sling_id'			=> $faker->randomElement($slingIds),
			//'hoist_id',		=> $faker->randomElement($hoistIds),
			//'user_id',		=> $faker->randomElement($userIds),
			'type'				=> $faker->realText($faker->numberBetween(10,11)),
			'inspection_date'	=> $faker->dateTimeThisMonth($max = 'now', $timezone = date_default_timezone_get()) ,
			'completed'			=> $faker->boolean(50),
		], $inspectionFields);
		while ($this->times--) { Inspection::create($inspection);
		}
	}

	private function getStub()
	{
		return [
			'type' => 'Arch Technologies'
		];
	}


}
