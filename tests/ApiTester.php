<?php
use Faker\Factory as Faker;
abstract class ApiTester extends TestCase
{
	protected $fake;
	protected $times = 1;

	function __construct()
	{
		$this->fake = Faker::create();
	}

	protected function times($count)
	{
		$this->times = $count;
		return $this;
	}

	public function getJson($uri, $method = 'GET', $parameters = [])
	{
		return json_decode($this->call($method, $uri, $parameters)->getContent());
	}

  /**
   * [assertObjectHasAttributes description]
   * @return [type] [description]
   */
    protected function assertObjectHasAttributes()
    {
		$args = func_get_args();
	   	$object = array_shift($args);
        foreach ($args as $attribute)
        {
			$this->assertObjectHasAttribute($attribute, $object);
        }
    }




}
