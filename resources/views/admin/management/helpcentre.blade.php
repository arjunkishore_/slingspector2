@extends('admin.layouts.master')

@section('bodyclass', 'dashboard')

@section('title', 'Dashboard')

@section('css')
  	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
	<link rel="stylesheet" href="/assets/global/vendor/animsition/animsition.css">
	<link rel="stylesheet" href="/assets/global/vendor/asscrollable/asScrollable.css">
	<link rel="stylesheet" href="/assets/global/vendor/switchery/switchery.css">
	<link rel="stylesheet" href="/assets/global/vendor/intro-js/introjs.css">
	<link rel="stylesheet" href="/assets/global/vendor/slidepanel/slidePanel.css">
	<link rel="stylesheet" href="/assets/global/vendor/flag-icon-css/flag-icon.css">
	<link rel="stylesheet" href="/assets/global/vendor/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="/assets/global/css/advanced/lightbox.css">
@endsection

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">SlingSpector Helpcenter</h1>
  		<div class="page-header-actions">
  		</div>
	</div>
@endsection

@section('content')
	<div class="row" data-plugin="matchHeight" data-by-row="true">
		<div class="col-xxl-6 col-lg-6 col-xs-12">
			<div class="panel">
            	<div class="panel-heading">
              		<h3 class="panel-title">FAQs</h3>
            	</div>
            	<div class="panel-body">
					<div class="col-lg-6 col-xs-12">
              <!-- Example Lightbox Gallery -->
              <div class="example-wrap">
                <h4 class="example-title">Slings</h4>
                <p>In this example lazy-loading of images is enabled for the next
                  image based on move direction.</p>
                <div class="example" id="exampleGallery">
                  <a class="inline-block" href="/assets/global/images/hoist1.png" title="view-4">
                    <img class="img-fluid" src="/assets/global/images/hoist1.png" alt="..." width="220">
                  </a>
                  <a class="inline-block" href="/assets/global/images/hoist1.png" title="view-5">
                    <img class="img-fluid" src="/assets/global/images/hoist1.png" alt="..." width="220">
                  </a>
                  <a class="inline-block" href="/assets/global/images/hoist1.png" title="view-6">
                    <img class="img-fluid" src="/assets/global/images/hoist1.png" alt="..." width="220">
                  </a>
                </div>
              </div>
              <!-- End Example Gallery -->
            </div>
            	</div>
          	</div>
		</div>
		<div class="col-xxl-6 col-lg-6 col-xs-12">
			<div class="panel">
            	<div class="panel-heading">
              		<h3 class="panel-title">Tutorial Videos</h3>
            	</div>
            	<div class="panel-body">

            	</div>
          	</div>
		</div>
	</div>
@endsection
