@extends('admin.layouts.master')

@section('bodyclass', 'hoists')

@if (isset($hoist))
	@section('title', 'Edit Hoist')
@else
	@section('title', 'New Hoist')
@endif

@section('css')
	<link rel="stylesheet" href="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
@stop

@section('page-javascript')
	<script src="/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
	<script src="/assets/global/js/components/bootstrap-datepicker.js"></script>
@stop

@section('page-header')
	<div class="page-header">
		@if (isset($hoist))
  			<h1 class="page-title">Edit Hoist</h1>
		@else
			<h1 class="page-title">New Hoist</h1>
		@endif
  		<div class="page-header-actions"></div>
		<ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
	        <li class="breadcrumb-item"><a href="/hoists">Hoists</a></li>
			@if (isset($hoist))
				<li class="breadcrumb-item active">Edit Hoist</li>
			@else
				<li class="breadcrumb-item active">New Hoist</li>
			@endif
		</ol>
	</div>
@stop

@section('content')
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					@if (isset($hoist))
						{{ Form::model($hoist, array('url' => 'hoists/'.$hoist->id, 'method' => 'PUT', 'class'=>'form-horizontal')) }}
					@else
						{{ Form::open(array('url' => 'hoists', 'method' => 'POST', 'class'=>'form-horizontal')) }}
					@endif
						<div class="form-group required row {{ $errors->has('barcode') ? 'has-error' :'' }}">
							{{ Form::label('barcode', 'Hoist ID', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
							<div class="col-md-9 col-xs-12">
								{{ Form::text('barcode', Input::old('barcode'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('barcode', ':message') }}</small>
							</div>
						</div>
				    	<?php if (Sentinel::getUser()->inRole('admin')){ ?>
						<div class="form-group required row {{ $errors->has('organisation_id') ? 'has-error' :'' }}">
							{{ Form::label('organisation_id', 'Organisation', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
						    <div class="col-md-9 col-xs-12">
								{{ Form::select('organisation_id', [null => ''] + $organisations, Input::old('organisation_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('organisation_id', ':message') }}</small>
				      		</div>
				    	</div>
						<?php } else { ?>
							<input type="hidden" name="organisation_id" value="<? echo Sentinel::getUser()->roles()->first()->pivot->organisation_id; ?>" />
						<?php } ?>
						<div class="form-group row {{ $errors->has('location_id') ? 'has-error' :'' }}">
							{{ Form::label('location_id', 'Facility', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::select('location_id', [null => ''] + $locations, Input::old('location_id'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('location_id', ':message') }}</small>
				      		</div>
				    	</div>
						<div class="form-group row {{ $errors->has('location_id') ? 'has-error' :'' }}">
							{{ Form::label('type', 'Hoist Type', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::select('type', $types, Input::old('type'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('type', ':message') }}</small>
				      		</div>
				    	</div>
						<div class="form-group row {{ $errors->has('swl') ? 'has-error' :'' }}">
							{{ Form::label('swl', 'Safe Working Load', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-6 col-xs-12">
								{{ Form::text('swl', Input::old('swl'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('swl', ':message') }}</small>
				      		</div>
							<div class="form-group required row {{ $errors->has('unit') ? 'has-error' :'' }}">
								<div class="col-md-2">
									{{ Form::select('unit', $units, Input::old('unit'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
									<small class="text-help">{{ $errors->first('unit', ':message') }}</small>
								</div>
							</div>
					    </div>
						<div class="form-group row {{ $errors->has('manufacturer') ? 'has-error' :'' }}">
							{{ Form::label('manufacturer', 'Manufacturer', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('manufacturer', Input::old('manufacturer'), array('class' => 'form-control', 'autocomplete' => 'off')) }}
								<small class="text-help">{{ $errors->first('manufacturer', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('date_of_manufacture') ? 'has-error' :'' }}">
							{{ Form::label('date_of_manufacture', 'Date of Manufacture', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('date_of_manufacture', Input::old('date_of_manufacture'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy' )) }}
								<small class="text-help">{{ $errors->first('date_of_manufacture', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('date_of_purchase') ? 'has-error' :'' }}">
							{{ Form::label('date_of_purchase', 'Date of Purchase', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('date_of_purchase', Input::old('date_of_purchase'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('date_of_purchase', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group row {{ $errors->has('end_of_life') ? 'has-error' :'' }}">
							{{ Form::label('end_of_life', 'Date of Decommission', array('class' => 'col-xs-12 col-md-3 form-control-label')) }}
				      		<div class="col-md-9 col-xs-12">
								{{ Form::text('end_of_life', Input::old('end_of_life'), array('class' => 'form-control', 'autocomplete' => 'off', 'data-plugin' => 'datepicker', 'data-date-format' => 'dd/mm/yyyy')) }}
								<small class="text-help">{{ $errors->first('end_of_life', ':message') }}</small>
				      		</div>
					    </div>
						<div class="form-group">
							<div class="col-lg-12 col-md-12 col-xs-12">
								{{ Form::submit('Save', array('class' => 'btn btn-primary pull-right')) }}
							</div>
	                    </div>
						{{ Form::token() }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop
