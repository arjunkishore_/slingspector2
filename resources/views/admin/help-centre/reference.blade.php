@extends('admin.layouts.master')

@section('bodyclass', 'help-centre')

@section('title', 'Reference Material | Help Centre')

@section('css')
  	<link rel="stylesheet" href="/assets/base/examples/css/dashboard/v1.css">
@endsection

@section('page-header')
	<div class="page-header">
  		<h1 class="page-title">Reference Material</h1>
  		<div class="page-header-actions">
  		</div>
	</div>
@endsection

@section('content')
	<h4>Slings</h4>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
				<ul class="nav nav-tabs nav-tabs-solid" role="tablist">
					<li class="nav-item active" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#webbing-loops-sling" aria-controls="webbing-loops-sling" role="tab">
							Webbing Loops Sling
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#keyhole-plates-sling" aria-controls="keyhole-plates-sling" role="tab">
							Keyhole Plate Sling
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link active" data-toggle="tab" href="#stand-up-sling" aria-controls="stand-up-sling" role="tab">
							Stand Up Sling
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="webbing-loops-sling" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/webbing-loop-sling.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="keyhole-plates-sling" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/keyhole-plate-sling.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="stand-up-sling" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/stand-up-sling.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
	<h4>Hoists</h4>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<div class="nav-tabs-horizontal nav-tabs-inverse" data-plugin="tabs">
				<ul class="nav nav-tabs nav-tabs-solid" role="tablist">
					<li class="nav-item active" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#trolley-hoist-metal-hook" aria-controls="trolley-hoist-metal-hook" role="tab">
							Trolley Hoist - Metal Hook
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#trolley-hoist-keyhole-plate" aria-controls="trolley-hoist-keyhole-plate" role="tab">
							Trolley Hoist - Keyhole Plate
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#trolley-hoist-stand-up" aria-controls="trolley-hoist-stand-up" role="tab">
							Trolley Hoist - Stand Up
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" data-toggle="tab" href="#ceiling-hoist-portable" aria-controls="ceiling-hoist-portable" role="tab">
							Ceiling Hoist - Portable
						</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link active" data-toggle="tab" href="#ceiling-hoist-fixed" aria-controls="ceiling-hoist-fixed" role="tab">
							Ceiling Hoist - Fixed
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="trolley-hoist-metal-hook" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/metal-hook-coat-hanger-hoist.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="trolley-hoist-keyhole-plate" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/keyhole-plate-tilt-frame-hoist.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="trolley-hoist-stand-up" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/stand-up-hoist.png" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane" id="ceiling-hoist-portable" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/portable-ceiling-hoist.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
					<div class="tab-pane active" id="ceiling-hoist-fixed" role="tabpanel">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-xs-12">
								<img src="/assets/images/diagrams/fixed-ceiling-hoist.jpg" style="max-width: 100%;" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
	<h4>Standards & Regulations</h4>
	<div class="panel">
  		<div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-md-12 col-lg-8 col-xs-12">
					<ul>
						<li><a href="/assets/documents/international-standard.pdf" target="_blank">International Standards</a></li>
						<li><a href="/assets/documents/lifting-operations-and-lifting-equipment-regulations-1998-uk" target="_blank">Lifting Operations and Lifting Equipment Regulations 1998 (UK)</a></li>
						<li><a href="/assets/documents/therapeutic-goods-administration-australia.pdf" target="_blank">Therapeutic Goods Administration (Australia)</a></li>
						<li><a href="/assets/documents/cardiff-and-value-university-local-health-board-wales.pdf" target="_blank">Inspection Proceedures - Cardiff and Vale University Local Health Board (Wales)</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
@endsection
