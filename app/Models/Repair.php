<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
class Repair extends Model
{
  use UuidModelTrait;
	use SoftDeletes;
	protected $fillable = ['id', 'sling_id','hoist_id','user_id', 'photos', 'notes','repair_complete','repair_date','repair_by','deleted_at','delete_reason','deleted_by'];
	public $timestamps = false;

	public function sling()
	{
		return $this->belongsTo('App\Models\Sling');
	}

	public function hoist()
	{
		return $this->belongsTo('App\Models\Hoist');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

	/**
     * Get the repair_date from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getRepairDateAttribute($value)
    {
		$split = explode('-', $value);
		return $split[2].'/'.$split[1].'/'.$split[0];
    }

	public function setRepairDateAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);
			$this->attributes['repair_date'] = $split[2].'-'.$split[1].'-'.$split[0];
		} else {
			$this->attributes['repair_date'] = $value;
		}
    }

	/**
     * Get the repair_date from yyyy-mm-dd to dd/mm/yyyy.
     *
     * @param  string  $value
     * @return string
     */
    public function getRepairCompleteAttribute($value)
    {
		$split = explode('-', $value);
		return $split[2].'/'.$split[1].'/'.$split[0];
    }

	public function setRepairCompleteAttribute($value)
    {
		if (is_string($value)){
			$split = explode('/', $value);
			$this->attributes['repair_complete'] = $split[2].'-'.$split[1].'-'.$split[0];
		} else {
			$this->attributes['repair_complete'] = $value;
		}
    }

}
