<?php

use Faker\Factory as Faker;
use App\Models\Wash;
use App\Models\User;
use App\Models\Sling;
use App\Models\Hoist;
use Illuminate\Database\Seeder;

class WashesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Localising faker to use Australian convention
		$faker = Faker::create('en_AU');

		// Get collection of ids from organisation and get the array of id using all()
		$slingIds 	= DB::table('slings')->pluck('id')->all();
		$userIds 	= DB::table('users')->pluck('id')->all();

		foreach (range(1,15) as $index) {
			Wash::create( [
				'id' 		=> $faker->uuid,
				'sling_id' 	=> $faker->randomElement($slingIds),
				'user_id' 	=> $faker->randomElement($userIds),
				'wash_date' => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now', $timezone = date_default_timezone_get()),
				'photos' 	=> $faker->uuid,
				'notes' 	=> $faker->realText($maxNbChars = 200, $indexSize = 2)
			]);
		}

    } // run()
}
