<?php

namespace App\Http\Controllers\Api;
use Input;
use Response;
use App\Models\Location;
use App\Models\Organisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        //
		$locations = $id ? Organisation::find($id)->locations : Location::all();
		return $this->respond([
			'data'=> $locations->all(),
			'payload'=> 'Slingspector API v1.0',
			'success' => true
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$location= new Location;
		$location['attributes']= $request->all();
		$location->save();

		return $this->setStatusCode(201)->respond([
			'id'	 => $location->id,
			'message'=> 'Location succesfully created',
			'success'=> true
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$location = Location::find($id);
		if (!$location)
		{
			return $this->respondNotFound('Location Not Found');
		}
		else {
			return $this->respond(['data'=>$location,
			"payload" => "Slingspector API v1.0",
			"success" => true]);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		try{
            if ($location = Location::find($id)){
                $location->update(Input::all());
                $location->save();
                return $this->respond(['success' => true, 'message' => 'Location information updated']);
            } else {
				return $this->respond(['success' => false, 'message' => 'Unable to update Location id->'.$id]);
            }
        }
        catch(Exception $e){
           // do task when error
           return $this->respond(['success' => false, 'message' => $e->getMessage()]);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		try{
			if ($location = Location::find($id)){
				$location->delete();
				return $this->respondDeleted('Location Deleted');
			} else {
				return $this->repondUnableToDelete();
			}
		}
		 catch(Exception $e){
		   // do task when error
		   return $this->respondUnableToDelete($e->getMessage());
		}
    }
}
