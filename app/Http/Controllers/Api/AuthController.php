<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Activation;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;
use App\Models\Organisation;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Sentinel;
use Redirect;
use Input;

class AuthController extends ApiController
{
	public function __construct(Request $request)
   	{
       $this->request = $request;
   	}

	/**
     * Account sign in.
     *
     * @return View
     */
    public function getLogin()
    {
        // Is the user logged in?
        if (Sentinel::check()) {
            return redirect('dashboard');
        }

        // Show the login page
        return view('admin.login');
    }

	public function logout()
    {
        Sentinel::logout(null, true);
        // Show the login page
        return view('admin.login');
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postLogin(Request $request)
    {
        try {
            // Try to log the user in
            if ($auth = strcmp($request->get('remember-me'), 'on') ? Sentinel::authenticateAndRemember($request->only('email', 'password')) : Sentinel::authenticate($request->only('email', 'password'))) {
                $auth->organisation_id = Sentinel::getUser()->inRole('admin') ? null : Sentinel::getUser()->roles()->first()->pivot->organisation_id;
                if ($auth->organisation_id){ $auth->location = Organisation::find($auth->organisation_id)->location; }
				return $this->respond([
					'data'=> $auth,
					'payload'=> 'Slingspector API v1.0',
					'success' => true
				]);
            } else {
				return $this->respond([
					'data'=> '',
					'payload'=> 'Slingspector API v1.0',
					'success' => false
				]);
            }

        } catch (UserNotFoundException $e) {
            $this->messageBag->add('email', 'Email or password is incorrect.');
        } catch (NotActivatedException $e) {
            $this->messageBag->add('email', 'This user account is not activated.');
        } catch (UserSuspendedException $e) {
            $this->messageBag->add('email', 'User account suspended.');
        } catch (UserBannedException $e) {
            $this->messageBag->add('email', 'This user account is banned.');
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $this->messageBag->add('email', 'User account suspended because of too many login attempts. Try again after '.$delay.' seconds');
        }

        // Ooops.. something went wrong
		return $this->respond([
			'data'=> $this->messageBag,
			'payload'=> 'Slingspector API v1.0',
			'success' => false
		]);
    }
}
